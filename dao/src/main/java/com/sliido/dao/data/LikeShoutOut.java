package com.sliido.dao.data;

import com.pairapp.util.PLog;
import com.pairapp.util.Task;
import com.path.android.jobqueue.Params;
import com.path.android.jobqueue.RetryConstraint;
import com.sliido.data.ParseClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.realm.Realm;

import static com.path.android.jobqueue.RetryConstraint.createExponentialBackoff;

/**
 * Created by yaaminu on 11/15/17.
 */

public class LikeShoutOut extends Task {
    private static final String TAG = "LikeShoutOut";
    private String id;
    private boolean like;

    public LikeShoutOut() {
    }

    public LikeShoutOut(Params params, String id, boolean like) {
        super(params);
        this.id = id;
        this.like = like;
    }

    public static LikeShoutOut create(String id, boolean like) {
        Params params = new Params(100)
                .setPersistent(true)
                .setGroupId("likeShoutout")
                .setRequiresNetwork(true)
                .addTags("likeShoutout");
        return new LikeShoutOut(params, id, like);
    }

    @Override
    protected JSONObject toJSON() throws JSONException {
        return new JSONObject()
                .put("id", id)
                .put("like", like);
    }

    @Override
    protected Task fromJSON(JSONObject jsonObject) throws JSONException {
        return create(jsonObject.getString("id"), jsonObject.getBoolean("like"));
    }

    @Override
    public void onRun() throws Throwable {
        ShoutOut shoutOut = ParseClient.newInstance()
                .like(id, like);
        Realm realm = ShoutOut.REALM();
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(shoutOut);
            realm.commitTransaction();
        } finally {
            realm.close();
        }
        PLog.d(TAG, "shout out: %s %s successfully", this, like ? "liked" : "unliked");
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount, int maxRunCount) {
        return createExponentialBackoff(runCount, TimeUnit.MINUTES.toMillis(1));
    }
}
