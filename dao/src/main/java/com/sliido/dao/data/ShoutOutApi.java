package com.sliido.dao.data;

import android.support.v4.util.Pair;

import com.pairapp.Errors.SliidoException;

import java.util.List;

/**
 * Created by yaaminu on 11/12/17.
 */

public interface ShoutOutApi {
    ShoutOut post(String body, List<Attachment> attachments) throws SliidoException;

    ShoutOut like(String id, boolean like) throws SliidoException;

    Pair<ShoutOut, ShoutOut> repost(String id) throws SliidoException;

    List<ShoutOut> loadShoutOuts(long oldest) throws SliidoException;
}
