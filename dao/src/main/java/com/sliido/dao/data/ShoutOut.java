package com.sliido.dao.data;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.pairapp.util.Config;
import com.parse.ParseObject;
import com.sliido.data.channel.Channel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

import static com.pairapp.util.Config.getApplicationWidePrefs;

/**
 * Created by yaaminu on 11/12/17.
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class ShoutOut extends RealmObject {

    public static final int POST_FAILED = -1, POSTING = 0, POSTED = 1;
    public static final String LIKE_KEY_PREFIX = "shoutout.likes.liked.";
    public static final String REPOST_KEY_PREFIX = "shoutout.reposts.reposted.";

    @Index
    private String postedBy;
    @PrimaryKey
    private String id;
    private String postedByName;
    private RealmList<Attachment> attachments;
    private String body;
    private long updatedAt;
    private long createdAt;
    private int likes;
    private int state;
    private int reposts;
    private boolean liked, reposted;
    private String parentPost;

    public ShoutOut(String id, String postedBy, String postedByName, String body, int likes,
                    long createdAt, long updatedAt, int state, int reposts, boolean liked,
                    boolean reposted, String parentPost, List<Attachment> attachments) {
        this.id = id;
        this.postedBy = postedBy;
        this.postedByName = postedByName;
        this.body = body;
        this.likes = likes;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.attachments = new RealmList<>();
        this.attachments.addAll(attachments);
        this.state = state;
        this.reposts = reposts;
        this.liked = liked;
        this.reposted = reposted;
        this.parentPost = parentPost;
    }

    public ShoutOut() {
    }

    public static ShoutOut create(ParseObject parseObject) {
        return new ShoutOut(parseObject.getObjectId(), parseObject.getString("postedBy"), parseObject.getString("postedByName"),
                parseObject.getString("body"), parseObject.getInt("likes"), parseObject.getCreatedAt().getTime(), parseObject.getUpdatedAt().getTime(),
                POSTED, parseObject.getInt("reposts"), getApplicationWidePrefs().contains(LIKE_KEY_PREFIX + parseObject.getObjectId()),
                getApplicationWidePrefs().contains(REPOST_KEY_PREFIX + parseObject.getObjectId()), parseObject.getString("parentPost")
                , createAttachments(parseObject.getList("attachments")));
    }

    private static List<Attachment> createAttachments(@Nullable List<Object> attachments) {
        if (attachments == null) {
            return Collections.emptyList();
        }
        List<Attachment> ret = new ArrayList<>(attachments.size());
        for (int i = 0; i < attachments.size(); i++) {
            ret.add(Attachment.fromJson(attachments.get(i).toString()));
        }
        return ret;
    }

    public static Realm REALM() {
        return Channel.REALM();
    }

    public String getId() {
        return id;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public int getState() {
        return state;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String getPostBody() {
        return body;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getReposts() {
        return reposts;
    }

    public RealmList<Attachment> getAttachments() {
        return attachments;
    }

    public boolean isLiked() {
        return liked;
    }

    public void toggleLiked() {
        final SharedPreferences.Editor editor = Config.getApplicationWidePrefs().
                edit();

        if (isLiked()) {
            editor.remove(ShoutOut.LIKE_KEY_PREFIX + getId());
        } else {
            editor.putBoolean(ShoutOut.LIKE_KEY_PREFIX + getId(), true);
        }
        this.liked = !liked;
        setLikes(liked ? Math.max(getLikes(), 0) + 1 : Math.max(getLikes() - 1, 0));
        editor.apply();
    }

    public boolean isReposted() {
        return reposted;
    }

    public void setReposted() {
        this.reposted = true;
        this.reposts = this.reposts + 1;
        final SharedPreferences.Editor editor = Config.getApplicationWidePrefs().
                edit();
        editor.putBoolean(ShoutOut.REPOST_KEY_PREFIX + getId(), true);
        editor.apply();
    }

    public boolean wasReposted() {
        return parentPost != null;
    }

    public String getPostedByName() {
        return postedByName;
    }
}
