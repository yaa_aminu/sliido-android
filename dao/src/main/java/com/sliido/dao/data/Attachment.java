package com.sliido.dao.data;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;

/**
 * Created by yaaminu on 11/12/17.
 */

public class Attachment extends RealmObject {
    private String url;
    private int size, type;

    public Attachment() {
    }

    public Attachment(String url, int size, int type) {
        this.url = url;
        this.size = size;
        this.type = type;
    }

    public static Attachment fromJson(String json) {
        try {
            JSONObject jsonObject
                    = new JSONObject(json);
            return new Attachment(jsonObject.getString("url"), jsonObject.getInt("size"), jsonObject.getInt("type"));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public String getUrl() {
        return url;
    }

    public int getSize() {
        return size;
    }

    public int getType() {
        return type;
    }

    public JSONObject toJson() {
        try {
            return new JSONObject()
                    .put("url", getUrl())
                    .put("type", getType())
                    .put("size", getSize());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
