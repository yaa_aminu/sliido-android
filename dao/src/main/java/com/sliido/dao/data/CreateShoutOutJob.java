package com.sliido.dao.data;

import android.support.annotation.Nullable;

import com.pairapp.util.Config;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.Task;
import com.path.android.jobqueue.Params;
import com.path.android.jobqueue.RetryConstraint;
import com.sliido.data.ParseClient;
import com.sliido.data.post.Post;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;

/**
 * Created by yaaminu on 11/12/17.
 */

public class CreateShoutOutJob extends Task {

    public static final String FILES_UPLOAD_TMP_PREFS = "files.upload.tmp.prefs";
    private static final String TAG = "CreateShoutOutJob";
    private String body;
    private List<String> attachments;

    public CreateShoutOutJob() {
    }

    private CreateShoutOutJob(Params params, String body, @Nullable List<String> attachments) {
        super(params);
        this.body = body;
        this.attachments = attachments == null ? Collections.<String>emptyList() : attachments;
    }

    public static CreateShoutOutJob create(String body, List<String> attachments) {
        return new CreateShoutOutJob(new Params(100)
                .setGroupId("shoutOut")
                .setPersistent(true)
                .setRequiresNetwork(true)
                .addTags("shoutOut"), body, attachments);
    }

    @Override
    protected JSONObject toJSON() throws JSONException {
        return new JSONObject()
                .put("body", body)
                .put("attachment", new JSONArray(attachments));
    }

    @Override
    public void onAdded() {
        super.onAdded();
        List<Attachment> tmp = new ArrayList<>(attachments.size());
        for (String attachment : attachments) {
            tmp.add(new Attachment(attachment, (int) new File(attachment).length(), Post.extensionToType(attachment)));
        }
        User currentUser = UserManager.getInstance().getCurrentUser();
        GenericUtils.assertThat(currentUser != null);
        ShoutOut shoutOut = new ShoutOut("tmpId", currentUser.getEmail(), currentUser.getName(), body, 0, System.currentTimeMillis(), System.currentTimeMillis(),
                Post.POSTING, 0, false, false, null, tmp);
        Realm realm = ShoutOut.REALM();
        try {
            realm.copyToRealmOrUpdate(shoutOut);
        } finally {
            realm.close();
        }
    }

    @Override
    protected Task fromJSON(JSONObject jsonObject) throws JSONException {
        List<String> attachments;

        final JSONArray attachment = jsonObject.optJSONArray("attachment");
        if (attachment != null) {
            attachments = new ArrayList<>(attachment.length());
            for (int i = 0; i < attachment.length(); i++) {
                attachments.add(attachment.getString(i));
            }
        } else {
            attachments = Collections.emptyList();
        }
        return create(jsonObject.getString("body"), attachments);
    }

    @Override
    public void onRun() throws Throwable {
        ShoutOut shoutOut;
        List<Attachment> tmp;
        if (!attachments.isEmpty()) {
            tmp = new ArrayList<>(attachments.size());
            for (String attachment : attachments) {
                final File file = new File(attachment);
                final String alreadyUploaded = Config.getPreferences(FILES_UPLOAD_TMP_PREFS).getString(FILES_UPLOAD_TMP_PREFS, null);
                if (alreadyUploaded == null) {
                    String url = ParseClient.newInstance().saveFileToBackendSync(file, null);
                    final Attachment att = new Attachment(url, (int) file.length(), Post.extensionToType(file.getAbsolutePath()));
                    Config.getPreferences(FILES_UPLOAD_TMP_PREFS)
                            .edit()
                            .putString(file.getAbsolutePath(), att.toJson().toString())
                            .apply();
                    tmp.add(att);
                } else {
                    tmp.add(Attachment.fromJson(alreadyUploaded));
                }
            }
            shoutOut = ShoutOutManager.getInstance()
                    .post(body, tmp);
        } else {
            shoutOut = ShoutOutManager.getInstance().post(body, Collections.<Attachment>emptyList());
        }
        Realm realm = ShoutOut.REALM();
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(shoutOut);
            realm.commitTransaction();
        } finally {
            realm.close();
        }
        PLog.d(TAG, "successfully run job %s", this);
    }

    @Override
    public String toString() {
        return "CreateShoutOutJob{" +
                "body='" + body + '\'' +
                ", attachments=" + attachments +
                '}';
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount, int maxRunCount) {
        PLog.e(TAG, "job erred", throwable.getMessage(), throwable);
        if (throwable instanceof RuntimeException) {
            return RetryConstraint.createExponentialBackoff(runCount, 60000);
        } else {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        }
    }
}
