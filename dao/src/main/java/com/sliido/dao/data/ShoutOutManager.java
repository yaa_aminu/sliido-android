package com.sliido.dao.data;

import android.os.SystemClock;

import com.pairapp.Errors.SliidoException;
import com.pairapp.util.PLog;
import com.pairapp.util.TaskManager;
import com.sliido.data.ParseClient;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import io.realm.Realm;

/**
 * Created by yaaminu on 11/12/17.
 */

public class ShoutOutManager {

    private static final String TAG = "ShoutOutManager";
    private static ShoutOutManager INSTANCE = new ShoutOutManager();
    private final ShoutOutApi client;
    private final AtomicLong lastFetched = new AtomicLong(0);

    private ShoutOutManager() {
        client = ParseClient.newInstance();
    }

    public static ShoutOutManager getInstance() {
        return INSTANCE;
    }

    ShoutOut post(String body, List<Attachment> attachments) throws SliidoException {
        return client.post(body, attachments);
    }

    public void postShoutout(final String postBody, final List<String> attachments) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                TaskManager.runJob(CreateShoutOutJob.create(postBody, attachments));
            }
        }, false);
    }

    public void loadNewShoutouts() {
        if (SystemClock.uptimeMillis() - lastFetched.get()
                < TimeUnit.MINUTES.toMillis(1)) {
            PLog.d(TAG, "updated not too long ago");
            return;
        }
        lastFetched.set(SystemClock.uptimeMillis());
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                long oldest = 0L;
                Realm realm = ShoutOut.REALM();
                try {
                    Number max = realm.where(ShoutOut.class)
                            .max("updatedAt");
                    if (max != null) {
                        oldest = max.longValue();
                    }
                    List<ShoutOut> shoutOuts = client.loadShoutOuts(oldest);
                    if (!shoutOuts.isEmpty()) {
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(shoutOuts);
                        realm.commitTransaction();
                    }
                } catch (SliidoException e) {
                    PLog.e(TAG, e.getMessage(), e);
                } finally {
                    realm.close();
                }
            }
        }, true);
    }

    public void like(Realm realm, ShoutOut shoutOut) {
        final boolean like = !shoutOut.isLiked();
        realm.beginTransaction();
        shoutOut.toggleLiked();
        realm.commitTransaction();
        final LikeShoutOut job = LikeShoutOut.create(shoutOut.getId(), like);
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                TaskManager.runJob(job);
            }
        }, false);
    }

    public void repost(Realm realm, ShoutOut shoutOut) {
        if (!shoutOut.isReposted()) {
            realm.beginTransaction();
            shoutOut.setReposted();
            realm.commitTransaction();
            final RepostJob job = RepostJob.create(shoutOut.getId());
            TaskManager.executeNow(new Runnable() {
                @Override
                public void run() {
                    TaskManager.runJob(job);
                }
            }, false);
        }
    }
}
