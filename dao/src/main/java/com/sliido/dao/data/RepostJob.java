package com.sliido.dao.data;

import android.support.v4.util.Pair;

import com.pairapp.util.PLog;
import com.pairapp.util.Task;
import com.path.android.jobqueue.Params;
import com.path.android.jobqueue.RetryConstraint;
import com.sliido.data.ParseClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.realm.Realm;

import static com.path.android.jobqueue.RetryConstraint.createExponentialBackoff;

/**
 * Created by yaaminu on 11/15/17.
 */

public class RepostJob extends Task {

    private static final String TAG = "LikeShoutOut";
    private String id;

    public RepostJob() {
    }

    public RepostJob(Params params, String id) {
        super(params);
        this.id = id;
    }

    public static RepostJob create(String id) {
        Params params = new Params(100)
                .setPersistent(true)
                .setGroupId("repostShoutout")
                .setRequiresNetwork(true)
                .addTags("repostShoutout");
        return new RepostJob(params, id);
    }

    @Override
    protected JSONObject toJSON() throws JSONException {
        return new JSONObject()
                .put("id", id);
    }

    @Override
    protected Task fromJSON(JSONObject jsonObject) throws JSONException {
        return create(jsonObject.getString("id"));
    }

    @Override
    public void onRun() throws Throwable {
        Pair<ShoutOut, ShoutOut> shoutOut = ParseClient.newInstance()
                .repost(id);
        Realm realm = ShoutOut.REALM();
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(shoutOut.first);
            realm.copyToRealmOrUpdate(shoutOut.second);
            realm.commitTransaction();
        } finally {
            realm.close();
        }
        PLog.d(TAG, "shout out: %s reposted successfully", this);
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount, int maxRunCount) {
        return createExponentialBackoff(runCount, TimeUnit.MINUTES.toMillis(1));
    }

}
