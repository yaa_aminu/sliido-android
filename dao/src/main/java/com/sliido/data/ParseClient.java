package com.sliido.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.text.TextUtils;

import com.pairapp.Errors.SliidoException;
import com.pairapp.net.FileApi;
import com.pairapp.net.FileClientException;
import com.pairapp.util.Config;
import com.pairapp.util.ConnectionUtils;
import com.pairapp.util.FileUtils;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.TaskManager;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.sliido.dao.R;
import com.sliido.dao.data.Attachment;
import com.sliido.dao.data.ShoutOut;
import com.sliido.dao.data.ShoutOutApi;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.channel.ChannelsApi;
import com.sliido.data.post.Comment;
import com.sliido.data.post.Post;
import com.sliido.data.user.User;
import com.sliido.data.user.UserApi;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

import bolts.Task;

import static com.pairapp.util.ThreadUtils.ensureNotMain;
import static com.parse.ParseException.CONNECTION_FAILED;
import static com.parse.ParseException.DUPLICATE_VALUE;
import static com.parse.ParseException.EMAIL_NOT_FOUND;
import static com.parse.ParseException.EMAIL_TAKEN;
import static com.parse.ParseException.INVALID_EMAIL_ADDRESS;
import static com.parse.ParseException.OBJECT_NOT_FOUND;
import static com.parse.ParseException.OPERATION_FORBIDDEN;
import static com.parse.ParseException.SCRIPT_ERROR;
import static com.parse.ParseException.USERNAME_TAKEN;

/**
 * author Null-Pointer on 1/12/2016.
 * more interfaces to come
 */
public class ParseClient implements UserApi, ChannelsApi, FileApi, ShoutOutApi {

    public static final String FUNC_CREATE_POST = "createPost";
    public static final String OBJECT_ID = "objectId";
    public static final int SEARCH_TYPE_CHANNELS = 0;
    public static final String SHOUT_OUTS = "shoutOuts";
    private static final String TAG = ParseClient.class.getSimpleName();
    private static final String KEY_UPDATED_AT = "updatedAt";
    private static final String FUNC_CREATE_CHANNEL = "createChannel";
    private static final String KEY_CHANNELS = "sleedoChannels"; //the field 'channels' is an existing field in ParseInstallation Objects.
    private static final String FEEDS_PREFS = "feedsPrefs";
    private static final String POST = "post";
    private static final String CLASS_NAME_CHANNEL = "channel";
    private static final String PARSE_USER_FIELD_CHANNELS_NO_USE_FOR_PARSEINSTALLATION = "channels";
    private static final String ENABLE_NOTIFICATION = "enableNotification";
    private static final String COMMENT = "comment";
    private static final String CHANNELS_CACHE_KEY = "channels";
    private static final String SYNC_FEEDS = "sync.feeds";
    private static final String SYNC_PERSONAL_CHANNEL = "syncPersonalChannel";
    private static final String PERSONAL_CHANNELS = "personalChannels";
    private static final String EMAIL_VERIFIED = "emailVerified";
    private static final Object channelCreationLock = new Object();
    private static final Semaphore likeLock = new Semaphore(1, true);
    private static final Map<String, Long> syncRateLimiter = new ConcurrentHashMap<>(4);
    private static final int SYNC_CHANNELS_LAPSE = 5000;
    private static volatile boolean isInitialised = false;

    private ParseClient() {
    }

    public static ParseClient newInstance() {
        GenericUtils.ensureConditionTrue(isInitialised, "not initialised, did you forget to init()");
        return new ParseClient();
    }

    public static void init(Context context, int logLevel) {
        GenericUtils.ensureNotNull(context);
        isInitialised = true;
        PLog.d(TAG, "initialising parse client");
        Parse.setLogLevel(logLevel);
        Parse.initialize(new Parse.Configuration.Builder(context)
                .server("https://sliido.herokuapp.com/_/")
//                .server("http://192.168.43.58:6000/_/")
                .applicationId("2EW6UsN7MjCgrQ4XVv9w4Y6huUuXwrU6LFNnTMoXc")
                .clientKey("8npXZ1gOvqZJmT7dkLA2PudMhOgcZsJvgBRASw9k")
                .build());
    }

    private static boolean canSync(String key, long lapse) {
        Long b = syncRateLimiter.get(key);
        return !(b != null && SystemClock.uptimeMillis() - b < lapse);
    }

    public static int fromParseErrorCode(int code) {
        return SliidoException.ERROR_UNKNOWN;
    }

    @Override
    public void deleteUnverifiedAccount() throws SliidoException {
        ensureNotMain();
        ensureConnected();
        ensureActuallyConnected();
        ParseUser user = ParseUser.getCurrentUser();
        if (user == null) {
            return;
        }
        final Task<Void> voidTask = ParseUser.logOutInBackground();
        try {
            voidTask.waitForCompletion();
            //noinspection ThrowableResultOfMethodCallIgnored
            final Exception error = voidTask.getError();
            if (error != null) {
                PLog.d(TAG, "error while deleting account: %s", error.getMessage());
                PLog.d(TAG, "stack", error);
                throw new SliidoException(error.getMessage(), GenericUtils.getString(R.string.error_deleting_account), -1);
            } else if (voidTask.isCancelled()) {
                PLog.d(TAG, "request to delete account cancelled");
                throw new SliidoException(GenericUtils.getString(R.string.error_deleting_account));
            }
        } catch (InterruptedException e) {
            PLog.d(TAG, e.getMessage(), e);
            throw new SliidoException(GenericUtils.getString(R.string.error_deleting_account));
        }
    }

    private void ensureActuallyConnected() throws SliidoException {
        if (!ConnectionUtils.isActuallyConnected()) {
            throw new SliidoException("not connected", GenericUtils.getString(R.string.connection_unavailable), SliidoException.ECONREFUSED);
        }
    }

    public void verifyEmail() throws SliidoException {
        ensureNotMain();
        ParseUser user = ParseUser.getCurrentUser();
        if (user == null) {
            throw new IllegalStateException("no user logged in");
        }
        try {
            user.fetch();
            if (!user.getBoolean("emailVerified")) {
                throw new SliidoException("unverified", GenericUtils.getString(R.string.activation_link_hint), -1);
            }
            setUpPush(user);
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e);
            String message;
            switch (e.getCode()) {
                case CONNECTION_FAILED:
                    message = GenericUtils.getString(R.string.connection_unavailable);
                    break;
                case ParseException.SESSION_MISSING:
                case ParseException.INVALID_SESSION_TOKEN:
                    message = e.getMessage();
                    break;
                default:
                    message = GenericUtils.getString(R.string.internal_server_error);
                    break;
            }
            throw new SliidoException(e.getMessage(), message, -1);
        }
    }

    @Override
    public User signUpOrLogin(String userName, String email, String password) throws SliidoException {
        ensureNotMain();
        GenericUtils.ensureNotEmpty(password);
        GenericUtils.ensureNotEmpty(email);
        ensureConnected();
        try {
            PLog.d(TAG, "logging in user");
            email = email.toLowerCase();
            ParseUser parseUser = ParseUser.logIn(email, password);
            //use the name from our backend instead of the provided username
            userName = parseUser.getString(User.FIELD_NAME);// FIXME: 2/1/2016 why are we setting the username field again?
            parseUser.put(User.FIELD_NAME, userName);
            User user = new User();
            user.setEmail(email);
            user.setName(userName);
            parseUser.save();
            setUpPush(ParseUser.getCurrentUser());
            return user;
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e);
            int code = e.getCode();
            int message;
            switch (code) {
                case INVALID_EMAIL_ADDRESS:
                    message = R.string.invalid_email_address;
                    break;
                case CONNECTION_FAILED:
                    message = R.string.connection_unavailable;
                    break;
                case OBJECT_NOT_FOUND:
                case EMAIL_NOT_FOUND:
                    if (userName != null && !userName.isEmpty()) {
                        return signUp(userName, email, password);
                    }
                    message = R.string.incorrect_email_or_password;
                    break;
                default:
                    message = R.string.internal_server_error;
                    break;
            }
            throw new SliidoException(e.getMessage(), GenericUtils.getString(message), 0);
        }
    }

    private void setUpPush(ParseUser user) throws ParseException {
        ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
        currentInstallation.put(User.FIELD_EMAIL, user.getEmail());
        List<String> subscribedChannels = getSubscribedChannels();
        if (!subscribedChannels.isEmpty()) {
            currentInstallation.addAllUnique(KEY_CHANNELS, subscribedChannels);
        }
        currentInstallation.save();
    }

    private User signUp(String username, String email, String password) throws SliidoException {
        ensureNotMain();
        ensureConnected();
        try {
            ParseUser parseUser = new ParseUser();
            parseUser.setEmail(email);
            parseUser.setUsername(email);
            parseUser.setPassword(password);
            parseUser.put(User.FIELD_NAME, username);
            parseUser.signUp();
            return new User(email, username);
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e);
            int code = e.getCode();
            int message;
            switch (code) {
                case USERNAME_TAKEN:
                case EMAIL_TAKEN:
                    message = R.string.email_taken;
                    break;
                case INVALID_EMAIL_ADDRESS:
                    message = R.string.invalid_email_address;
                    break;
                case CONNECTION_FAILED:
                    message = R.string.connection_unavailable;
                    break;
                default:
                    message = R.string.internal_server_error;
                    break;
            }
            throw new SliidoException(e.getMessage(), GenericUtils.getString(message), 0);
        }
    }

    @SuppressLint("ApplySharedPref")
    @Override
    public void logOut() throws SliidoException {
        ensureNotMain();
        ensureActuallyConnected();
        ParseUser user = ParseUser.getCurrentUser();
        if (user == null) {
            PLog.w(TAG, "logging out when no user is logged in");
        } else {
            PLog.w(TAG, "logging out %s", user.getEmail());
            ParseUser.logOut();
            PLog.v(TAG, "logged out successfully");
        }
        File dir = Config.getApplication().getFilesDir();
        if (dir.exists() && dir.isDirectory()) {
            File[] files = dir.listFiles();
            for (File file : files) {
                //noinspection ResultOfMethodCallIgnored
                file.delete();
            }
        }
        Config.getApplicationWidePrefs().edit().clear().commit();
        Config.getPreferences(ChannelManager.CACHE_PREFS).edit().clear().commit();

    }

    @Override
    public List<String> getNames(List<String> emails) throws SliidoException {
        ParseQuery<ParseUser> query = ParseQuery.getQuery(ParseUser.class);
        query.whereContainedIn("email", emails);
        query.selectKeys(Collections.singletonList(User.FIELD_NAME));
        try {
            List<ParseUser> users = query.find();
            List<String> names = new ArrayList<>(users.size());
            for (ParseUser user : users) {
                names.add(user.getString(User.FIELD_NAME));
            }
            return names;
        } catch (ParseException e) {
            throw new SliidoException(e.getMessage(), e.getCode());
        }
    }

    @Override
    public void resetPassword(String email) throws SliidoException {
        GenericUtils.ensureNotEmpty(email);
        GenericUtils.ensureConditionTrue(getCurrentUser() == null, "user already logged in");
        ensureConnected();
        ensureNotMain();
        try {
            ParseUser.requestPasswordReset(email);
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e.getCode());
            rethrowParseError(e);
        }
    }

    private void ensureConnected() throws SliidoException {
        if (!ConnectionUtils.isConnected()) {
            throw new SliidoException("no connection", GenericUtils.getString(R.string.connection_unavailable), SliidoException.ECONREFUSED);
        }
    }

    @Override
    public boolean isCurrentUserVerified() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        return currentUser != null &&
                (currentUser.getBoolean("verified") || currentUser.getBoolean("emailVerified"));
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        ParseUser user = ParseUser.getCurrentUser();
        if (user != null) {
            user.fetchIfNeededInBackground();
            User u = new User();
            u.setName(user.getString(User.FIELD_NAME));
            u.setEmail(user.getEmail());
            return u;
        }
        return null;
    }

    @NonNull
    @Override
    public List<User> getUsers(String... emails) throws SliidoException {
        ensureNotMain();
        ensureConnected();

        throw new UnsupportedOperationException();
    }

    private void rethrowParseError(ParseException e) throws SliidoException {
        String message;
        int errorCode;
        switch (e.getCode()) {
            case OBJECT_NOT_FOUND:
                message = GenericUtils.getString(R.string.resource_not_found);
                errorCode = SliidoException.ENOTFOUND;
                break;
            case OPERATION_FORBIDDEN:
                message = GenericUtils.getString(R.string.unauthorized);
                errorCode = SliidoException.EUNAUTHORIZED;
                break;
            case CONNECTION_FAILED:
                message = GenericUtils.getString(R.string.connection_unavailable);
                errorCode = SliidoException.ECONREFUSED;
                break;
            case DUPLICATE_VALUE:
                message = GenericUtils.getString(R.string.duplicate_resource);
                errorCode = SliidoException.EDUPLICATE;
                break;
            case SCRIPT_ERROR:
                //check if the error was deliberately sent by the cloud code
                try {
                    JSONObject object = new JSONObject(e.getMessage());
                    try {
                        object = new JSONObject(object.getString("message"));
                    } catch (JSONException ignored) {

                    }
                    String msg = object.getString("message");
                    int code = object.getInt("code");
                    if (code != SCRIPT_ERROR) {//avoid stack overflow exceptions
                        rethrowParseError(new ParseException(code, msg));
                    }
                    //else fall through
                } catch (JSONException ignored) {
                    PLog.d(TAG, ignored.getMessage(), ignored);
                }
                //fall through
            default:
                message = e.getMessage();
                errorCode = SliidoException.INTERNALSERVERERROR;
                break;
        }
        throw new SliidoException(e.getMessage(), message, errorCode);
    }

    private void ensureUserVerified() {
        if (!isCurrentUserVerified()) {
            throw new IllegalStateException("no logged in user");
        }
    }

    @Nullable
    @Override
    public Channel createChannel(@NonNull String name, String coverPhoto, boolean isPublic) throws SliidoException {
        synchronized (channelCreationLock) {
            GenericUtils.ensureNotEmpty(name);
            ensureNotMain();
            if ("teamsliido".equalsIgnoreCase(name) || Config.appName().equalsIgnoreCase(name.trim()) || "yaaaminu".equalsIgnoreCase(name.trim())) {
                throw new SliidoException("cant use reserve channel name " + name,
                        String.format(GenericUtils.getString(R.string.reserved_name_error), name), -1);
            }
            ensureConnected();
            ParseUser user = ParseUser.getCurrentUser();
            GenericUtils.ensureConditionTrue(user != null && user.getBoolean(EMAIL_VERIFIED), "no user logged in or verified");
            Map<String, Object> params = new HashMap<>(3);
            params.put(Channel.FIELD_NAME, name);
            params.put(Channel.FIELD_COVER_PHOTO, coverPhoto);
            params.put(Channel.FIELD_IS_PUBLIC, isPublic ? 1 : 0);
            try {
                assert user != null;
                final Map<String, String> ret = ParseCloud.callFunction(FUNC_CREATE_CHANNEL, params);
                final String channelId = ret.get(Channel.FIELD_CHANNEL_ID);
                user.addUnique(PERSONAL_CHANNELS, channelId);
                user.save();
                return Channel.create(ret.get(Channel.FIELD_CHANNEL_ID), ret.get(Channel.FIELD_NAME),
                        user.getEmail(), user.getString(User.FIELD_NAME), 0, "true".equals(ret.get("isSuper")),
                        coverPhoto, isPublic);
            } catch (ParseException e) {
                PLog.d(TAG, e.getMessage(), e);
                rethrowParseError(e);
            }
            return null;
        }
    }

    @Override
    public int subscribeToChannel(@NonNull String channelId) throws SliidoException {
        GenericUtils.ensureNotEmpty(channelId);
        ensureNotMain();
        ensureConnected();
        if (ParseUser.getCurrentUser() == null) {
            throw new SliidoException("You have to login first");
        }
        int subscribersCount = -1;
        if (getSubscribedChannels().contains(channelId)) {
            throw new SliidoException("EDUPLICATE SUBSCRIPTION", GenericUtils.getString(R.string.already_subscribed), -1);
        } else if (getPersonalChannels().contains(channelId)) {
            throw new SliidoException("EDUPLICATE SUBSCRIPTION", GenericUtils.getString(R.string.cannot_subscribe_to_own_channel), -1);
        }
        try {
            ParseObject object = ParseQuery.getQuery(CLASS_NAME_CHANNEL).get(channelId);
            object.increment(Channel.FIELD_SUBSCRIBERS);
            subscribersCount = object.getInt(Channel.FIELD_SUBSCRIBERS);
            ParseUser user = ParseUser.getCurrentUser();
            assert user != null;
            user.addUnique(PARSE_USER_FIELD_CHANNELS_NO_USE_FOR_PARSEINSTALLATION, channelId);
            user.save();
            ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
            currentInstallation.addUnique(KEY_CHANNELS, channelId);
            currentInstallation.save();

            //it's important, we increase number of subscribers only after all operations succeeded.
            object.save();
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e);
            rethrowParseError(e);
        }
        return subscribersCount;
    }

    @Override
    public int unSubscribeToChannel(@NonNull final String channelId) throws SliidoException {
        GenericUtils.ensureNotEmpty(channelId);
        ensureNotMain();
        ensureConnected();
        int subscribersCount = -1;
        if (!getSubscribedChannels().contains(channelId)) {
            throw new SliidoException("EINVALID SUBSCRIPTION", GenericUtils.getString(R.string.cannot_unsubcribe_to_channel), -1);
        } else if (getPersonalChannels().contains(channelId)) {
            throw new SliidoException("EINVALID SUBSCRIPTION", GenericUtils.getString(R.string.cannot_unsubcribe_to_own_channel), -1);
        }
        try {
            ParseObject object = ParseQuery.getQuery(CLASS_NAME_CHANNEL).get(channelId);
            object.increment(Channel.FIELD_SUBSCRIBERS, -1);
            subscribersCount = object.getInt(Channel.FIELD_SUBSCRIBERS);
            ParseUser user = ParseUser.getCurrentUser();
            assert user != null;
            List<String> list = Collections.singletonList(channelId);
            user.removeAll(PARSE_USER_FIELD_CHANNELS_NO_USE_FOR_PARSEINSTALLATION, list);
            user.save();
            ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
            currentInstallation.removeAll(KEY_CHANNELS, list);
            currentInstallation.save();

            //it's important, we increase number of subscribers only after all operations succeeded.
            object.save();
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e);
            rethrowParseError(e);
        }
        return subscribersCount;
    }

    @NonNull
    @Override
    public Channel fetchChannel(long oldest, String id) throws SliidoException {
        ensureConnected();
        ensureNotMain();
        ensureUserVerified();
        GenericUtils.ensureNotEmpty(id);
        ParseQuery<ParseObject> query = new ParseQuery<>(CLASS_NAME_CHANNEL);
        newer(query, oldest);
        try {
            ParseObject obj = query.get(id);
            return parseObjectToChannel(obj);
        } catch (ParseException e) {
            PLog.trace(PLog.LEVEL_ERROR, TAG, e);
            rethrowParseError(e); //will throw
        }
        throw new AssertionError(); //handle parseParseError must throw
    }

    @NonNull
    @Override
    public List<Channel> findChannels(boolean subscribed,
                                      boolean up, long time) throws SliidoException {
        ensureConnected();
        ensureNotMain();
        if (!canSync(CHANNELS_CACHE_KEY + subscribed, SYNC_CHANNELS_LAPSE)) {
            throttle(5000);
        }
        ParseQuery<ParseObject> query = new ParseQuery<>(CLASS_NAME_CHANNEL);

        try {
            List<String> subChannels = getSubscribedChannels(),
                    personalChannels = getPersonalChannels();
            if (subscribed) {
                if (subChannels.isEmpty() && personalChannels.isEmpty()) {
                    return Collections.emptyList();
                }
                List<String> channels = new ArrayList<>(subChannels.size() + personalChannels.size());
                channels.addAll(subChannels);
                channels.addAll(personalChannels);
                query.whereContainedIn(OBJECT_ID, channels);
            } else {
                query.whereEqualTo(Channel.FIELD_IS_PUBLIC, true);
                if (!subChannels.isEmpty())
                    query.whereNotContainedIn(OBJECT_ID, subChannels);
                if (getCurrentUser() != null) {
                    query.whereNotEqualTo(Channel.FIELD_CREATED_BY, getCurrentUser().getEmail());
                }
            }

            if (up) {
                newer(query, time);
            } else {
                older(query, time);
            }
            query.setLimit(25);
            List<ParseObject> temp = query.find();
            List<Channel> channels = new ArrayList<>(temp.size());
            for (int i = 0; i < temp.size(); i++) {
                ParseObject parseObject = temp.get(i);
                Channel channel = parseObjectToChannel(parseObject);
                channels.add(channel);
            }
            syncRateLimiter.put(CHANNELS_CACHE_KEY + subscribed, SystemClock.uptimeMillis());
            return channels;
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e);
            if (e.getCode() == ParseException.OBJECT_NOT_FOUND) {
                return Collections.emptyList();
            }
            rethrowParseError(e);
        }
        return Collections.emptyList();
    }

    private void throttle(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Channel parseObjectToChannel(ParseObject parseObject) {
        Channel channel = new Channel();
        channel.setName(parseObject.getString(Channel.FIELD_NAME));
        channel.setCreatedBy(parseObject.getString(Channel.FIELD_CREATED_BY));
        channel.setCreatedByName(parseObject.getString(Channel.FIELD_CREATED_BY_NAME));
        channel.setChannelId(parseObject.getObjectId());
        channel.setCreatedAt(parseObject.getCreatedAt().getTime());
        channel.setUpdatedAt(parseObject.getUpdatedAt().getTime());
        channel.setSubscribers(parseObject.getInt(Channel.FIELD_SUBSCRIBERS));
        channel.setSuper(parseObject.getBoolean(Channel.FIELD_IS_SUPER));
        channel.setPublic(parseObject.getBoolean(Channel.FIELD_IS_PUBLIC));
        channel.setCoverPhoto(parseObject.getString(Channel.FIELD_COVER_PHOTO));
        final User currentUser = getCurrentUser();
        channel.setSubscribed(currentUser != null && !currentUser.getEmail().equals(parseObject.getString(Channel.FIELD_CREATED_BY)) &&
                getSubscribedChannels().contains(parseObject.getObjectId()));
        return channel;
    }

    @Nullable
    @Override
    public Post post(@NonNull String postBody,
                     @Nullable String attachment,
                     @NonNull String channelId,
                     @NonNull String channelName, @Nullable String title,
                     @Nullable String type, long size) throws SliidoException {
        GenericUtils.ensureNotEmpty(postBody, channelId, channelName);
        ensureNotMain();
        ensureConnected();
        ensureUserVerified();
        try {
            ParseUser user = ParseUser.getCurrentUser();
            Map<String, Object> params = new HashMap<>(3);
            params.put(Post.FIELD_POST_BODY, postBody);
            params.put(Post.FIELD_CHANNEL_ID, channelId);
            params.put(Post.FIELD_CHANNEL_NAME, channelName);
            params.put(Post.FIELD_ATTACHMENT, attachment);
            params.put(Post.FEILD_TITLE, title);
            //re-use params
            params = ParseCloud.callFunction(FUNC_CREATE_POST, params);

            Post post = new Post();
            post.setCommentCount(0);
            post.setViews(0);
            post.setLikes(0);
            post.setState(Post.POSTED);
            post.setLiked(false);
            post.setChannelId(channelId);
            post.setAttachment(attachment);
            if (!TextUtils.isEmpty(attachment)) {
                post.setAttachment(attachment);
                post.setAttachmentTitle(title);
                post.setAttachmentSize(size);
                post.setExtension(type);
            }
            post.setPostId((String) params.get(Post.FIELD_POST_ID));
            post.setChannelName(channelName);
            post.setCreatedAt((Long) params.get(Post.FIELD_DATE_CREATED));
            post.setUpdatedAt((Long) params.get(Post.FIELD_UPDATED_AT));
            post.setPostedBy(user.getEmail());
            post.setPostedByName((String) user.get(User.FIELD_NAME));
            post.setPostBody(postBody);
            return post;
        } catch (ParseException e) {
            rethrowParseError(e);
        }
        return null;
    }

    @SuppressLint("ApplySharedPref")
    @Override
    public int likePost(@NonNull String postId) throws SliidoException {
        likeLock.acquireUninterruptibly();
        try {
            GenericUtils.ensureNotEmpty(postId);
            ensureUserVerified();
            ensureConnected();

            SharedPreferences prefs = Config.getPreferences(FEEDS_PREFS);
            prefs.edit().putBoolean(postId, true).commit();

            ParseQuery<ParseObject> query = new ParseQuery<>(POST);
            query.selectKeys(Arrays.asList(Post.FIELD_LIKERS, Post.FIELD_LIKES));
            ParseObject object = query.get(postId);
            int ret = object.getInt(Post.FIELD_LIKES);
            final List<Object> list = object.getList(Post.FIELD_LIKERS);
            //noinspection ConstantConditions //user must be verified. see check above
            if (list == null || !list.contains(getCurrentUser().getEmail())) {
                object.increment(Post.FIELD_LIKES, 1);
                object.addUnique(Post.FIELD_LIKERS, getCurrentUser().getEmail());
                ret++;
                object.save();
                ParseCloud.callFunction("notifyLiked", Collections.singletonMap("postId", postId));
            } else {
                PLog.d(TAG, "can't like a post you have already liked");
            }
            return ret;
        } catch (ParseException e) {
            rethrowParseError(e);
            return -1;//will never get executed
        } finally {
            likeLock.release();
        }
    }

    @SuppressLint("ApplySharedPref")
    @Override
    public int unLikePost(@NonNull String postId) throws SliidoException {
        likeLock.acquireUninterruptibly();
        try {
            GenericUtils.ensureNotEmpty(postId);
            ensureUserVerified();
            ensureConnected();

            SharedPreferences prefs = Config.getPreferences(FEEDS_PREFS);
            prefs.edit().remove(postId).commit();
            ParseQuery<ParseObject> query = new ParseQuery<>(POST);
            query.selectKeys(Arrays.asList(Post.FIELD_LIKERS, Post.FIELD_LIKES));
            ParseObject object = query.get(postId);
            int ret = object.getInt(Post.FIELD_LIKES);
            final List<Object> likers = object.getList(Post.FIELD_LIKERS);
            //noinspection ConstantConditions //user must be verified. see check above
            if (likers != null && likers.contains(getCurrentUser().getEmail())) {
                if (object.getInt(Post.FIELD_LIKES) > 0) {
                    object.increment(Post.FIELD_LIKES, -1);
                    ret--;
                }
                object.removeAll(Post.FIELD_LIKERS, Collections.singleton(getCurrentUser().getEmail()));
                object.save();
            } else {
                PLog.d(TAG, "can't unlike a post you haven't liked");
            }
            return ret < 0 ? 0 : ret;
        } catch (ParseException e) {
            rethrowParseError(e);
            return -1;//will never get executed
        } finally {
            likeLock.release();
        }
    }

    @NonNull
    @Override
    public Post refreshPost(long updatedAt, String postId) throws SliidoException {
        ensureNotMain();
        ensureConnected();
        GenericUtils.ensureNotEmpty(postId);
        ParseQuery<ParseObject> objectParseQuery = new ParseQuery<>(POST);
        try {
            return parseObjectToPostObject(objectParseQuery.get(postId));
        } catch (ParseException e) {
            PLog.trace(TAG, e);
            rethrowParseError(e);
        }
        throw new AssertionError();
    }

    @NonNull
    @Override
    public Comment addCommentToPost(String postID, String channelID, String commentBody, String localCommentID) throws SliidoException {
        ParseObject parseObject = ParseObject.create(COMMENT);
        parseObject.put(Comment.FIELD_POST_ID, postID);
        parseObject.put(Comment.FIELD_COMMENT_BODY, commentBody);
        parseObject.put(Comment.FIELD_CHANNEL_ID, channelID);
        User currentUser = getCurrentUser();
        GenericUtils.ensureConditionTrue(currentUser != null, "user not logged in");
        assert currentUser != null;
        parseObject.put(Comment.FIELD_POSTED_BY_EMAIL, currentUser.getEmail());
        parseObject.put(Comment.FIELD_POSTED_BY_NAME, currentUser.getName());
        try {
            parseObject.save();
            return parseObjectToComment(parseObject, localCommentID);
        } catch (ParseException e) {
            rethrowParseError(e);
        }
        throw new AssertionError();
    }

    @NonNull
    private Comment parseObjectToComment(ParseObject object, @Nullable String localCommentID) {
        return new Comment.Builder()
                .channelID(object.getString(Comment.FIELD_CHANNEL_ID))
                .postID(object.getString(Comment.FIELD_POST_ID))
                .commentBody(object.getString(Comment.FIELD_COMMENT_BODY))
                .createdAt(object.getCreatedAt().getTime())
                .localCommentID(localCommentID)
                .updatedAt(object.getUpdatedAt().getTime())
                .id(object.getObjectId())
                .postedByEmail(object.getString(Comment.FIELD_POSTED_BY_EMAIL))
                .postedByName(object.getString(Comment.FIELD_POSTED_BY_NAME))
                .build();
    }

    @NonNull
    @Override
    public List<Comment> getPostComments(long updatedAt, String postID, boolean older) throws SliidoException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(COMMENT)
                .whereEqualTo(Comment.FIELD_POST_ID, postID)
                .setLimit(50);
        if (older) {
            older(query, updatedAt);
        } else {
            newer(query, updatedAt);
        }
        try {
            List<ParseObject> objects = query.find();
            List<Comment> comments = new ArrayList<>(objects.size());
            for (ParseObject object : objects) {
                comments.add(parseObjectToComment(object, null));
            }
            return comments;
        } catch (ParseException e) {
            rethrowParseError(e);
        }
        throw new AssertionError();
    }

    @NonNull
    @Override
    public List<Post> findFeeds(@Nullable String channelID, long time, boolean older) throws SliidoException {
        ensureConnected();
        ensureNotMain();
        channelID = channelID == null ? "" : channelID;
        if (!canSync(SYNC_FEEDS + channelID + older, SYNC_CHANNELS_LAPSE)) {
            throttle(5000);
        }
        ParseQuery<ParseObject> query = new ParseQuery<>(POST);
        if (GenericUtils.isEmpty(channelID)) {
            final List<String> mutableSubscribedChannels;
            List<String> subscribedChannels = getSubscribedChannels();
            List<String> personalChannels = getPersonalChannels();
            mutableSubscribedChannels = new ArrayList<>(subscribedChannels.size() + personalChannels.size() + 1);
            mutableSubscribedChannels.addAll(subscribedChannels);
            mutableSubscribedChannels.addAll(personalChannels);
            //if time is > zero it means it's not user has some posts already loaded so
            //we assume he/she has loaded the first post for the default sliido channel
            if (mutableSubscribedChannels.isEmpty() && time > 0) {
                return Collections.emptyList();
            }
            //force user to subscribe to default sliido channel
            mutableSubscribedChannels.add(Config.appName());
            query.whereContainedIn(Post.FIELD_CHANNEL_ID, mutableSubscribedChannels);
            query = ParseQuery.or(Arrays.asList(query, ParseQuery.getQuery(POST).whereEqualTo(Post.FIELD_WHITE_LISTED, true)));
        } else {
            query.whereEqualTo(Post.FIELD_CHANNEL_ID, channelID);
        }
        query.addDescendingOrder(KEY_UPDATED_AT);
        query.setLimit(35);
        if (older) {
            if (time <= 0) {
                throw new IllegalArgumentException("fetching older requires a valid time");
            }
            older(query, time);
        } else {
            newer(query, time);
        }
        try {
            List<ParseObject> posts = query.find();
            List<Post> postsTemp = new ArrayList<>();
            for (ParseObject object : posts) {
                Post post = parseObjectToPostObject(object);
                postsTemp.add(post);
            }
            syncRateLimiter.put(SYNC_FEEDS + channelID + older, SystemClock.uptimeMillis());
            return postsTemp;
        } catch (ParseException ignored) {
            if (ignored.getCode() == ParseException.OBJECT_NOT_FOUND) {
                return Collections.emptyList();
            }
            rethrowParseError(ignored);
        }
        return Collections.emptyList();
    }

    @NonNull
    public List<String> getPersonalChannels() {
        if (ParseUser.getCurrentUser() == null) return Collections.emptyList();
        List<String> personalChannels = ParseUser.getCurrentUser().getList(PERSONAL_CHANNELS);
        if (personalChannels == null) {
            return Collections.emptyList();
        }
        return personalChannels;
    }

    @NonNull
    private Post parseObjectToPostObject(ParseObject object) {
        Post post = new Post();
        post.setUpdatedAt(object.getUpdatedAt().getTime());
        post.setCreatedAt(object.getCreatedAt().getTime());
        post.setChannelId(object.getString(Post.FIELD_CHANNEL_ID));
        post.setPostedByName(object.getString(Post.FIELD_POSTED_BY_NAME));
        post.setPostedBy(object.getString(Post.FIELD_POSTED_BY));
        post.setLikes(object.getInt(Post.FIELD_LIKES));
        String attachment = object.getString(Post.FIELD_ATTACHMENT);
        if (!TextUtils.isEmpty(attachment)) {
            Uri uri = Uri.parse(attachment);
            post.setAttachment(uri.toString());
            post.setAttachmentTitle(object.getString(Post.FEILD_TITLE));
            String size = uri.getQueryParameter("size");
            post.setAttachmentSize(TextUtils.isEmpty(size) ? 0 : Long.parseLong(size));
            String[] split = uri.getLastPathSegment().split("\\.");
            post.setExtension(split.length == 2 ? split[1] : "");
            post.setType(Post.extensionToType(uri.getLastPathSegment()));
        }
        String postId = object.getObjectId();
        post.setPostId(postId);
        final List<Object> following = object.getList("commenters");
        post.setCommenters(TextUtils.join(",", following == null ? Collections.emptyList() : following));
        List<String> likers = object.getList(Post.FIELD_LIKERS);
        likers = likers == null ? Collections.<String>emptyList() : likers;
        post.setLikers(TextUtils.join(",", likers));

        post.setLiked(getCurrentUser() != null &&
                (likers.contains(getCurrentUser().getEmail()) || Config.getPreferences(FEEDS_PREFS).getBoolean(postId, false)));

        post.setState(Post.POSTED);
        post.setViews(object.getInt(Post.FIELD_VIEWS));
        post.setChannelName(object.getString(Post.FIELD_CHANNEL_NAME));
        post.setPostBody(object.getString(Post.FIELD_POST_BODY));
        post.setCommentCount(object.getLong(Post.FIELD_COMMENT_COUNT));
        return post;
    }

    @Override
    public int incrementViews(@NonNull String postId) throws SliidoException {
        GenericUtils.ensureNotEmpty(postId);
        ensureUserVerified();
        ensureConnected();

        ParseQuery<ParseObject> query = new ParseQuery<>(POST);
        try {
            ParseObject object = query.get(postId);
            object.increment(Post.FIELD_VIEWS);
            object.saveEventually();
            return 1;
        } catch (ParseException e) {
            rethrowParseError(e);
            return -1;
        }
    }

    @NonNull
    @Override
    public List<String> getSubscribedChannels() {
        if (ParseUser.getCurrentUser() == null) return Collections.emptyList();
        List<String> channels = ParseUser
                .getCurrentUser().getList(PARSE_USER_FIELD_CHANNELS_NO_USE_FOR_PARSEINSTALLATION);
        if (channels == null) {
            channels = Collections.emptyList();
        }
        return channels;

    }

    @NonNull
    @Override
    public List<Channel> fetchChannelsForCurrentUser(long time) throws SliidoException {
        ensureConnected();
        ensureNotMain();
        if (!canSync(SYNC_PERSONAL_CHANNEL, SYNC_CHANNELS_LAPSE)) {
            throttle(5000);
        }
        if (getPersonalChannels().isEmpty()) {
            return Collections.emptyList();
        }
        ParseUser user = ParseUser.getCurrentUser();
        assert user != null;
        ParseQuery<ParseObject> objectParseQuery = new ParseQuery<>(CLASS_NAME_CHANNEL);
        objectParseQuery.whereEqualTo(Channel.FIELD_CREATED_BY, user.getEmail());
        older(objectParseQuery, time);
        try {
            List<ParseObject> objects = objectParseQuery.find();
            List<Channel> channels = new ArrayList<>(objects.size());
            for (ParseObject object : objects) {
                channels.add(parseObjectToChannel(object));
            }
            syncRateLimiter.put(SYNC_PERSONAL_CHANNEL, SystemClock.uptimeMillis());
            return channels;
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e);
            if (e.getCode() == ParseException.OBJECT_NOT_FOUND) {
                return Collections.emptyList();
            }
            rethrowParseError(e);
        }
        return Collections.emptyList();
    }

    @Override
    public void disableOrEnableNotificationsForAllChannels(boolean enable) throws SliidoException {
        ensureUserVerified();
        ensureNotMain();
        ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
        currentInstallation.put(ENABLE_NOTIFICATION, enable ? 1 : 0);
        currentInstallation.saveEventually();
    }

    @Override
    public void disableNotificationsForChannel(String channel, boolean enable) throws SliidoException {
        ensureUserVerified();
        ensureNotMain();
        ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
        if (enable) {
            currentInstallation.addUnique(KEY_CHANNELS, channel);
        } else {
            currentInstallation.removeAll(KEY_CHANNELS, Collections.singletonList(channel));
        }
        currentInstallation.saveEventually();
    }

    private void newer(ParseQuery<ParseObject> query, long lastUpdated) {
        lastUpdated = lastUpdated < 0L ? 0 : lastUpdated;
        query.whereGreaterThan(KEY_UPDATED_AT, new Date(lastUpdated));
        query.orderByDescending(KEY_UPDATED_AT);
    }

    private void older(ParseQuery<ParseObject> query, long lastUpdated) {
        if (lastUpdated > 0L) {
            query.whereLessThan(KEY_UPDATED_AT, new Date(lastUpdated));
        }
        query.orderByDescending(KEY_UPDATED_AT);
    }

    public List<Channel> search(String query, int searchType) throws SliidoException {
        try {
            ParseQuery<ParseObject> parseQuery;
            switch (searchType) {
                case SEARCH_TYPE_CHANNELS:
                    parseQuery = ParseQuery.getQuery(CLASS_NAME_CHANNEL);
                    parseQuery.whereContains("nameUpper", query.toUpperCase(Locale.US));
                    break;
                default:
                    throw new AssertionError();
            }
            List<ParseObject> ret = parseQuery.find();
            if (ret.isEmpty()) return Collections.emptyList();

            List<Channel> items = new ArrayList<>(ret.size());
            for (ParseObject parseObject : ret) {
                items.add(parseObjectToChannel(parseObject));
            }
            return items;
        } catch (ParseException e) {
            throw new SliidoException(e.getMessage(), SliidoException.ERROR_UNKNOWN);
        }
    }

    @Override
    public void saveFileToBackend(final File file, final FileSaveCallback callback, final ProgressListener listener) {
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                try {
                    String url = saveFileToBackendSync(file, listener);
                    callback.done(null, url);
                } catch (FileClientException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    callback.done(e, null);
                }
            }
        }, true);
    }

    @Override
    public String saveFileToBackendSync(File file, ProgressListener listener) throws FileClientException {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            ParseObject obj = ParseObject.create("attachments");
            ParseFile parseFile = new ParseFile(IOUtils.toByteArray(inputStream), FileUtils.getMimeType(file.getAbsolutePath()));
            parseFile.save();
            obj.put("file", parseFile);
            obj.save();
            return obj.getParseFile("file").getUrl();
        } catch (ParseException e) {
            throw new FileClientException(e, e.getCode());
        } catch (IOException e) {
            throw new FileClientException(e, -1);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    @Override
    public void deleteFileFromBackend(String fileName, FileDeleteCallback callback) {
        throw new UnsupportedOperationException();
    }

    private ShoutOut createShoutOut(final String body, final List<Attachment> attachments) throws SliidoException {
        ensureConnected();
        ensureUserVerified();
        User user = getCurrentUser();
        ParseObject parseObject = ParseObject.create(SHOUT_OUTS);
        //noinspection ConstantConditions
        parseObject.put("postedBy", user.getEmail());
        parseObject.put("postedByName", user.getName());
        parseObject.put("body", body);
        parseObject.put("likes", 0);
        try {
            if (!attachments.isEmpty()) {
                for (Attachment attachment : attachments) {
                    GenericUtils.assertThat(attachment.getUrl().startsWith("http"));
                    parseObject.add("attachments", new Attachment(attachment.getUrl(), attachment.getSize(),
                            Post.extensionToType(attachment.getUrl())).toJson().toString());
                }
            }
            parseObject.save();
            return ShoutOut.create(parseObject);
        } catch (ParseException e) {
            PLog.e(TAG, e.getMessage(), e);
            throw new SliidoException(e.getMessage());
        }
    }

    @Override
    public ShoutOut post(String body, List<Attachment> attachments) throws SliidoException {
        return createShoutOut(body, attachments);
    }

    @Override
    public ShoutOut like(String id, boolean like) throws SliidoException {
        ensureUserVerified();
        ensureConnected();
        try {
            final ParseObject parseObject = ParseQuery.getQuery(SHOUT_OUTS)
                    .get(id);
            parseObject
                    .increment("likes", like ? 1 : -1);
            parseObject.save();
            return ShoutOut.create(parseObject);
        } catch (ParseException e) {
            throw new SliidoException(e.getMessage(), SliidoException.ERROR_UNKNOWN);
        }
    }

    @Override
    public Pair<ShoutOut, ShoutOut> repost(String id) throws SliidoException {
        ensureUserVerified();
        ensureConnected();
        try {
            final ParseObject parseObject = ParseQuery.getQuery(SHOUT_OUTS)
                    .get(id);

            ParseObject clone = ParseObject.create(SHOUT_OUTS);
            for (String key : parseObject.keySet()) {
                clone.put(key, parseObject.get(key));
            }
            clone.put("parentPost", parseObject.getObjectId());
            parseObject
                    .increment("reposts", 1);
            ParseObject.saveAll(Arrays.asList(parseObject, clone));
            return Pair.create(ShoutOut.create(parseObject), ShoutOut.create(clone));
        } catch (ParseException e) {
            throw new SliidoException(e.getMessage(), SliidoException.ERROR_UNKNOWN);
        }
    }

    @Override
    public List<ShoutOut> loadShoutOuts(long oldest) throws SliidoException {
        try {
            GenericUtils.assertThat(oldest >= 0);
            final ParseQuery<ParseObject> query = ParseQuery.getQuery(SHOUT_OUTS);
            query.whereGreaterThan("updatedAt", new Date(oldest));
            query.addDescendingOrder("updatedAt");
            query.setLimit(200);
            List<ParseObject> ret = query.find();
            if (ret.isEmpty()) {
                return Collections.emptyList();
            }
            List<ShoutOut> shoutouts = new ArrayList<>(ret.size());
            for (ParseObject parseObject : ret) {
                shoutouts.add(ShoutOut.create(parseObject));
            }
            return shoutouts;
        } catch (ParseException e) {
            throw new SliidoException(e.getMessage(), fromParseErrorCode(e.getCode()));
        }
    }

    @SuppressWarnings("unused")
    static class CreateFakePostsCommentsChannels {
        static ParseClient client = ParseClient.newInstance();
        static SecureRandom random = new SecureRandom();
        static String postBody = "The chance to study further in the University of Health and allied sciences " +
                "is an incredible opportunity and one I intend on seizing wholeheartedly with no " +
                "reservation. It also represents to me a great challenge which I am ever ready to " +
                "take on however hard a row it is to hoe.",
                commentBody = "This is comment for testing purposes, It's intentionally made up";

        static void create() {
            int i = 0;
            while (i++ < 100) {
                doWork(i);
            }
        }

        static private void doWork(final int i) {
            TaskManager.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Channel channel = client.createChannel("Channel " + i + " " + random.nextInt(), null, true);
                        int j = 0;
                        while (j++ < 5) {
                            assert channel != null;
                            Post post = client.post(i + "-" + j + " " + postBody, "attachments", channel.getChannelId(), channel.getName(), null, null, 0);
                            int k = 0;
                            while (k++ < 5) {
                                assert post != null;
                                client.addCommentToPost(post.getPostId(), post.getChannelId(), i + "-" + j + "-" + k + " " + commentBody, "comment"); //local commentID is bogus since we are not going to persist locally
                            }
                        }
                    } catch (SliidoException e) {
                        e.printStackTrace();
                    }
                }
            }, true);
        }
    }
}