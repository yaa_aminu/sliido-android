package com.sliido.data.channel;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pairapp.Errors.ErrorCenter;
import com.pairapp.Errors.SliidoException;
import com.pairapp.util.Config;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.FileUtils;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.TaskManager;
import com.pairapp.util.ThreadUtils;
import com.sliido.dao.R;
import com.sliido.data.LikePostJob;
import com.sliido.data.ParseClient;
import com.sliido.data.post.Comment;
import com.sliido.data.post.CreatePostJob;
import com.sliido.data.post.Post;
import com.sliido.data.user.UserManager;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmQuery;
import rx.Subscriber;

/**
 * by null-pointer on 2/24/2016.
 */
@SuppressWarnings("WeakerAccess")
public class ChannelManager {

    public static final String EVENT_CREATE_CHANNEL = "createChannel";
    public static final String TAG = ChannelManager.class.getSimpleName();
    public static final String CACHE_PREFS = "cache.prefs.time." + TAG;
    public static final String EVENT_CHANNELS_SYNCED = "channelsSynced";
    public static final String EVENT_FEEDS_SYNCED = "event_feeds_synced";
    public static final String EVENT_DATA_FETCH_OLD_CHANNELS = "sync.fetch.old.channels";
    public static final String EVENT_DATA_FETCH_NEW_CHANNELS = "sync.fetch.new.channels";
    public static final String CHANNELS_SUBSCRIBED = "channels.subscribed";
    public static final String EVENT_DATA_FETCH_NEW_POSTS = "sync.fetch.new.posts";
    public static final String EVENT_DATA_FETCH_OLD_POSTS = "sync.fetch.old.posts";
    public static final Object EVENT_CHANNEL_REFRESHED = "sync.fetch.channel.refresh";
    public static final String EVENT_COMMENTS_LOADED = "sync.fetch.comments";
    public static final String CHANNELS_NOT_SUBSCRIBED = "channels.not.subscribed";
    public static final String CHANNELS_PERSONAL = "channels.personal.time";
    public static final String POSTS_NEW = "posts.new.";
    public static final String POST_OLD = "post.old.";
    private static final String EVENT_DATA_REFRESH_POSTS = "sync.fetch.existing.posts";
    private static final String ENABLE_NOTIFICAION = "notificaions.new.posts";
    private static final Set<String> subScribingChannels = new HashSet<>(5),
            unSubscribingChannels = new HashSet<>(5);
    private static final Comparator<Channel> channelComparator = new Comparator<Channel>() {
        @Override
        public int compare(Channel lhs, Channel rhs) {
            return lhs.getUpdatedAt() < rhs.getUpdatedAt() ? -1 : (lhs == rhs ? 0 : 1);
        }
    };
    private static final Comparator<Post> postComparator = new Comparator<Post>() {
        @Override
        public int compare(Post lhs, Post rhs) {
            return lhs.getUpdatedAt() < rhs.getUpdatedAt() ? -1 : (lhs == rhs ? 0 : 1);
        }
    };

    public static void CreateChannel(final String name, final String coverPhoto, final boolean isPublic) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {

                EventBus eventBus = EventsCenter.getEventBus();
                Realm realm = Channel.REALM();
                ChannelsApi api = ParseClient.newInstance();
                try {
                    if (realm.where(Channel.class).count() == 0) {
                        //sync channels first before allowing this user to create a new one
                        //it could be that they have created a channel before on a different device
                        //we do this so that find*channels methods will not misbehave in some edge cases
                        findChannelsForCurrentUserSync(realm);
                    }
                    Channel channel = api.createChannel(name, coverPhoto, isPublic);
                    assert channel != null;
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(channel);
                    realm.commitTransaction();

                    eventBus.post(new EventBus.Event(EVENT_CREATE_CHANNEL, null, channel));
                } catch (SliidoException e) {
                    if (!eventBus.post(new EventBus.Event(EVENT_CREATE_CHANNEL, e, null))) {
                        ErrorCenter.reportError(EVENT_CREATE_CHANNEL, e.getUserFriendlyMessage(), null);
                    }
                } finally {
                    realm.close();
                }
            }
        }, true);
    }

    public static void subscribeToChannel(final String channelId, final UserManager.Callback callback) {
        synchronized (subScribingChannels) {
            if (subScribingChannels.contains(channelId)) {
                TaskManager.executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.done(new SliidoException(GenericUtils.getString(R.string.already_subscribing)));
                    }
                });
                return;
            }
            subScribingChannels.add(channelId);
        }
        TaskManager.execute(new Runnable() {
            SliidoException exception = null;

            @Override
            public void run() {
                if (ThreadUtils.isMainThread()) {
                    synchronized (subScribingChannels) {
                        subScribingChannels.remove(channelId);
                    }
                    callback.done(exception);
                } else {
                    try {
                        int subscribersCount = ParseClient.newInstance().subscribeToChannel(channelId);
                        Realm realm = Channel.REALM();
                        Channel channel = realm.where(Channel.class).equalTo(Channel.FIELD_CHANNEL_ID, channelId).findFirst();
                        if (channel != null) {
                            realm.beginTransaction();
                            channel.setSubscribers(subscribersCount);
                            channel.setSubscribed(true);
                            realm.commitTransaction();
                        }
                        realm.close();
                    } catch (SliidoException e) {
                        exception = e;
                    }
                    TaskManager.executeOnMainThread(this);
                }
            }
        }, true);
    }

    public static boolean isUnsubScribing(String channelID) {
        synchronized (unSubscribingChannels) {
            return unSubscribingChannels.contains(channelID);
        }
    }

    public static boolean isSubScribing(String channelID) {
        synchronized (subScribingChannels) {
            return subScribingChannels.contains(channelID);
        }
    }

    public static void unSubscribeToChannel(final String channelId, final UserManager.Callback callback) {
        synchronized (unSubscribingChannels) {
            if (unSubscribingChannels.contains(channelId)) {
                TaskManager.executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.done(new SliidoException(GenericUtils.getString(R.string.already_unsubscribing)));
                    }
                });
                return;
            }
            unSubscribingChannels.add(channelId);
        }
        TaskManager.execute(new Runnable() {
            SliidoException exception = null;

            @SuppressLint("CommitPrefEdits")
            @Override
            public void run() {
                if (ThreadUtils.isMainThread()) {
                    synchronized (unSubscribingChannels) {
                        unSubscribingChannels.remove(channelId);
                    }
                    callback.done(exception);
                } else {
                    try {
                        int subscribersCount = ParseClient.newInstance().unSubscribeToChannel(channelId);
                        Realm realm = Channel.REALM();
                        Channel channel = realm.where(Channel.class).equalTo(Channel.FIELD_CHANNEL_ID, channelId).findFirst();
                        if (channel != null) {
                            realm.beginTransaction();
                            channel.setSubscribers(subscribersCount);
                            channel.setSubscribed(false);
                            realm.commitTransaction();
                        }
                        Config.getApplicationWidePrefs().edit().remove(ENABLE_NOTIFICAION + channelId).commit();
                        realm.close();
                    } catch (SliidoException e) {
                        exception = e;
                    }
                    TaskManager.executeOnMainThread(this);
                }
            }
        }, true);
    }

    public static void refreshChannel(final String channelId) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Realm realm = Channel.REALM();
                    long oldest = 0L;
                    RealmQuery<Channel> query = realm.where(Channel.class);
                    Channel ch = query.equalTo(Channel.FIELD_CHANNEL_ID, channelId).findFirst();
                    long updatedAt = -1;
                    if (ch != null) {
                        updatedAt = ch.getUpdatedAt();
                        oldest = updatedAt;
                    }
                    try {
                        if (ch != null || System.currentTimeMillis() - updatedAt > AlarmManager.INTERVAL_FIFTEEN_MINUTES / 15) {
                            Channel channel = ParseClient.newInstance().fetchChannel(oldest, channelId);
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(channel);
                            realm.commitTransaction();
                        }
                    } finally {
                        realm.close();
                    }
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNEL_REFRESHED, null, channelId));
                } catch (SliidoException e) {
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNEL_REFRESHED, e, channelId));
                }
            }
        }, true);
    }

    public static void findHighSubscribedChannels(final boolean subscribed) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Realm realm = Channel.REALM();
                    List<Channel> channels = ParseClient.newInstance().findChannels(
                            subscribed, true, getTimeFromCachePrefs(subscribed ? CHANNELS_SUBSCRIBED : CHANNELS_NOT_SUBSCRIBED, -1));
                    try {
                        if (channels.size() > 0) {
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(channels);
                            realm.commitTransaction();

                            Collections.sort(channels, channelComparator);
                            updateCacheTime(subscribed ? CHANNELS_SUBSCRIBED : CHANNELS_NOT_SUBSCRIBED, channels.get(0).getUpdatedAt());
                        }
                    } finally {
                        realm.close();
                    }
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNELS_SYNCED, null, EVENT_DATA_FETCH_NEW_CHANNELS));
                } catch (SliidoException e) {
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNELS_SYNCED, e, EVENT_DATA_FETCH_NEW_CHANNELS));
                }
            }
        }, true);
    }

    public static void findLowSubscribedChannels(final boolean subscribed) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Realm realm = Channel.REALM();

                    List<Channel> channels = ParseClient.newInstance().
                            findChannels(subscribed, false, getTimeFromCachePrefs(subscribed ? CHANNELS_SUBSCRIBED : CHANNELS_NOT_SUBSCRIBED, -1));
                    try {
                        if (channels.size() > 0) {
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(channels);
                            realm.commitTransaction();

                            Collections.sort(channels, channelComparator);
                            updateCacheTime(subscribed ? CHANNELS_SUBSCRIBED : CHANNELS_NOT_SUBSCRIBED, channels.get(channels.size() - 1).getUpdatedAt());
                        }
                    } finally {
                        realm.close();
                        realm.close();
                    }
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNELS_SYNCED, null, EVENT_DATA_FETCH_OLD_CHANNELS));
                } catch (SliidoException e) {
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNELS_SYNCED, e, EVENT_DATA_FETCH_OLD_CHANNELS));
                }
            }
        }, true);
    }

    public static void createPost(final String postBody, final String attachment, final String channelId, final String channelName, final UserManager.Callback callback) {
        TaskManager.execute(new Runnable() {
            SliidoException exception = null;

            @Override
            public void run() {
                if (ThreadUtils.isMainThread()) {
                    callback.done(exception);
                } else {
                    CreatePostJob createPostJob = CreatePostJob.create(postBody, attachment, channelId, channelName);
                    TaskManager.runJob(createPostJob);
                    TaskManager.executeOnMainThread(this);
                }
            }
        }, true);
    }

    public static void incrementLikes(final String postId) {

        Realm realm = Post.REALM();
        final boolean isLiked;
        try {
            Post post = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postId).findFirst();
            if (post == null) {
                return;
            }
            isLiked = post.isLiked(); //store this first before changing it
            realm.beginTransaction(); //quickly increment the likes locally and try to increment that of the backend lazily
            if (isLiked) {
                post.setLikes(post.getLikes() - 1);
            } else {
                post.setLikes(post.getLikes() + 1);
            }
            post.setLiked(!isLiked);
            realm.commitTransaction();
        } finally {
            realm.close();
        }
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                TaskManager.cancelJobSync(LikePostJob.generateTag(postId));
                TaskManager.runJob(LikePostJob.create(postId, !isLiked));
            }
        }, true);
    }

    @Nullable
    public static String isValidChannelName(@NonNull String proposedName) {
        if (GenericUtils.isEmpty(proposedName)) {
            return GenericUtils.getString(R.string.field_required);
        }
        if (proposedName.length() < 3) {
            return GenericUtils.getString(R.string.user_name_too_short);
        }
        return null;
    }

    public static void findNewFeeds(@Nullable final String channelID) {
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                //both channel and post use the same realm
                Realm realm = Post.REALM();
                try {
                    List<Post> posts = ParseClient
                            .newInstance().findFeeds(channelID, getTimeFromCachePrefs(POSTS_NEW + channelID, -1), false);
                    if (!posts.isEmpty()) {
                        realm.beginTransaction();
                        for (Post post : posts) {
                            Post tmp = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, post.getPostId()).findFirst();
                            if (tmp == null) {
                                Channel channel = realm.where(Channel.class).equalTo(Channel.FIELD_CHANNEL_ID, post.getChannelId()).findFirst();
                                if (channel != null) {
                                    channel.incrementNewPostCount();
                                }
                            }
                        }
                        realm.copyToRealmOrUpdate(posts);
                        realm.commitTransaction();
                        Collections.sort(posts, postComparator);
                        updateCacheTime(POSTS_NEW + channelID, posts.get(0).getUpdatedAt());
                    } else {
                        PLog.d(TAG, "found no new feeds");
                    }
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_FEEDS_SYNCED, null, EVENT_DATA_FETCH_NEW_POSTS));
                } catch (SliidoException e) {
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_FEEDS_SYNCED, e, EVENT_DATA_FETCH_NEW_POSTS));
                } finally {
                    realm.close();
                }
            }
        }, true);
    }

    public static void findNewFeeds() {
        findNewFeeds(null);
    }


    public static void findOldFeeds(@Nullable final String channelID) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                Realm realm = Post.REALM();
                try {
                    long timeFromCachePrefs = getTimeFromCachePrefs(POST_OLD + channelID, 0);
                    if (timeFromCachePrefs <= 0) {
                        RealmQuery<Post> postRealmQuery = realm.where(Post.class).equalTo(Post.FIELD_STATE, Post.POSTED);
                        if (UserManager.getInstance().isCurrentUserVerified()) {
                            //noinspection ConstantConditions
                            postRealmQuery.
                                    notEqualTo(Post.FIELD_POSTED_BY, UserManager.getInstance().getCurrentUser().getEmail());
                        }
                        if (channelID != null) {
                            postRealmQuery.equalTo(Post.FIELD_CHANNEL_ID, channelID);
                        }
                        Number number = postRealmQuery.min(Post.FIELD_UPDATED_AT);
                        timeFromCachePrefs = number == null ? System.currentTimeMillis() : number.longValue();
                    }
                    if (timeFromCachePrefs <= 0) {
                        PLog.d(TAG, "cannot fetch more ");
                        return;
                    }

                    List<Post> posts = ParseClient.newInstance().findFeeds(channelID,
                            timeFromCachePrefs, true);
                    if (!posts.isEmpty()) {
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(posts);
                        realm.commitTransaction();

                        Collections.sort(posts, postComparator);
                        updateCacheTime(POST_OLD + channelID, posts.get(posts.size() - 1).getUpdatedAt());
                    } else {
                        PLog.d(TAG, "found no old feeds");
                    }
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_FEEDS_SYNCED, null, EVENT_DATA_FETCH_OLD_POSTS));
                } catch (SliidoException e) {
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_FEEDS_SYNCED, e, EVENT_DATA_FETCH_OLD_POSTS));
                } finally {
                    realm.close();
                }
            }
        }, true);
    }


    public static void refreshPost(final String postId) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    refreshPostSync(postId);
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNELS_SYNCED, null, EVENT_DATA_REFRESH_POSTS));
                } catch (SliidoException e) {
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNELS_SYNCED, e, EVENT_DATA_REFRESH_POSTS));
                }
            }
        }, true);
    }

    public static Post getPostSync(String postId) throws SliidoException {
        Realm realm = Post.REALM();

        try {
            Post post = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postId)
                    .findFirst();
            if (post == null) {
                return refreshPostSync(postId);
            }
            return realm.copyFromRealm(post);
        } finally {
            realm.close();
        }
    }

    public static Post refreshPostSync(String postId) throws SliidoException {
        Realm realm = Post.REALM();
        RealmQuery<Post> query = realm.where(Post.class);
        Post post = query.equalTo(Channel.FIELD_CHANNEL_ID, postId).findFirst();
        long time = 0L;
        if (post != null) {
            time = post.getUpdatedAt();
        }
        try {
            post = ParseClient.newInstance().refreshPost(time, postId);
            realm.beginTransaction();
            Post tmp = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postId).findFirst();
            if (tmp == null) {
                Channel channel = realm.where(Channel.class).equalTo(Channel.FIELD_CHANNEL_ID, post.getChannelId()).findFirst();
                if (channel != null) {
                    channel.incrementNewPostCount();
                }
            }
            post = realm.copyToRealmOrUpdate(post);
            realm.commitTransaction();
            return realm.copyFromRealm(post);
        } finally {
            realm.close();
        }
    }

    public static List<String> getSubScribedChannels() {
        return ParseClient.newInstance().getSubscribedChannels();
    }

    private static void findChannelsForCurrentUserSync(final Realm realm) {
        ThreadUtils.ensureNotMain();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    List<Channel> chs = ParseClient.newInstance().fetchChannelsForCurrentUser(getTimeFromCachePrefs(CHANNELS_PERSONAL, -1));
                    if (!chs.isEmpty()) {
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(chs);
                        realm.commitTransaction();

                        Collections.sort(chs, channelComparator);
                        updateCacheTime(CHANNELS_PERSONAL, chs.get(chs.size() - 1).getUpdatedAt());
                    }
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNELS_SYNCED, null, EVENT_DATA_FETCH_OLD_CHANNELS));
                } catch (SliidoException e) {
                    PLog.d(TAG, e.getMessage(), e);
                    EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_CHANNELS_SYNCED, e, EVENT_DATA_FETCH_OLD_CHANNELS));
                }
            }
        };
        runnable.run();
    }

    public static boolean isNotificationsEnabled() {
        return Config.getApplicationWidePrefs().getBoolean(ENABLE_NOTIFICAION, true);
    }

    public static void enableOrDisableNotifications(final boolean enable) {
        enableOrDisableNotifications("", enable);
    }

    public static void enableOrDisableNotifications(@NonNull final String channelID, final boolean enable) {
        if (!UserManager.getInstance().isCurrentUserVerified()) {
            return;
        }
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (ChannelManager.class) {
                    ChannelsApi api = ParseClient.newInstance();
                    try {
                        SharedPreferences applicationWidePrefs = Config.getApplicationWidePrefs();
                        String key = ENABLE_NOTIFICAION + channelID;
                        //first contains(key) check ensures the default value is never used
                        if (applicationWidePrefs.contains(key) && (enable == applicationWidePrefs.getBoolean(key, false))) {
                            PLog.d(TAG, "no change, not updating");
                            return;
                        }
                        if (GenericUtils.isEmpty(channelID)) {
                            api.disableOrEnableNotificationsForAllChannels(enable);
                        } else {
                            api.disableNotificationsForChannel(channelID, enable);
                        }
                        applicationWidePrefs.edit().putBoolean(key, enable).apply();
                    } catch (SliidoException e) {
                        PLog.e(TAG, e.getMessage(), e);
                    }
                }
            }
        }, true);
    }

    public static boolean isNotificationsEnabled(String channelID) {
        return Config.getApplicationWidePrefs().getBoolean(ENABLE_NOTIFICAION + channelID, isNotificationsEnabled());
    }

    public static void addComment(final String postId, final String channelId, final String comment) {
        TaskManager
                .executeNow(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        ChannelsApi client = ParseClient.newInstance();
                                        // FIXME: 10/16/2016
                                        String localCommentID = generateLocalCommentID(channelId, postId);
                                        //noinspection ConstantConditions
                                        Comment comment1 = new Comment.Builder()
                                                .channelID(channelId)
                                                .postID(postId)
                                                .localCommentID(localCommentID)
                                                .commentBody(comment)
                                                .createdAt(System.currentTimeMillis())
                                                .updatedAt(System.currentTimeMillis())
                                                .id(localCommentID)
                                                .postedByEmail(ParseClient.newInstance().getCurrentUser().getEmail())
                                                .postedByName(ParseClient.newInstance().getCurrentUser().getName())
                                                .build();
                                        Realm realm = Post.REALM();
                                        try {
                                            realm.beginTransaction();
                                            realm.copyToRealm(comment1);
                                            Post parentPost = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postId).findFirst();
                                            if (parentPost != null) {
                                                parentPost.setCommentCount(parentPost.getCommentCount() + 1);
                                            }
                                            realm.commitTransaction();
                                            comment1 = client.addCommentToPost(postId, channelId, comment, localCommentID);
                                            realm.beginTransaction();
                                            Comment tmp = realm.where(Comment.class).equalTo(Comment.FIELD_LOCAL_COMMENT_ID, localCommentID).findFirst();
                                            if (tmp != null) {
                                                tmp.deleteFromRealm();
                                            }
                                            realm.copyToRealmOrUpdate(comment1);
                                            realm.commitTransaction();
                                        } finally {
                                            realm.close();
                                        }
                                    } catch (SliidoException e) {
                                        PLog.e(TAG, e.getMessage(), e);
                                        //TODO should we swallow this?
                                    }
                                }
                            }

                        , true);
    }

    private static String generateLocalCommentID(String channelId, String postId) {
        return Comment.POSTING_MARKER_COMMENT_ID + FileUtils.hash(channelId + postId + System.currentTimeMillis() + "" + new SecureRandom().nextLong());
    }

    public static boolean isPosting(Comment comment) {
        return comment.getCommentID().equals(comment.getLocalCommentID());
    }

    public static void loadComments(final String postID, final boolean older) {
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                Realm realm = Post.REALM();
                try {
                    RealmQuery<Comment> query = realm.where(Comment.class).equalTo(Comment.FIELD_POST_ID, postID);
                    Number date = older ? query.min(Comment.FIELD_UPDATED_AT) : query.max(Comment.FIELD_UPDATED_AT);
                    try {
                        List<Comment> comments = ParseClient.newInstance().getPostComments((date != null ? date.longValue() : -1), postID, older);
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(comments);
                        realm.commitTransaction();
                        EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_COMMENTS_LOADED, null, older));
                    } catch (SliidoException e) {
                        PLog.e(TAG, e.getMessage(), e);
                        EventsCenter.getEventBus().postSticky(new EventBus.Event(EVENT_COMMENTS_LOADED, e, older));
                    }
                } finally {
                    realm.close();
                }
            }
        }, true);
    }

    public static void clearNewPostCountAsync(final String channelID, Realm realm) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Channel ch = realm.where(Channel.class).equalTo(Channel.FIELD_CHANNEL_ID, channelID).findFirst();
                if (ch != null) {
                    ch.clearNewPostCount();
                }
            }
        });
    }

    @NonNull
    public static List<String> getPersonalChannels() {
        return ParseClient.newInstance().getPersonalChannels();
    }

    private static long getTimeFromCachePrefs(String key, long defaultValue) {
        return Config.getPreferences(CACHE_PREFS).getLong(key, defaultValue);
    }

    @SuppressLint("CommitPrefEdits")
    public static void updateCacheTime(String key, long value) {
        Config.getPreferences(CACHE_PREFS).edit().putLong(key, value).commit();
    }


    public static void retryPosting(final Post post) {
        final String postBody = post.getPostBody(), channelID = post.getChannelId(),
                channelName = post.getChannelName(), postID = post.getPostId(), attachment = post.getAttachment();
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                CreatePostJob createPostJob = CreatePostJob.create(postBody, attachment, channelID, channelName, postID);
                TaskManager.runJob(createPostJob);
            }
        }, false);
    }

    public static void cancelPost(final Post post) {
        final String postID = post.getPostId();
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                Realm realm = Post.REALM();
                try {
                    Post tmp = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postID).findFirst();
                    if (tmp != null) {
                        realm.beginTransaction();
                        tmp.deleteFromRealm();
                        realm.commitTransaction();
                    }
                } finally {
                    realm.close();
                }
                TaskManager.cancelJobAsync(postID);
            }
        }, false);
    }

    public static rx.Observable<Integer> search(@NonNull final String query) {
        return rx.Observable.create(new rx.Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    if (GenericUtils.isEmpty(query)) {
                        subscriber.onNext(0);
                    } else {
                        final List<Channel> search = ParseClient.newInstance()
                                .search(query, ParseClient.SEARCH_TYPE_CHANNELS);
                        Realm realm = Realm.getDefaultInstance();
                        try {
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(search);
                            realm.commitTransaction();
                        } finally {
                            realm.close();
                        }
                        subscriber.onNext(search.size());
                    }
                    subscriber.onCompleted();
                } catch (SliidoException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }
}
