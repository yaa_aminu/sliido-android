package com.sliido.data.channel;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pairapp.Errors.SliidoException;
import com.sliido.data.post.Comment;
import com.sliido.data.post.Post;

import java.util.List;

/**
 * by null-pointer on 2/24/2016.
 */
public interface ChannelsApi {

    @Nullable
    Channel createChannel(@NonNull String name, String coverPhoto, boolean isPublic) throws SliidoException;

    int subscribeToChannel(@NonNull String channelId) throws SliidoException;

    int unSubscribeToChannel(String channelId) throws SliidoException;

    @NonNull
    List<Channel> findChannels(boolean subscribed, boolean up, long time) throws SliidoException;

    @NonNull
    Channel fetchChannel(long date, String ids) throws SliidoException;

    @Nullable
    Post post(@NonNull String messageBody, @Nullable String attachments, @NonNull String channelId, @NonNull String channelName, @Nullable String title, @Nullable String type, long size) throws SliidoException;

    int likePost(@NonNull String postId) throws SliidoException;

    int unLikePost(@NonNull String postId) throws SliidoException;

    int incrementViews(@NonNull String postId) throws SliidoException;

    @NonNull
    Post refreshPost(long updatedAt, String postId) throws SliidoException;

    @NonNull
    Comment addCommentToPost(String postID, String channelID, String commentBody, String trackingID) throws SliidoException;

    @NonNull
    List<Comment> getPostComments(long updatedAt, String postID, boolean older) throws SliidoException;

    @NonNull
    List<Post> findFeeds(String channelID, long time, boolean older) throws SliidoException;

    @NonNull
    List<Channel> fetchChannelsForCurrentUser(long time) throws SliidoException;

    void disableOrEnableNotificationsForAllChannels(boolean enable) throws SliidoException;

    void disableNotificationsForChannel(String channel, boolean enable) throws SliidoException;
}
