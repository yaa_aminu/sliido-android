package com.sliido.data.channel;

import android.content.Context;
import android.support.annotation.NonNull;

import com.pairapp.util.Config;
import com.pairapp.util.GenericUtils;
import com.sliido.dao.BuildConfig;
import com.sliido.data.DataMigrater;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 * by null-pointer on 2/23/2016.
 */
@RealmClass
public class Channel extends RealmObject {

    public static final String FIELD_CHANNEL_ID = "channelId", FIELD_NAME = "name", FIELD_CREATED_BY = "createdBy", FIELD_CREATED_BY_NAME = "createdByName",
            FIELD_SUBSCRIBERS = "subscribers", FIELD_UPDATED_AT = "updatedAt", FIELD_CREATED_AT = "createdAt";
    public static final String FIELD_SUBSCRIBED = "subscribed", FIELD_NEW_POST_COUNT = "newPostCount";
    public static final String FIELD_IS_SUPER = "isSuper";
    public static final String FIELD_COVER_PHOTO = "coverPhoto", FIELD_IS_PUBLIC = "isPublic";
    private static final RealmConfiguration config;

    static {
        File file = Config.getApplicationContext().getDir("data", Context.MODE_PRIVATE);
        config = new RealmConfiguration.Builder()
                .directory(file)
                .name("channels.realm")
                .schemaVersion(BuildConfig.SCHEMA_VERSION)
                .migration(DataMigrater.get())
                .build();
    }

    @Required
    @PrimaryKey
    private String channelId;
    @Required
    @Index
    private String name;
    @Required
    @Index
    private String createdBy;
    private boolean subscribed;
    @Index
    private int subscribers;
    @Required
    private String createdByName;
    @Index
    private long createdAt;
    @Index
    private long updatedAt;
    private int newPostCount = 0;
    @SuppressWarnings("FieldCanBeLocal")
    private boolean isSuper;

    private String coverPhoto;
    private boolean isPublic;

    public Channel() {
    }

    @NonNull
    public static Channel create(@NonNull String channelId, @NonNull String name,
                                 @NonNull String createdBy,
                                 @NonNull String createdByName, int subscribers, boolean isSuper, String coverPhoto, boolean isPublic) {
        GenericUtils.ensureNotEmpty(channelId, name, createdBy, createdByName);
        GenericUtils.ensureConditionTrue(subscribers >= 0, "invalid value for number of subscribers");
        Channel channel = new Channel();
        channel.setChannelId(channelId);
        channel.setName(name);
        channel.setCreatedBy(createdBy);
        channel.setCreatedByName(createdByName);
        channel.setPublic(isPublic);
        channel.setCoverPhoto(coverPhoto);
        channel.setSubscribers(subscribers);
        channel.setSuper(isSuper);
        return channel;
    }

    public static Realm REALM() {
        return Realm.getInstance(config);
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public int getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(int subscribers) {
        this.subscribers = subscribers;
    }

    public String getCreatedByName() {
        return createdByName;
    }

    public void setCreatedByName(String createdByName) {
        this.createdByName = createdByName;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getNewPostCount() {
        return newPostCount;
    }

    public void clearNewPostCount() {
        this.newPostCount = 0;
    }

    void incrementNewPostCount() {
        this.newPostCount++;
    }

    public void setSuper(boolean isSuper) {
        this.isSuper = isSuper;
    }

    public boolean isSubscribing() {
        return ChannelManager.isSubScribing(getChannelId());
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        this.isPublic = aPublic;
    }
}
