package com.sliido.data;

import android.support.annotation.NonNull;

import com.pairapp.util.PLog;
import com.pairapp.util.Task;
import com.path.android.jobqueue.Params;
import com.path.android.jobqueue.RetryConstraint;
import com.sliido.data.channel.ChannelsApi;
import com.sliido.data.post.Post;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.realm.Realm;

/**
 * Created by yaaminu on 11/6/17.
 */

public class LikePostJob extends Task {
    private static final String TAG = "LikePostJob";
    private String postId;
    private boolean like;

    public LikePostJob() {
    }

    private LikePostJob(Params params, String postId, boolean like) {
        super(params);
        this.postId = postId;
        this.like = like;
    }

    public static LikePostJob create(String postId, boolean like) {
        Params params = new Params(100)
                .setRequiresNetwork(true)
                .addTags(generateTag(postId))
                .setGroupId("likePost.group." + postId)
                .setPersistent(true);
        return new LikePostJob(params, postId, like);
    }

    @NonNull
    public static String generateTag(String postId) {
        return "like.tag." + postId;
    }

    @Override
    protected JSONObject toJSON() throws JSONException {
        return new JSONObject().put("postId", postId).put("like", like);
    }

    @Override
    protected Task fromJSON(JSONObject jsonObject) throws JSONException {
        return create(jsonObject.getString("postId"), jsonObject.getBoolean("like"));
    }

    @Override
    public void onAdded() {
        PLog.d(TAG, "added job: %s", this);
    }

    @Override
    public void onRun() throws Throwable {
        PLog.d(TAG, "running job: %s", this);
        ChannelsApi api = ParseClient.newInstance();
        Realm realm = Post.REALM();
        Post post = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postId).findFirst();
        if (post != null) {
            int likes = like ? api.likePost(postId) : api.unLikePost(postId);
            realm.beginTransaction();
            post.setLikes(likes);
            post.setLiked(like);
            realm.commitTransaction();
            realm.close();
        }
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount, int maxRunCount) {
        PLog.d(TAG, throwable.getMessage(), throwable);
        return RetryConstraint.createExponentialBackoff(runCount, TimeUnit.SECONDS.toMillis(throwable instanceof RuntimeException ? 120 : 30));
    }

    @Override
    protected int getRetryLimit() {
        return 10000;
    }

    @Override
    protected void onCancel() {
        PLog.d(TAG, "cancelled job: %s", this);
    }

    @Override
    public String toString() {
        return "LikePostJob{" +
                "postId='" + postId + '\'' +
                ", like=" + like +
                '}';
    }
}
