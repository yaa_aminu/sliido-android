package com.sliido.data.post;

import android.support.annotation.Nullable;

import com.pairapp.util.GenericUtils;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by aminu on 10/16/2016.
 */

public class Comment extends RealmObject {

    public static final String POSTING_MARKER_COMMENT_ID = "posting_";

    public static String FIELD_CREATED_AT = "createdAt", FIELD_UPDATED_AT = "updatedAt", FIELD_COMMENT_ID = "commentID",
            FIELD_POST_ID = "postID", FIELD_LOCAL_COMMENT_ID = "localCommentID", FIELD_COMMENT_BODY = "commentBody", FIELD_POSTED_BY_EMAIL = "postedByEmail",
            FIELD_POSTED_BY_NAME = "postedByName", FIELD_CHANNEL_ID = "channelD";
    @Index
    private long createdAt;
    private long updatedAt;
    @Required
    private String postedByName;
    @Required
    private String postedByEmail;
    @Index
    @Required
    private String postID;
    @Required
    private String commentBody;
    @Required
    private String channelID;

    @PrimaryKey
    @Required
    private String commentID;

    @Index
    private String localCommentID;

    @Nullable
    public String getLocalCommentID() {
        return localCommentID;
    }

    public String getCommentID() {
        return commentID;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public String getChannelID() {
        return channelID;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String getPostedByName() {
        return postedByName;
    }

    public String getPostID() {
        return postID;
    }

    public static class Builder {
        Comment buildingComment;

        public Builder() {
            buildingComment = new Comment();
        }

        public Builder id(String commentID) {
            GenericUtils.ensureNotEmpty(commentID);
            buildingComment.commentID = commentID;
            return this;
        }

        public Builder postID(String postID) {
            GenericUtils.ensureNotEmpty(postID);
            buildingComment.postID = postID;
            return this;
        }

        public Builder channelID(String channelID) {
            GenericUtils.ensureNotEmpty(channelID);
            buildingComment.channelID = channelID;
            return this;
        }

        public Builder postedByName(String postedByName) {
            GenericUtils.ensureNotEmpty(postedByName);
            buildingComment.postedByName = postedByName;
            return this;
        }

        public Builder postedByEmail(String postedByEmail) {
            GenericUtils.ensureNotEmpty(postedByEmail);
            buildingComment.postedByEmail = postedByEmail;
            return this;
        }

        public Builder commentBody(String commentBody) {
            GenericUtils.ensureNotEmpty(commentBody);
            buildingComment.commentBody = commentBody;
            return this;
        }

        public Builder createdAt(long createdAt) {
            GenericUtils.ensureConditionTrue(createdAt > 0, "invalid time");
            buildingComment.createdAt = createdAt;
            return this;
        }

        public Builder updatedAt(long updatedAt) {
            GenericUtils.ensureConditionTrue(updatedAt > 0, "invalid time");
            buildingComment.updatedAt = updatedAt;
            return this;
        }

        public Builder localCommentID(@Nullable String localCommentID) {
//            GenericUtils.ensureNotEmpty(localCommentID);
            buildingComment.localCommentID = localCommentID;
            return this;
        }

        public Comment build() {
            GenericUtils.ensureConditionTrue(buildingComment.updatedAt > 0, "invalid time");
            GenericUtils.ensureConditionTrue(buildingComment.createdAt > 0, "invalid time");
            GenericUtils.ensureNotEmpty(buildingComment.commentBody);
            GenericUtils.ensureNotEmpty(buildingComment.commentID);
            GenericUtils.ensureNotEmpty(buildingComment.postID);
            GenericUtils.ensureNotEmpty(buildingComment.channelID);
            GenericUtils.ensureNotEmpty(buildingComment.postedByEmail);
            GenericUtils.ensureNotEmpty(buildingComment.postedByName);
            return buildingComment;
        }
    }

}
