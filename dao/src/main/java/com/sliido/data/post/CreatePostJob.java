package com.sliido.data.post;

import android.net.Uri;
import android.os.SystemClock;
import android.text.TextUtils;

import com.pairapp.Errors.SliidoException;
import com.pairapp.net.FileApi;
import com.pairapp.net.FileClientException;
import com.pairapp.util.FileUtils;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.Task;
import com.path.android.jobqueue.Params;
import com.path.android.jobqueue.RetryConstraint;
import com.sliido.data.ParseClient;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;

import io.realm.Realm;

/**
 * Created by aminu on 11/6/2016.
 */

public class CreatePostJob extends Task {

    public static final String TAG = CreatePostJob.class.getSimpleName();
    static final int PRIORITY = 100;
    static final int RETRY_LIMIT = 50;
    static final String TEST = "test";

    static String env = null;

    private final String postBody, channelID, channelName, postID;
    private final FileApi.ProgressListener progressListener = new FileApi.ProgressListener() {
        @Override
        public void onProgress(long expected, long transferred) {
            PLog.d(TAG, "expected: %d, uploaded: %d, %d%%", expected, transferred, (transferred * 100) / expected);
        }
    };
    private String attachment;

    public CreatePostJob() { //required no-arg c'tor
        this.postBody = "";
        this.channelName = "";
        this.channelID = "";
        this.postID = "";
    }

    private CreatePostJob(Params params, String postBody, String attachment, String channelID, String channelName, String postID) {
        super(params);
        this.postBody = postBody;
        this.channelID = channelID;
        this.channelName = channelName;
        this.postID = postID;
        this.attachment = attachment;
    }

    static String generateLocalPostID(String channelId, String channelName) {
        return FileUtils.hash(channelId + channelName + (TEST.equals(env) ? System.currentTimeMillis() : SystemClock.uptimeMillis()) + "" + new SecureRandom().nextLong());
    }

    public static CreatePostJob create(String postBody, String attachments, String channelID, String channelName) {
        return create(postBody, attachments, channelID, channelName, generateLocalPostID(channelID, channelName));
    }

    public static CreatePostJob create(String postBody, String attachments, String channelID, String channelName, String postID) {
        GenericUtils.ensureNotEmpty(postBody, channelID, channelName);
        Params params = new Params(100);
        params.setRequiresNetwork(true);
        params.setGroupId("newPosts");
        params.addTags(postID);
        params.persist();
        return new CreatePostJob(params, postBody, attachments, channelID, channelName, postID);
    }

    String getPostID() {
        return postID;
    }

    String getChannelID() {
        return channelID;
    }

    String getChannelName() {
        return channelName;
    }

    String getPostBody() {
        return postBody;
    }

    @Override
    protected JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Post.FIELD_POST_BODY, postBody);
        jsonObject.put(Post.FIELD_CHANNEL_ID, channelID);
        jsonObject.put(Post.FIELD_CHANNEL_NAME, channelName);
        jsonObject.put(Post.FIELD_POST_ID, postID);
        jsonObject.put(Post.FIELD_ATTACHMENT, attachment);
        return jsonObject;
    }

    @Override
    protected CreatePostJob fromJSON(JSONObject jsonObject) throws JSONException {
        return create(jsonObject.getString(Post.FIELD_POST_BODY), jsonObject.optString(Post.FIELD_ATTACHMENT),
                jsonObject.getString(Post.FIELD_CHANNEL_ID), jsonObject.getString(Post.FIELD_CHANNEL_NAME),
                jsonObject.getString(Post.FIELD_POST_ID));
    }

    @Override
    public void onAdded() {
        PLog.d(TAG, "post with id: %s  added", postID);
        Post localPost = new Post();
        localPost.setUpdatedAt(System.currentTimeMillis());
        localPost.setCreatedAt(System.currentTimeMillis());
        localPost.setPostId(postID);
        localPost.setState(Post.POSTING);
        localPost.setChannelId(channelID);
        localPost.setChannelName(channelName);
        localPost.setChannelId(channelID);
        if (!TextUtils.isEmpty(attachment)) {
            // TODO: 11/8/2016 copy the attachment to a different dir and also give it a unique name so
            //that it does not overwrite an existing file with same name
            File srcFile = new File(attachment);
            localPost.setType(Post.extensionToType(attachment));
            localPost.setAttachmentSize(srcFile.length());
            localPost.setAttachmentTitle(srcFile.getName());
            localPost.setExtension(FileUtils.getExtension(attachment));
            localPost.setAttachment(attachment);
        }
        localPost.setPostBody(postBody);
        localPost.setLiked(false);
        User user = UserManager.getInstance().getCurrentUser();
        //noinspection ConstantConditions
        localPost.setPostedBy(user.getEmail());
        localPost.setPostedByName(user.getName());
        Realm realm = Post.REALM();
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(localPost);
            realm.commitTransaction();
        } finally {
            realm.close();
        }
    }

    @Override
    public void onRun() throws Throwable {
        PLog.d(TAG, "posting with the post with post body %s", postBody);
        Realm realm = Post.REALM();
        try {
            Post post;


            Uri uri = null;
            String title = null, type = null;
            long size = 0;
            if (!TextUtils.isEmpty(attachment)) {
                File file = new File(attachment);
                if (file.exists()) {
                    FileApi api = ParseClient.newInstance();
//                            SmartFileClient.getInstance("IMffUgePk1EZl5ZTY7SwzvddSTJ8VD", "7Gvort1GgExTBHSIrTvobufrgFMD6h", FileUtils.hash(channelID));
                    size = file.length();
                    title = file.getName();
                    type = FileUtils.getExtension(file.getAbsolutePath());
                    uri = Uri.parse(api.saveFileToBackendSync(file, progressListener));
                    uri = uri.buildUpon().appendQueryParameter(Post.SIZE, "" + size).build();
                }
            }
            post = ParseClient.newInstance().post(postBody, uri != null ? uri.toString() : "", channelID, channelName, title, type, size);

            assert post != null;
            Post tmp = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postID).findFirst();
            realm.beginTransaction();
            if (tmp != null) {
                tmp.deleteFromRealm();
            }
            realm.copyToRealmOrUpdate(post);
            realm.commitTransaction();
        } finally {
            realm.close();
        }
    }

    @Override
    protected void onCancel() {
        if (getCurrentRunCount() < getRetryLimit()) {
            PLog.d(TAG, "post with post body %s cancelled", postBody);
        } else {
            PLog.d(TAG, "post with post body %s failed permanently", postBody);
        }
        Realm realm = Post.REALM();
        Post tmp = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postID).findFirst();
        if (tmp != null) {
            realm.beginTransaction();
            tmp.setState(Post.POST_FAILED);
            realm.commitTransaction();
        }
    }

    @Override
    protected int getRetryLimit() {
        return 50;
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(Throwable throwable, int runCount, int maxRunCount) {
        PLog.w(TAG, "creating post encountered an error %s", throwable.getMessage());
        PLog.w(TAG, throwable.getMessage(), throwable);

        if (throwable instanceof SliidoException) {
            if (((SliidoException) throwable).getErrorCode() == SliidoException.EUNAUTHORIZED) {
                return RetryConstraint.CANCEL;
            }
            return RetryConstraint.createExponentialBackoff(runCount, 200);
        }
        if (throwable instanceof OutOfMemoryError) {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        }
        if (throwable instanceof FileClientException) {
            if (throwable.getCause() != null) {
                if (throwable.getCause() instanceof IOException) {
                    return RetryConstraint.createExponentialBackoff(runCount, 1000);
                }
                if (throwable.getCause() instanceof Error) {
                    return RetryConstraint.createExponentialBackoff(runCount, 10000);
                }
            }
            return RetryConstraint.CANCEL;
        }
        PLog.w(TAG, "unknown raised while posting, cancelling job");
        return RetryConstraint.CANCEL;
    }
}
