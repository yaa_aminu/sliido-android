package com.sliido.data.post;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pairapp.util.Config;
import com.pairapp.util.GenericUtils;
import com.sliido.data.channel.Channel;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 * by null-pointer on 2/24/2016.
 */
@RealmClass
public class Post extends RealmObject {

    public static final int POST_FAILED = -1, POSTING = 0, POSTED = 1;
    public static final String FIELD_POST_BODY = "postBody",
            FIELD_CHANNEL_ID = "channelId",
            FIELD_VIEWS = "views",
            FIELD_LIKES = "likes",
            FIELD_DATE_CREATED = "createdAt",
            FIELD_POSTED_BY = "postedBy",
            FIELD_POSTED_BY_NAME = "postedByName",
            FIELD_POST_ID = "postId",
            FIELD_CHANNEL_NAME = "channelName",
            FIELD_UPDATED_AT = "updatedAt",
            FIELD_COMMENT_COUNT = "commentCount",
            FIELD_STATE = "state",
            FIELD_ATTACHMENT = "attachment", FEILD_SIZE = "attachmentSize",
            FEILD_TITLE = "attachmentTitle";

    public static final int MIN_POST_LENGTH = 10;
    public static final String SIZE = "size";
    public static final long MAX_ATTACHMENT_SIZE = FileUtils.ONE_MB * 16;
    public static final String FIELD_WHITE_LISTED = "whiteListed";
    public static final int OTHER = 0,
            IMAGE = 1, VIDEO = 2, AUDIO = 3;
    public static final String FIELD_LIKERS = "likers";
    private String extension;
    private int state;
    @Required
    @PrimaryKey
    private String postId;
    @Required
    private String postBody;
    @Required
    @Index
    private String channelId;
    @Required
    private String channelName;
    @Required
    @Index
    private String postedBy;
    @Required
    private String postedByName;
    private int views;
    private int likes;
    @Index
    private long createdAt;
    private boolean liked;
    @Index
    private long updatedAt;
    private String attachment, attachmentTitle;
    private long commentCount, attachmentSize;
    private String commenters;
    private String likers;
    private int type;

    public static Realm REALM() {
        return Channel.REALM();
    }

    public static int extensionToType(String path) {
        String mimeType = com.pairapp.util.FileUtils.getMimeType(path);
        if (GenericUtils.isEmpty(mimeType)) {
            return OTHER;
        }
        if (mimeType.startsWith("image/")) {
            return IMAGE;
        } else if (mimeType.startsWith("video/")) {
            return VIDEO;
        } else if (mimeType.startsWith("audio/")) {
            return AUDIO;
        } else {
            return OTHER;
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String messageBody) {
        this.postBody = messageBody;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes < 0 ? 0 : likes;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    public String getPostedByName() {
        return postedByName;
    }

    public void setPostedByName(String postedByName) {
        this.postedByName = postedByName;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public boolean isLiked() {
        return this.liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Nullable
    public String getAttachmentTitle() {
        return attachmentTitle;
    }

    public void setAttachmentTitle(String title) {
        this.attachmentTitle = title;
    }

    public long getAttachmentSize() {
        return attachmentSize;
    }

    public void setAttachmentSize(long attachmentSize) {
        this.attachmentSize = attachmentSize;
    }

    @Nullable
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @NonNull
    public String getCommenters() {
        return commenters == null ? "" : commenters;
    }

    public void setCommenters(String commenters) {
        this.commenters = commenters;
    }

    @NonNull
    public String getLikers() {
        return likers == null ? "" : likers;
    }

    public void setLikers(String likers) {
        this.likers = likers;
    }

    @Nullable
    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public boolean isDownloaded() {
        try {
            return !GenericUtils.isEmpty(getAttachment()) && (new File(getAttachment()).exists() || new File(Config.getAppBinFilesBaseDir(), getChannelName() + "_" + getAttachmentTitle()).exists());
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    @Nullable
    public String getPath() {
        try {
            File file = new File(getAttachment());
            if (file.exists()) {
                return file.getAbsolutePath();
            }
            return new File(Config.getAppBinFilesBaseDir(), getChannelName() + "_" + getAttachmentTitle()).getAbsolutePath();
        } catch (FileNotFoundException e) {
            return null;
        }
    }
}
