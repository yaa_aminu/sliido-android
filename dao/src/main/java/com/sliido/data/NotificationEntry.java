package com.sliido.data;

import android.content.Context;

import com.sliido.dao.R;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by yaaminu on 11/3/17.
 */

public class NotificationEntry extends RealmObject {
    public static final int TYPE_COMMENT = 1, TYPE_LIKE = 2;
    private int type;
    private String data;
    private long date;
    @PrimaryKey
    private String id;
    private String postId;
    private boolean read;
    private int notificationId;

    @SuppressWarnings("unused")
    public NotificationEntry() { //required no-arg c'tor
    }

    public NotificationEntry(String id, String postId, int type, String data, long date, int notificationId) {
        this.setId(id);
        this.setType(type);
        this.setData(data);
        this.setDate(date);
        this.setPostId(postId);
        this.setNotificationId(notificationId);
        this.read = false;
    }


    public String getText(Context context) {
        if (getType() == TYPE_COMMENT) {
            return context.getString(R.string.commented_on_post, summarizeNames());
        } else if (getType() == TYPE_LIKE) {
            return context.getString(R.string.like_post, summarizeNames());
        } else {
            throw new AssertionError();
        }
    }

    private String summarizeNames() {
        String[] names = getData().split(",");
        if (names.length > 3) {
            return names[0].split("\\s+")[0] + ", " + names[1].split("\\s+")[0] + ", " + names[2].split("\\s+")[0]
                    + " and " + (names.length - 3) + " others";
        } else if (names.length == 3) {
            return names[0].split("\\s+")[0] + ", " + names[1].split("\\s+")[0] + " and " + names[2].split("\\s+")[0];
        } else if (names.length == 2) {
            return names[0].split("\\s+")[0] + " and " + names[1].split("\\s+")[0];
        } else if (names.length == 1) {
            return names[0].split("\\s+")[0];
        } else {
            return "";
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }
}
