package com.sliido.data;

import com.sliido.dao.data.ShoutOut;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;

/**
 * Created by yaaminu on 11/15/17.
 */

public class DataMigrater {

    public static RealmMigration get() {
        return new Migration();
    }

    private static final class Migration implements RealmMigration {

        @Override
        public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
            if (oldVersion < 2) {
                final RealmObjectSchema shoutSchema = realm.getSchema().get(ShoutOut.class.getSimpleName());
                if (shoutSchema != null) {
                    migrateShoutoutToV2(shoutSchema);
                }
            }
            if (oldVersion < 3) {
                final RealmObjectSchema shoutSchema = realm.getSchema().get(ShoutOut.class.getSimpleName());
                if (shoutSchema != null) {
                    migrateShoutoutToV3(shoutSchema);
                }
            }
        }

        private void migrateShoutoutToV2(RealmObjectSchema schema) {
            schema.addField("updatedAt", long.class);
            schema.addField("createdAt", long.class);
            schema.addField("likes", int.class);
            schema.addIndex("postedBy");
        }

        private void migrateShoutoutToV3(RealmObjectSchema schema) {
            schema.addField("liked", boolean.class);
            schema.addField("reposts", int.class);
            schema.addField("state", int.class);
            schema.addField("parentPost", String.class);
        }
    }
}
