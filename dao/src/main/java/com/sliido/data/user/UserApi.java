package com.sliido.data.user;

import com.pairapp.Errors.SliidoException;

import java.util.List;

/**
 * author Null-Pointer on 1/12/2016.
 */
public interface UserApi {
    User signUpOrLogin(String username, String email, String password) throws SliidoException;

    List<User> getUsers(String... emails) throws SliidoException;

    boolean isCurrentUserVerified();

    User getCurrentUser();

    void verifyEmail() throws SliidoException;

    void deleteUnverifiedAccount() throws SliidoException;

    void resetPassword(String email) throws SliidoException;

    List<String> getSubscribedChannels();

    List<String> getPersonalChannels();

    void logOut() throws SliidoException;

    List<String> getNames(List<String> emails) throws SliidoException;
}
