package com.sliido.data.user;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.pairapp.Errors.SliidoException;
import com.pairapp.util.Config;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.TaskManager;
import com.pairapp.util.ThreadUtils;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.sliido.dao.BuildConfig;
import com.sliido.dao.R;
import com.sliido.data.ParseClient;
import com.sliido.data.channel.Channel;
import com.sliido.data.post.Post;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import io.realm.Realm;

import static com.pairapp.util.EventBus.Event;

/**
 * author Null-Pointer on 1/11/2016.
 */
public class UserManager {

    public static final String VERIFICATION_EVENT = "VERIFICATION_EVENT";
    public static final String DELETEACCOUNT_EVENT = "deleteaccountEvent";
    public static final String LOGOUT_EVENT = "loutEvent";
    public static final String LOGIN_EVENT = "loginEvent";
    private static final String TAG = "UserManager";
    private static UserManager INSTANCE;
    private final UserApi userApi;
    private boolean isBusy;

    private UserManager() {
        userApi = ParseClient.newInstance();
    }

    public static UserManager getInstance() {
        if (INSTANCE == null) {
            synchronized (UserManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new UserManager();
                }
            }
        }
        return INSTANCE;
    }

    public static String isValidUserName(String userName) {
        GenericUtils.ensureNotNull(userName);
        return userName.matches("^[A-Za-z][\\w \\-']{4,25}") ? null : GenericUtils.getString(R.string.name_invalid);
    }

    public static String isValidPassword(String password) {
        GenericUtils.ensureNotNull(password);
        //TODO: Replace this
        return password.length() > 4 ? null : GenericUtils.getString(R.string.password_invalid);
    }

    public static String isValidEmail(String email) {
        GenericUtils.ensureNotNull(email);
        // TODO: 1/16/2016 replace this
        return email.contains("@") && email.length() > 5 ? null : GenericUtils.getString(R.string.email_invalid);
    }

    public void login(final String email, final String password) {
        GenericUtils.ensureNotNull(email, password);
        Event event;
        if (userApi.getCurrentUser() != null) {
            throw new IllegalStateException();
        }
        if (isBusy) {
            if (BuildConfig.DEBUG) {
                throw new IllegalStateException("signUpOrLogin already in progress");
            }
            event = new Event(LOGIN_EVENT, new SliidoException(GenericUtils.getString(R.string.app_busy)), null);
            EventsCenter.getEventBus().postSticky(event);
            return;
        }

        String errorMessage = null;
        if (isValidEmail(email) != null) {
            errorMessage = isValidEmail(email);
        } else if (isValidPassword(password) != null) {
            errorMessage = isValidPassword(password);
        }
        if (errorMessage != null) {
            event = new Event(LOGIN_EVENT, new SliidoException(errorMessage), null);
            EventsCenter.getEventBus().postSticky(event);
        } else {
            continueSetup(null, email, password);
        }
    }

    private void continueSetup(final String userName, final String email, final String password) {
        if (isBusy) {
            return;
        }
        isBusy = true;
        Event event;
        try {
            User user = userApi.signUpOrLogin(userName, email, password);
            event = new Event(LOGIN_EVENT, null, user);
            EventsCenter.getEventBus().postSticky(event);
        } catch (SliidoException e) {
            event = new Event(LOGIN_EVENT, e, null);
            EventsCenter.getEventBus().postSticky(event);
        } finally {
            isBusy = false;
        }
    }

    public void signUp(String username, String email, String password) {
        Event event;
        if (userApi.getCurrentUser() != null) {
            throw new IllegalStateException();
        }
        if (isBusy) {
            if (BuildConfig.DEBUG) {
                throw new IllegalStateException("signUpOrLogin already in progress");
            }
            event = new Event(LOGIN_EVENT, new SliidoException(GenericUtils.getString(R.string.app_busy)), null);
            EventsCenter.getEventBus().postSticky(event);
            return;
        }

        String errorMessage = null;
        if (isValidUserName(username) != null) {
            errorMessage = isValidUserName(username);
        } else if (isValidEmail(email) != null) {
            errorMessage = isValidEmail(email);
        } else if (isValidPassword(password) != null) {
            errorMessage = isValidPassword(password);
        }
        if (errorMessage != null) {
            event = new Event(LOGIN_EVENT, new SliidoException(errorMessage), null);
            EventsCenter.getEventBus().postSticky(event);
        } else {
            continueSetup(username, email, password);
        }
    }

    @Nullable
    public User getCurrentUser() {
        return userApi.getCurrentUser();
    }

    public boolean isCurrentUserVerified() {
        return userApi.isCurrentUserVerified();
    }

    public void verifyCurrentUser() {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                Event event;
                try {
                    userApi.verifyEmail();
                    event = new Event(VERIFICATION_EVENT, null, getCurrentUser());
                    EventsCenter.getEventBus().postSticky(event);
                } catch (SliidoException e) {
                    event = new Event(VERIFICATION_EVENT, e, null);
                    EventsCenter.getEventBus().postSticky(event);
                }
            }
        }, true);
    }

    public void reset() {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                Event event;
                try {
                    userApi.deleteUnverifiedAccount();
                    event = new Event(DELETEACCOUNT_EVENT, null, null);
                    EventsCenter.getEventBus().postSticky(event);
                } catch (SliidoException e) {
                    event = new Event(DELETEACCOUNT_EVENT, e, null);
                    EventsCenter.getEventBus().postSticky(event);
                }
            }
        }, true);
    }

    public void resetPassword(@NonNull final String email, @NonNull final Callback callback) {
        GenericUtils.ensureNotNull(email, callback);
        TaskManager.execute(new Runnable() {
            SliidoException exception = null;

            @Override
            public void run() {
                if (ThreadUtils.isMainThread()) { //second call see below
                    callback.done(exception);
                } else {
                    try {
                        userApi.resetPassword(email);
                    } catch (SliidoException e) {
                        exception = e;
                    }
                    TaskManager.executeOnMainThread(this); //re-use this runnable
                }
            }
        }, true);
    }

    public List<String> getSubscribedChannels() {
        return userApi.getSubscribedChannels();
    }

    public void logout(final Callback callback) {
        GenericUtils.ensureNotNull(callback);
        TaskManager.execute(new Runnable() {
            SliidoException exception = null;

            @Override
            public void run() {
                if (ThreadUtils.isMainThread()) {
                    callback.done(exception);
                } else {
                    try {
                        userApi.logOut();
                        EventsCenter.getEventBus().post(new Event(LOGOUT_EVENT, null, null));
                        Realm realm = Channel.REALM();
                        try {
                            realm.beginTransaction();
                            realm.deleteAll();
                            realm.commitTransaction();
                        } finally {
                            realm.close();
                        }
                        realm = Post.REALM();
                        try {
                            realm.beginTransaction();
                            realm.deleteAll();
                            realm.commitTransaction();
                        } finally {
                            realm.close();
                        }
                        clearAllPrefs();
                    } catch (SliidoException e) {
                        exception = e;
                    }
                    TaskManager.executeOnMainThread(this);
                }
            }
        }, true);
    }

    @SuppressLint("CommitPrefEdits")
    private void clearAllPrefs() {
        try {
            File directory = new File(Config.getApplication().getFilesDir().getParent(), "shared_prefs");
            if (directory.isDirectory()) {
                String[] files = directory.list();
                if (files != null) {
                    for (String file : files) {
                        if (file.endsWith(".xml")) {
                            // strip off the .xml from the name
                            SharedPreferences preferences = Config.getPreferences(file.substring(0, file.length() - 4));
                            preferences.edit().clear().commit();
                        }
                        if (!new File(directory, file).delete()) {
                            PLog.w(TAG, "failed to delete file: %s", file);
                        } else {
                            PLog.w(TAG, "deleted file: %s", file);
                        }
                    }
                }
            }
            FileUtils.deleteDirectory(directory);
        } catch (IOException e) {
            PLog.e(TAG, e.getMessage(), e);
            //ignore
        }
    }

    public void completeFacebookRegistration(@Nullable ParseUser user, @NonNull Callback callback) {
        if (user == null) {
            PLog.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
            callback.done(new SliidoException("Login cancelled"));
        } else if (user.getString("name") == null || user.getEmail() == null || !user.getBoolean("emailVerified")) {
            getUserDetailFromFb(user, callback);
        } else {
            PLog.d("MyApp", "User logged in through Facebook!");
            callback.done(null);
        }
    }


    private void getUserDetailFromFb(final ParseUser user, final Callback callback) {
        final GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    if (response.getError() != null) {
                        throw new ParseException(response.getError().getException());
                    }
                    saveUser(object, user, callback);
                } catch (ParseException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    callback.done(new SliidoException(e.getMessage()));
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void saveUser(final JSONObject object, final ParseUser user, final Callback callback) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    user.put(User.FIELD_NAME, object.getString(User.FIELD_NAME));
                    user.put(User.FIELD_EMAIL, object.getString(User.FIELD_EMAIL));
                    user.put("verified", true);
                    user.save();
                    executeOnMainThread(callback, null);
                } catch (ParseException | JSONException e) {
                    executeOnMainThread(callback, e);
                }
            }
        }, true);
    }

    private void executeOnMainThread(final Callback callback, @Nullable final Exception e) {
        TaskManager.executeOnMainThread(new Runnable() {
            @Override
            public void run() {
                callback.done(e == null ? null : new SliidoException(e.getMessage()));
            }
        });
    }

    public List<String> getNames(List<String> emails) throws SliidoException {
        return userApi.getNames(emails);
    }

    public interface Callback {
        void done(SliidoException e);
    }
}
