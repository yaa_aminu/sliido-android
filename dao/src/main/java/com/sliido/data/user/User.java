package com.sliido.data.user;

import android.content.Context;
import android.support.annotation.NonNull;

import com.pairapp.util.Config;
import com.pairapp.util.GenericUtils;
import com.sliido.dao.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;
import io.realm.annotations.Required;

/**
 * author Null-Pointer on 1/11/2016.
 */
@RealmClass
public class User extends RealmObject {
    public static final String FIELD_NAME = "name",
            FIELD_EMAIL = "email";


    @Required
    @Index
    private String name;
    @Required
    @PrimaryKey
    private String email;

    public User() {
    }

    public User(String email, String name) {
        GenericUtils.ensureNotNull(email);
        GenericUtils.ensureNotNull(name);
        this.email = email;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @NonNull
    static User copy(@NonNull User user) {
        return copy(user, false);
    }


    @NonNull
    static List<User> copy(@NonNull Collection<User> users, boolean deepCopy) {
        List<User> copy = new ArrayList<>();
        for (User user : users) {
            copy.add(copy(user, deepCopy));
        }
        return copy;
    }

    @NonNull
    static List<User> copy(@NonNull Collection<User> users) {
        return copy(users, false);
    }


    @NonNull
    static User copy(@NonNull User user, boolean deepCopy) {
        GenericUtils.ensureNotNull(user);
        if (!user.isValid() && !deepCopy) {
            return user;
        }
        return internalCopy(user);
    }

    private static User internalCopy(User user) {
        User copy = new User();
        copy.setName(user.getName());
        copy.setEmail(user.getEmail());
        return copy;
    }

    @NonNull
    static JSONObject toJSON(@NonNull User user) {
        GenericUtils.ensureNotNull(user);
        try {
            JSONObject object = new JSONObject();
            object.put(User.FIELD_NAME, user.getName());
            object.put(User.FIELD_EMAIL, user.getEmail());
            return object;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @NonNull
    static User fromJSON(@NonNull JSONObject object) {
        GenericUtils.ensureNotNull(object);
        try {
            User user = new User();
            user.setName(object.getString(FIELD_NAME));
            user.setEmail(object.getString(User.FIELD_EMAIL));
            return user;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static Realm REALM() {
        return Realm.getInstance(config);
    }

    private static final RealmConfiguration config;

    static {
        File file = Config.getApplicationContext().getDir("data", Context.MODE_PRIVATE);
        config = new RealmConfiguration.Builder()
                .directory(file)
                .name("userstore.realm")
                .schemaVersion(BuildConfig.VERSION_CODE)
                .deleteRealmIfMigrationNeeded().build();
    }
}
