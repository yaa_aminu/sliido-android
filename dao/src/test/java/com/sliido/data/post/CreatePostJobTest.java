package com.sliido.data.post;

import com.pairapp.util.PLog;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by aminu on 11/6/2016.
 */
@SuppressWarnings("WeakerAccess")
public class CreatePostJobTest {

    public static final String POST_BODY = "postBody";
    private static final String CHANNEL_ID = "channelID";
    public static final String CHANNEL_NAME = "channelName";
    public static final String POST_ID = "postID";

    @Before
    public void setup() {
        //harks to force the test to run without using any android api
        CreatePostJob.env = CreatePostJob.TEST;
        PLog.setLogLevel(PLog.LEVEL_NONE);
    }

    @Test
    public void create() throws Exception {
        testMustHandleIllegalArgs();
        CreatePostJob job = CreatePostJob.create(POST_BODY, CHANNEL_ID, CHANNEL_NAME, POST_ID);
        assertJobWellformed(job);
    }

    @Test
    public void create2() throws Exception {
        testMustHandleIllegalArgs();
        CreatePostJob job = CreatePostJob.create(POST_BODY, "",CHANNEL_ID, CHANNEL_NAME);
        assertJobWellformed2(job);
    }

    void assertJobWellformed2(CreatePostJob job) {
        assertEquals(POST_BODY, job.getPostBody());
        assertEquals(CHANNEL_ID, job.getChannelID());
        assertEquals(CHANNEL_NAME, job.getChannelName());
        assertTrue(POST_ID, job.getPostID().length() > 0);
        assertEquals(CreatePostJob.PRIORITY, job.getPriority());
        assertEquals(CreatePostJob.RETRY_LIMIT, job.getRetryLimit());
        assertTrue(job.isPersistent());
        assertTrue(job.requiresNetwork());
    }

    private void assertJobWellformed(CreatePostJob job) {
        assertEquals(POST_BODY, job.getPostBody());
        assertEquals(CHANNEL_ID, job.getChannelID());
        assertEquals(CHANNEL_NAME, job.getChannelName());
        assertEquals(POST_ID, job.getPostID());
        assertEquals(CreatePostJob.PRIORITY, job.getPriority());
        assertEquals(CreatePostJob.RETRY_LIMIT, job.getRetryLimit());
        assertTrue(job.isPersistent());
        assertTrue(job.requiresNetwork());
    }

    private void testMustHandleIllegalArgs() {
        try {
            CreatePostJob.create("","", "", "");
            CreatePostJob.create(null, "","", "");
            CreatePostJob.create("", "",null, "");
            CreatePostJob.create("","", "", null);
            CreatePostJob.create("","", "", "");
            CreatePostJob.create(null,"", null, null);
            fail("must throw " + IllegalArgumentException.class.getName());
        } catch (IllegalArgumentException e) {

        }
    }

    @Test
    public void toJSON() throws Exception {
        JSONObject object = CreatePostJob.create(POST_BODY,"", CHANNEL_ID, CHANNEL_NAME).toJSON();
        assertEquals(POST_BODY, object.getString(Post.FIELD_POST_BODY));
        assertEquals(CHANNEL_ID, object.getString(Post.FIELD_CHANNEL_ID));
        assertEquals(CHANNEL_NAME, object.getString(Post.FIELD_CHANNEL_NAME));
        assertTrue(POST_ID, object.getString(Post.FIELD_POST_ID).length() > 0);
    }

    @Test
    public void fromJSON() throws Exception {
        JSONObject object = CreatePostJob.create(POST_BODY,"", CHANNEL_ID, CHANNEL_NAME).toJSON();
        assertJobWellformed2(new CreatePostJob().fromJSON(object));
    }

}