package com.sliido.dao.data;

import android.app.Application;
import android.content.Context;

import com.pairapp.Errors.SliidoException;
import com.pairapp.util.Config;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.PLog;
import com.parse.Parse;
import com.sliido.data.ParseClient;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * author Null-Pointer on 1/18/2016.
 */
@PrepareForTest({Parse.class, Config.class, ParseClient.class, Context.class})
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
public class UserManagerTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();
    private ParseClient mokParseClient;
    private Context mock;
    private boolean called;
    private EventBus.Event theEvent;
    private String eventName = UserManager.LOGIN_EVENT;

    private final EventBus.EventsListener listener = new EventBus.EventsListener() {
        @Override
        public int threadMode() {
            return EventBus.ANY;
        }

        @Override
        public void onEvent(EventBus bus, EventBus.Event event) {
            assertNotNull(event);
            called = true;
            theEvent = event;
            assertEquals(eventName, event.tag);
            System.out.println("event recieved");
        }

        @Override
        public boolean sticky() {
            return true;
        }
    };
    private User user;


    @Before
    public void setUp() throws SliidoException {
        PLog.setLogLevel(PLog.LEVEL_NONE);
        mockStatic(ParseClient.class);
        mockStatic(Context.class);
        mockStatic(Config.class);
        mock = mock(Context.class);
        Config.init(any(Application.class), "");
        mokParseClient = mock(ParseClient.class);
        user = mock(User.class);
        when(user.getName()).thenReturn("amean");
        when(user.getEmail()).thenReturn("afolanyaaminu@gmail.com");
        when(mokParseClient.signUpOrLogin(anyString(), anyString(), anyString())).thenReturn(user);
        EventsCenter.init();
        when(Config.getApplicationContext()).thenReturn(mock);
        when(mock.getApplicationContext()).thenReturn(mock);
        when(mock.getString(Matchers.anyInt())).thenReturn("test resource string");
        when(ParseClient.newInstance()).thenReturn(mokParseClient);
    }

    @After
    public void tearDown() {
        EventsCenter.getEventBus().unregister(UserManager.LOGIN_EVENT, listener);
    }

    @Test
    public void testLogin() throws Exception {
        UserManager manager = UserManager.getInstance();
        try {
            manager.login(null, "password");
            fail("must throw");
        } catch (IllegalArgumentException ignored) {

        }

        try {
            manager.login(null, null);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {

        }
        try {
            manager.login("email", null);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {

        }
        manager.login("afoo", "bar"); //invalid inputs
        eventName = UserManager.LOGIN_EVENT;
        EventsCenter.getEventBus().register(eventName, listener);
        assertTrue(called);
        assertNotNull(theEvent.error);
        called = false;
        manager.login("amean@gmail.com", "valid password");
        assertTrue(called);
        assertTrue(theEvent.data instanceof User);
        called = false;

    }

    @Test
    public void testSignUp() throws Exception {

    }

    @Test
    public void testGetCurrentUser() throws Exception {

    }

    @Test
    public void testIsCurrentUserVerified() throws Exception {

    }

    @Test
    public void testIsValidUserName() throws Exception {

    }

    @Test
    public void testIsValidPassword() throws Exception {

    }

    @Test
    public void testIsValidEmail() throws Exception {

    }

    @Test
    public void testVerifyCurrentUser() throws Exception {

    }

    @Test
    public void testReset() throws Exception {

    }
}
