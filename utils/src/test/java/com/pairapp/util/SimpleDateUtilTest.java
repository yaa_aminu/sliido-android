package com.pairapp.util;


import org.junit.Test;

import static org.junit.Assert.*;

/**
 * by Null-Pointer on 2/15/2016.
 */
public class SimpleDateUtilTest {

    @Test
    public void testFormatDateRage() throws Exception {

    }

    @Test
    public void testFormatSessionDate() throws Exception {

    }

    @Test
    public void testFormatSessionDate1() throws Exception {

    }

    @Test
    public void testTimeStampNow() throws Exception {

    }

    @Test
    public void testTimeStampNow1() throws Exception {

    }

    @Test
    public void testToDateString() throws Exception {
        assertEquals("02:15", SimpleDateUtil.toDateString(2.25));
        assertEquals("09:30", SimpleDateUtil.toDateString(9.5));
        assertEquals("23:54", SimpleDateUtil.toDateString(23.9));
        assertEquals("08:45", SimpleDateUtil.toDateString(8.75));
        assertEquals("12:06", SimpleDateUtil.toDateString(12.1));
        assertEquals("00:06", SimpleDateUtil.toDateString(0.1));
    }

    @Test
    public void testGetMinutes() throws Exception {
        assertEquals(30, SimpleDateUtil.getMinutes(2.5));
        assertEquals(48, SimpleDateUtil.getMinutes(21.8));
        assertEquals(45, SimpleDateUtil.getMinutes(2.75));
        assertEquals(54, SimpleDateUtil.getMinutes(23.90));
        assertEquals(15, SimpleDateUtil.getMinutes(9.25));
    }

    @Test
    public void testToTime() throws Exception {
        assertEquals(2.5, SimpleDateUtil.toTime(2, 30),0);
        assertEquals(5.75, SimpleDateUtil.toTime(5, 45),0);
        assertEquals(12.25, SimpleDateUtil.toTime(12, 15),0);
        assertEquals(0.1, SimpleDateUtil.toTime(0, 6),0);
    }

    @Test
    public void testGetHours() throws Exception {
        assertEquals(2, SimpleDateUtil.getHours(2.5));
        assertEquals(21, SimpleDateUtil.getHours(21.8));
        assertEquals(8, SimpleDateUtil.getHours(8.75));
        assertEquals(23, SimpleDateUtil.getHours(23.90));
        assertEquals(9, SimpleDateUtil.getHours(9.25));
    }
}