package com.pairapp.util.test;

import android.app.Application;
import android.content.Context;
import android.os.Looper;
import android.webkit.MimeTypeMap;

import com.pairapp.util.Config;
import com.pairapp.util.FileUtils;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import static com.pairapp.util.Config.getApplicationContext;
import static com.pairapp.util.Config.getTempDir;
import static com.pairapp.util.FileUtils.copyTo;
import static com.pairapp.util.FileUtils.hashFile;
import static com.pairapp.util.FileUtils.save;
import static com.pairapp.util.FileUtils.sizeInLowestPrecision;
import static java.lang.System.getenv;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;


/**
 * author Null-Pointer on 1/16/2016.
 */
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
@PrepareForTest({MimeTypeMap.class, Config.class, Looper.class})
public class FileUtilsTest {
    File tempDir = new File(getenv("temp"));
    File gradleBuildFile = new File("D:\\codecentral\\PairApp\\Sliido\\build2.gradle"), //copy of the gradle build file for the project;
            testDestination = new File(tempDir, "build.gradle");
    private String baseurl = "http://localhost:5000/";
    @SuppressWarnings("FieldCanBeLocal")
    private Application mock;
    @SuppressWarnings("FieldCanBeLocal")
    private String resString = "test resource string";
    ;

    @Before
    public void setUp() {
        mockStatic(Looper.class);
        mockStatic(Config.class);
        PLog.setLogLevel(PLog.LEVEL_NONE);
        mock = mock(Application.class);
        Config.init(mock,"");
        Context mockContext = mock(Context.class);
        when(Config.getApplication()).thenReturn(mock);
        when(Config.getApplicationContext()).thenReturn(mockContext);
        when(mockContext.getString(Matchers.anyInt())).thenReturn(resString);
        Thread mockThread = new Thread();
        Looper looper = mock(Looper.class);
        when(Looper.getMainLooper()).thenReturn(looper);
        when(looper.getThread()).thenReturn(mockThread);
        percent = 0;
        bytesExpected = 0;
    }

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Test
    public void testResolveContentUriToFilePath() throws Exception {

    }

    @Test
    public void testResolveContentUriToFilePath1() throws Exception {

    }

    @Test
    public void testGetMimeType() throws Exception {

        mockStatic(MimeTypeMap.class);
        MimeTypeMap map = PowerMockito.mock(MimeTypeMap.class);
        PowerMockito.when(MimeTypeMap.getSingleton()).thenReturn(map);
        PowerMockito.when(map.getMimeTypeFromExtension("txt")).thenReturn("text/plain");
        assertEquals("text/plain", FileUtils.getMimeType("foo.txt"));
        PowerMockito.when(map.getMimeTypeFromExtension("jpg")).thenReturn("image/jpeg");
        assertEquals("image/jpeg", FileUtils.getMimeType("foo.jpg"));
        PowerMockito.when(map.getMimeTypeFromExtension("mp4")).thenReturn("video/mp4");
        assertEquals("video/mp4", FileUtils.getMimeType("foo.mp4"));
    }

    @Test
    public void testGetExtension() throws Exception {
        assertEquals("png", FileUtils.getExtension("image.png"));
        assertEquals("ffaa", FileUtils.getExtension("image.ffaa"));
        assertEquals("bar", FileUtils.getExtension("image.bar"));
        assertEquals("mp3", FileUtils.getExtension("image.mp3"));
        assertEquals("baz", FileUtils.getExtension("image.baz"));
        assertEquals("foo", FileUtils.getExtension("image.foo"));
        assertNull(FileUtils.getExtension("fafaf"));
        assertNull(FileUtils.getExtension(null));
    }

    @Test
    public void testGetExtension1() throws Exception {
        assertEquals("png", FileUtils.getExtension(null, "png"));
        assertEquals("ffaa", FileUtils.getExtension("image", "ffaa"));
        assertNull(FileUtils.getExtension("fafaf", null));
        assertNull(FileUtils.getExtension(null), null);
    }

    @Test
    public void testHash() throws Exception {
        byte[] bytes = {
                10, 15
        };
        assertNotNull(FileUtils.hash(bytes));
    }

    @Test
    public void testHash1() throws Exception {

    }

    @Test
    public void testBytesToString() throws Exception {
        byte[] bytes = {
                10, 15
        };
        assertEquals("0A0F".toLowerCase(), FileUtils.bytesToString(bytes));
    }

    @Test
    public void testSave() throws Exception {
        when(getTempDir()).thenReturn(tempDir);
        Application app = mock(Application.class);
        when(getApplicationContext()).thenReturn(app);
        when(app.getString(0)).thenReturn("dummy");
        save(testDestination, new FileInputStream(gradleBuildFile));
        assertTrue(testDestination.exists());
        assertEquals(hashFile(gradleBuildFile), hashFile(testDestination));
        assertEquals(gradleBuildFile.length(), testDestination.length());
    }

    @Test
    public void testSave1() throws Exception {
        final File destination = new File(tempDir, "testfile.jpg");
        try {
            save(destination, "http://lakflfalkfa");
            fail("must throw");
        } catch (IOException ignored) {

        }
        try {
            save(destination, "");
            fail("must throw");
        } catch (IOException ignored) {

        }

        try {
            save(null, (String) null);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {

        }
        String url1 = baseurl + "testfile.jpg";
        URL url = new URL(url1);
        final URLConnection urlConnection = url.openConnection();
        int contentLength = urlConnection.getHeaderFieldInt("content-length", -1);
        urlConnection.getInputStream().close();
        save(destination, url1);
        assertTrue(destination.exists());
        assertTrue(destination.length() > 0);
        assertEquals(contentLength, destination.length());
        try {
            save(destination, "http://localhost:5000/faf");
            fail("must throw");
        } catch (IOException ignored) {

        }
    }

    @Test
    public void testHashFile() throws Exception {

    }

    int percent = 0;
    int bytesExpected = 0;
    FileUtils.ProgressListener progressListener = new FileUtils.ProgressListener() {
        @Override
        public void onProgress(long expected, long processed) throws IOException {
            assertEquals(bytesExpected, expected);
            assertTrue(expected >= processed);
            int prev = percent;
            percent = (int) ((100 * processed) / expected);
            assertTrue(prev <= percent);
            assertTrue(percent <= 100 && percent >= 0);
            System.out.print(percent + "%" + ",");
        }
    };

    @Test
    public void testSave2() throws Exception {
        File destination = new File(tempDir, "testfile.jpg");
        String url1 = baseurl + "testfile.jpg";
        URL url = new URL(url1);
        final URLConnection urlConnection = url.openConnection();
        int contentLength = urlConnection.getHeaderFieldInt("content-length", -1);
        urlConnection.getInputStream().close();
        bytesExpected = contentLength;
        save(destination, url1, progressListener);
        assertTrue(destination.exists());
        assertTrue(destination.length() > 0);
        assertEquals(contentLength, destination.length());
        try {
            save(destination, "http://localhost:5000/faf");
            fail("must throw");
        } catch (IOException ignored) {

        }
        try {
            save(null, null, progressListener);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            save(null, "http://localhost:5000", progressListener);
            fail("mus throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            save(destination, null, progressListener);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            save(null, null, null);
            fail("mus throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            save(tempDir, baseurl + "testfile.jpg", progressListener);
            fail("mus throw");
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testSave3() throws Exception {
        File destination = new File(tempDir, "testfile.jpg");
        String url1 = baseurl + "testfile.jpg";
        URL url = new URL(url1);
        final URLConnection urlConnection = url.openConnection();
        int contentLength = urlConnection.getHeaderFieldInt("content-length", -1);
        InputStream in = urlConnection.getInputStream();
        bytesExpected = contentLength;
        save(destination, in, contentLength, progressListener);
        assertTrue(destination.exists());
        assertTrue(destination.length() > 0);
        assertEquals(contentLength, destination.length());

        try {
            save(null, null, -1, null);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            save(null, null, -1, progressListener);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            save(null, null, contentLength, progressListener);
            fail("mus throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            save(null, in, contentLength, progressListener);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            save(tempDir, in, contentLength, progressListener);
            fail("mus throw");
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testCopyTo() throws Exception {
        File file1 = new File(tempDir, "fooo.gradle");
        FileUtils.copyTo(gradleBuildFile, file1);
        assertEquals(gradleBuildFile.length(), file1.length());
        assertEquals(FileUtils.hashFile(gradleBuildFile), FileUtils.hashFile(file1));
        try {
            copyTo(null, tempDir);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            copyTo((File) null, null);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            copyTo(tempDir, file1);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }

    }

    @Test
    public void testCopyTo1() throws Exception {
        File file1 = new File(tempDir, "fooo.gradle");
        FileUtils.copyTo(gradleBuildFile.getAbsolutePath(), file1.getAbsolutePath());
        assertEquals(gradleBuildFile.length(), file1.length());
        assertEquals(FileUtils.hashFile(gradleBuildFile), FileUtils.hashFile(file1));
        try {
            copyTo(null, tempDir.getAbsolutePath());
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            copyTo((String) null, null);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            copyTo(tempDir.getAbsolutePath(), file1.getAbsolutePath());
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testSizeInLowestPrecision() throws Exception {
        try {
            sizeInLowestPrecision(-12);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        try {
            sizeInLowestPrecision(0);
            fail("must throw");
        } catch (IllegalArgumentException ignored) {
        }
        //just to ensure it does not throw any runtime exception
        assertNotNull(sizeInLowestPrecision(gradleBuildFile.length()));
        assertFalse(GenericUtils.isEmpty(sizeInLowestPrecision(gradleBuildFile.length())));
    }

    @Test
    public void testSizeInLowestPrecision1() throws Exception {
        try {
            sizeInLowestPrecision("not existing");
            fail("must throw");
        } catch (IOException ignored) {
        }
        try {
            sizeInLowestPrecision(null);
            fail("must throw");
        } catch (IllegalArgumentException|IOException ignored) {
        }
        //just to ensure it does not throw any runtime exception
        assertNotNull(sizeInLowestPrecision(gradleBuildFile.getAbsolutePath()));
        assertFalse(GenericUtils.isEmpty(sizeInLowestPrecision(gradleBuildFile.getAbsolutePath())));
    }
}
