package com.pairapp.Errors;

import com.pairapp.util.GenericUtils;
import com.pairapp.util.R;

/**
 * @author Null-Pointer on 8/29/2015.
 */
public class SliidoException extends Exception {
    public static final int ERROR_UNKNOWN = -1;
    public static final int EDUPLICATE = 1000;
    public static final int ECONREFUSED = 1001;
    public static final int INTERNALSERVERERROR = 1001;
    public static final int ENOTFOUND = 1004;
    public static final int EUNAUTHORIZED = 1005;
    private final int errorCode;
    private final String userFriendlyMessage;


    public SliidoException() {
        this(GenericUtils.getString(R.string.error_unknown), ERROR_UNKNOWN);
    }

    public SliidoException(String message, int errorCode) {
        this(message, message, errorCode);
    }

    public SliidoException(String message, String userFriendlyMessage, int errorCode) {
        super(message);
        this.errorCode = errorCode;
        this.userFriendlyMessage = userFriendlyMessage;
    }

    public SliidoException(String message) {
        this(message, ERROR_UNKNOWN);
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getUserFriendlyMessage() {
        return userFriendlyMessage;
    }

}
