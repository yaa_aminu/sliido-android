package com.pairapp.Errors;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.pairapp.util.Config;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.NavigationManager;
import com.pairapp.util.R;
import com.pairapp.util.TaskManager;
import com.pairapp.util.ThreadUtils;

import java.lang.ref.WeakReference;

/**
 * a utility class aimed at centralising error reporting to the user in the future this class
 * may be used to even report errors to our backend.
 * Components who report errors in callbacks are advised to do so through this class.
 * <p/>
 * it smartly queues errors and waits for a new activity to be visible before showing the error to the user. this behaviour relies
 * on {@link com.pairapp.util.NavigationManager} to know the state of the current activity.
 *
 * @author by Null-Pointer on 9/6/2015.
 */
public class ErrorCenter {

    public static final int INDEFINITE = -1;
    private static final String TAG = ErrorCenter.class.getSimpleName();
    private static final long DEFAULT_TIMEOUT = 1000L;
    private static Error waitingError;
    private static WeakReference<ErrorShower> errorShower = new WeakReference<>(null);//to free us from null-checks

    /**
     * @param errorId      a unique identifier for this error. this is useful if a component needs to update
     *                     a particular error or cancel it at all.
     *                     which means reporting an error with the same id will replace the existing key.
     * @param errorMessage the message to be shown to the user
     * @see {@link #reportError(String, String, long)} if you want to timeout the error.
     */
    public static void reportError(String errorId, String errorMessage) {
        reportError(errorId, errorMessage, INDEFINITE);
    }


    /**
     * @param errorId      a unique identifier for this error. this is useful if a component needs to update
     *                     a particular error or cancel it at all..
     * @param errorMessage the message to be shown to the user
     * @param timeout      when the message should be considered stale. can't be less than 10000 (10 seconds)
     *                     the timeout is only relevant when there is no visible activity. if there is one it will
     *                     be ignored
     * @see {@link #reportError(String, String)} if you don't want to timeout the error.
     */
    public static synchronized void reportError(String errorId, final String errorMessage, long timeout) {
        if (timeout != INDEFINITE) {
            timeout = Math.max(timeout, DEFAULT_TIMEOUT); //time out cannot be less than DefaultTimeout
        }
        NavigationManager.States currentActivityState = null;
        final Error tmp = new Error(errorId, errorMessage, timeout);
        try {
            currentActivityState = NavigationManager.getCurrentActivityState();
            if (!currentActivityState.equals(NavigationManager.States.DESTROYED) || !currentActivityState.equals(NavigationManager.States.STOPPED)) {
                final ErrorShower errorShower = ErrorCenter.errorShower.get();
                if (errorShower != null) {
                    if (ThreadUtils.isMainThread()) {
                        errorShower.showError(tmp);
                    } else {
                        TaskManager.executeOnMainThread(new Runnable() {
                            @Override
                            public void run() {
                                errorShower.showError(tmp);
                            }
                        });
                    }
                    return;
                }
            }
        } catch (NavigationManager.NoActiveActivityException e) {
            Log.d(TAG, "no visible activity. waiting till an activity shows up");
        }
        waitingError = tmp;
    }

    @Deprecated //use showNotification() instead
    public static synchronized void reportError(String id, String errorMessage, Intent action) {
        Context applicationContext = Config.getApplicationContext();
        if (action == null) {
            action = new Intent();
            action.setComponent(new ComponentName(applicationContext, applicationContext.getPackageName() + ".MainActivity"));
        }
        // intent.putExtra("id", id);
        // intent.putExtra("action", action);
        // String message = intent.getStringExtra("message");
        PendingIntent pendingIntent = null;
        Class<?> clazz = action.getClass();
        if (clazz == null) {
            if (action.getAction() != null) {
                pendingIntent = PendingIntent.getActivity(applicationContext, id.hashCode(), action, PendingIntent.FLAG_UPDATE_CURRENT);
            }
        } else if (clazz.getSuperclass()/*null pointer ex impossible*/.equals(Service.class)) { //will break component is not direct subclass
            pendingIntent = PendingIntent.getService(applicationContext, id.hashCode(), action, PendingIntent.FLAG_UPDATE_CURRENT);
        } else if (clazz.getSuperclass()/*null pointer ex impossible*/.equals(BroadcastReceiver.class)) {
            pendingIntent = PendingIntent.getBroadcast(applicationContext, id.hashCode(), action, PendingIntent.FLAG_UPDATE_CURRENT);
        } else if (clazz.getSuperclass()/*null pointer ex impossible*/.equals(WakefulBroadcastReceiver.class)) {
            pendingIntent = PendingIntent.getBroadcast(applicationContext, id.hashCode(), action, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        if (pendingIntent == null) {
            pendingIntent = PendingIntent.getActivity(applicationContext, id.hashCode(), action, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(applicationContext)
                .setContentTitle(applicationContext.getString(R.string.error))
                .setTicker(errorMessage)
                .setContentText(errorMessage)
                .setAutoCancel(true);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.ic_stat_notifications_active).build();
        NotificationManagerCompat manager = NotificationManagerCompat.from(applicationContext);// getSystemService(NOTIFICATION_SERVICE));
        manager.notify(id, 100000111, builder.build());
    }

    /**
     * cancels a pending error report
     *
     * @param errorId the id of the report
     */
    @SuppressWarnings("unused")
    public static void cancel(String errorId) {
        if (waitingError != null && waitingError.id.equals(errorId)) {
            Log.d(TAG, "cancelling error with id: " + errorId);
            waitingError = null;
        } else if (errorShower != null) {
            ErrorShower shower = ErrorCenter.errorShower.get();
            if (shower != null)
                shower.disMissError(errorId);
        }
    }

    /**
     * register a new error shower. We don't hold a strong references so
     * you may not pass an anonymous instance.
     *
     * @param errorShower the error shower to register, may not be null
     * @throws IllegalArgumentException if errorShow is null
     */
    public static synchronized void registerErrorShower(ErrorShower errorShower) {
        if (errorShower == null) throw new IllegalArgumentException("null!");
        ErrorCenter.errorShower = new WeakReference<>(errorShower);
        showPendingError();
    }

    /**
     * unregistered an error shower.
     *
     * @param errorShower the error shower to unregister
     */
    public static synchronized void unRegisterErrorShower(ErrorShower errorShower) {
        if (ErrorCenter.errorShower != null) {
            ErrorShower ourErrorShower = ErrorCenter.errorShower.get();
            if (ourErrorShower != null && ourErrorShower == errorShower) {
                ErrorCenter.errorShower.clear();
            }
        }
    }

    /**
     * a hook for components that feel there may be a pending error.
     * for e.g. an {@link android.app.Activity} can call this method whenever it comes to
     * the screen typically in its {@link Activity#onResume()}) callback.
     * the {@link NavigationManager} calls this method automatically anytime an {@link Activity}
     * reports itself as resumed.
     *
     * @throws IllegalStateException when  called from a thread other than the main Thread.
     */
    public static void showPendingError() {
        ThreadUtils.ensureMain();
        if (waitingError != null) {
            if (waitingError.timeout == INDEFINITE || waitingError.timeout <= System.currentTimeMillis()) {
                reportError(waitingError.id, waitingError.message, waitingError.timeout);
            }
            waitingError = null;
            return;
        }
        Log.d(TAG, "no error to show either they have time out or there is none at all");
    }

    public static void reportError(String id, String message, ReportStyle style, int indefinite) {
        final Error tmp = new Error(id, message, indefinite, style);
        if (style == ReportStyle.NOTIFICATION) {
            reportError(id, message, null);
        } else {
            if (errorShower != null && errorShower.get() != null) {
                final ErrorShower errorShower = ErrorCenter.errorShower.get();
                TaskManager.executeOnMainThread(new Runnable() {

                    @Override
                    public void run() {
                        errorShower.showError(tmp);
                    }
                });
            } else {
                if (style == ReportStyle.DIALOG_NOT) {
                    reportError(id, message, null);
                    return;
                }
                waitingError = tmp;
            }
        }
    }

    public static void showNotification(@NonNull Context context,
                                        int notId,
                                        @NonNull String title,
                                        @NonNull String message,
                                        @DrawableRes int icon,
                                        @Nullable PendingIntent action,
                                        boolean light) {
        showNotification(context, notId, title, message, icon, action, light, false);
    }

    public static void showNotification(@NonNull Context context,
                                        int notId,
                                        @NonNull String title,
                                        @NonNull String message,
                                        @DrawableRes int icon,
                                        @Nullable PendingIntent action,
                                        boolean light, boolean onGoing) {
        showNotification(context, notId, title, message, icon, action, light, onGoing, false);
    }


    public static void showNotification(@NonNull Context context,
                                        int notId,
                                        @NonNull String title,
                                        @NonNull String message,
                                        @DrawableRes int icon,
                                        @Nullable PendingIntent action,
                                        boolean light, boolean onGoing, boolean playSound) {
        GenericUtils.ensureNotEmpty(title, message);
        GenericUtils.ensureNotNull(context);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentText(message);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        builder.setTicker(message);
        builder.setOngoing(onGoing);
        builder.setAutoCancel(!onGoing);
        builder.setSmallIcon(icon == 0 ? R.drawable.ic_stat_notifications_active : icon);
        builder.setContentTitle(title);
        if (playSound) {
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        }
        if (light) {
            builder.setLights(Color.BLUE, 750, 2000);
        }
        if (action != null) {
            builder.setContentIntent(action);
        }
        Notification notification = builder.build();
        NotificationManagerCompat.from(context).notify(notId, notification);
    }

    public enum ReportStyle {
        DIALOG, NOTIFICATION, STICKY, DIALOG_NOT
    }

    public interface ErrorShower {
        void showError(Error error);

        void disMissError(String errorId);
    }

    public static class Error {
        public final String message, id;
        public final long timeout;
        public final ReportStyle style;

        private Error(String id, String message, long timeout) {
            this(id, message, timeout, ReportStyle.DIALOG);
        }

        private Error(String id, String message, long timeout, ReportStyle style) {
            this.id = id;
            this.message = message;
            this.timeout = System.currentTimeMillis() + timeout;
            this.style = style;
        }
    }
}
