package com.pairapp.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.annotation.NonNull;

import com.pairapp.Errors.ErrorCenter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author null-pointer
 */
public class Config {

    public static final int NOT_ID_REQUEST_ACCEPTED = 100002;
    public static final int NOT_ID_NEW_SCHEDULE = 10001;
    public static final int NOT_ID_NEW_LECTURE_MATERIAL = 10003;
    private static final String APP_PREFS = "sleedo.prefs.xml",
            TAG = Config.class.getSimpleName(),
            logMessage = "calling getApplication when init has not be called",
            detailMessage = "application is null. Did you forget to call Config.init()?";
    private static final AtomicBoolean isAppOpen = new AtomicBoolean(false);
    private static String APP_NAME = "";
    private static Application application;

    private static boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    public static boolean appOpen() {
        return isAppOpen.get();
    }

    public static void appOpen(boolean appOpen) {
        ThreadUtils.ensureMain();
        isAppOpen.set(appOpen);
    }

    public static void init(Application pairApp, String appName) {
        Config.application = pairApp;
        GenericUtils.ensureNotEmpty(appName);
        Config.APP_NAME = appName;
        setUpDirs();
    }


    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void setUpDirs() {
        if (isExternalStorageAvailable()) {
            //no need to worry calling this several times
            //if the file is already a directory it will fail silently
            try {
                getAppBinFilesBaseDir().mkdirs();
                getAppProfilePicsBaseDir().mkdirs();
                getTempDir().mkdirs();
            } catch (FileNotFoundException e) {
                PLog.d(TAG, e.getMessage(), e);
            }
        } else {
            PLog.w(TAG, "This is strange! no sdCard available on this device");
            ErrorCenter.reportError("noSdcard" + TAG, getApplicationContext().getString(R.string.no_sdcard), null);
        }

    }

    @NonNull
    public static Context getApplicationContext() {
        ensureInitialised();
        return application.getApplicationContext();
    }

    private static void ensureInitialised() {
        if (application == null) {
            logAndThrow(logMessage, detailMessage);
        }
    }

    @NonNull
    public static Application getApplication() {
        ensureInitialised();
        return Config.application;
    }

    private static void logAndThrow(String msg, String detailMessage) {
        PLog.w(TAG, msg);
        throw new IllegalStateException(detailMessage);
    }

    @NonNull
    public static SharedPreferences getApplicationWidePrefs() {
        ensureInitialised();
        if (application == null) {
            throw new IllegalStateException("application is null,did you forget to call init(Context) ?");
        }
        return getPreferences(Config.APP_PREFS);
    }

    public static String appName() {
        ensureInitialised();
        return APP_NAME;
    }

    @NonNull
    public static File getAppBinFilesBaseDir() throws FileNotFoundException {
        ensureInitialised();
        checkHasSdcard();
        File file = new File(Environment
                .getExternalStoragePublicDirectory(APP_NAME), getApplicationContext().getString(R.string.folder_name_files));
        if (!file.isDirectory()) {
            if (!file.mkdirs()) {
                throw new FileNotFoundException(GenericUtils.getString(R.string.no_sdcard));
            }
        }
        return file;
    }

    @NonNull
    public static File getAppProfilePicsBaseDir() throws FileNotFoundException {
        ensureInitialised();
        checkHasSdcard();
        File file = new File(Environment
                .getExternalStoragePublicDirectory(APP_NAME), getApplicationContext().getString(R.string.folder_name_profile));
        if (!file.isDirectory()) {
            if (!file.mkdirs()) {
                throw new FileNotFoundException(GenericUtils.getString(R.string.no_sdcard));
            }
        }
        return file;
    }

    private static void checkHasSdcard() throws FileNotFoundException {
        if (!isExternalStorageAvailable()) {
            throw new FileNotFoundException(GenericUtils.getString(R.string.no_sdcard));
        }
    }

    public static File getTempDir() throws FileNotFoundException {
        ensureInitialised();
        checkHasSdcard();
        File file = new File(Environment
                .getExternalStorageDirectory(), "TMP");
        if (!file.isDirectory()) {
            if (!file.mkdirs()) {
                throw new FileNotFoundException(GenericUtils.getString(R.string.no_sdcard));
            }
        }
        return file;
    }

    public static String linksEndPoint() {
        ensureInitialised();
//        return "http://10.0.3.3:7000";
        return "https://pairapp-link-maker.herokuapp.com";
    }

    public static SharedPreferences getPreferences(String s) {
        ensureInitialised();
        GenericUtils.ensureNotEmpty(s);
//        return new SecurePreferences(getApplicationContext(), "password", null);
        return getApplication().getSharedPreferences(s, Context.MODE_PRIVATE);
    }
}
