package com.pairapp.util;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.TagConstraint;
import com.path.android.jobqueue.config.Configuration;
import com.path.android.jobqueue.di.DependencyInjector;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author by Null-Pointer on 9/27/2015.
 */
public class TaskManager {

    public static final String TAG = TaskManager.class.getSimpleName();
    private static final AtomicBoolean initialised = new AtomicBoolean(false);
    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int CORE_POOL_SIZE = CPU_COUNT + 3;
    private static final int MAXIMUM_POOL_SIZE = CPU_COUNT * 2 + 4;
    private static final int KEEP_ALIVE = 4;
    private static final BlockingQueue<Runnable> nonNetworkWorkQueue =
            new LinkedBlockingQueue<>(128), networkWorkQueue = new LinkedBlockingQueue<>(256);
    private static final ExecutorService NON_NETWORK_EXECUTOR
            = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE,
            TimeUnit.SECONDS, nonNetworkWorkQueue),
            NETWORK_EXECUTOR
                    = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE,
                    TimeUnit.SECONDS, networkWorkQueue);
    private static ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    private static JobManager jobManager;


    public static void init(final Application application, DependencyInjector injector) {
        if (!initialised.getAndSet(true)) {
            PLog.w(TAG, "initialising %s", TAG);
            final Configuration config = new Configuration.Builder(application)
                    .injector(injector)
                    .customLogger(new PLog("JobManager"))
                    .jobSerializer(new TaskSerializer())
//                    .networkUtil(ConnectionUtils.getNetworkUtil())
                    .build();
            final Timer timer = new Timer("cleanup Timer", true);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        jobManager = new JobManager(application, config);
                        jobManager.start();
                        File file = Config.getTempDir();
                        if (file.exists()) {
                            org.apache.commons.io.FileUtils.cleanDirectory(Config.getTempDir());
                        }
                    } catch (IOException ignored) {

                    } finally {
                        timer.cancel();
                    }
                }
            }, 1); //immediately

        }
    }

    public static long runJob(Task job) {
        ThreadUtils.ensureNotMain();
        ensureInitialised();
        GenericUtils.ensureConditionTrue(job != null && job.isValid(), "invalid job");
        assert job != null;
        return jobManager.addJob(job);
    }

    public static void executeOnMainThread(Runnable r) {
        ensureInitialised();
        new Handler(Looper.getMainLooper()).post(r);
    }

    public static <V> Future<V> execute(Callable<V> task, boolean requiresNetwork) {
        ensureInitialised();
        if (requiresNetwork) {
            return NETWORK_EXECUTOR.submit(task);
        } else {
            return NON_NETWORK_EXECUTOR.submit(task);
        }
    }

    public static void execute(Runnable task, boolean requiresNetwork) {
        ensureInitialised();
        if (requiresNetwork) {
            NETWORK_EXECUTOR.execute(task);
        } else {
            NON_NETWORK_EXECUTOR.execute(task);
        }
    }

    public static void executeNow(Runnable runnable, boolean requiresNetwork) {
        ensureInitialised();
        cachedThreadPool.execute(runnable);
    }


    private static void ensureInitialised() {
        if (!initialised.get()) {
            throw new IllegalArgumentException("did you forget to init()?");
        }
    }

    public static void cancelJobAsync(String tag) {
        jobManager.cancelJobsInBackground(null, TagConstraint.ALL, tag);
    }

    public static void cancelJobSync(String tag) {
        jobManager.cancelJobs(TagConstraint.ALL, tag);
    }
}
