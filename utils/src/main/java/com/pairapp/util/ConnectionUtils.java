package com.pairapp.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.path.android.jobqueue.network.NetworkEventProvider;
import com.path.android.jobqueue.network.NetworkUtil;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.pairapp.util.GenericUtils.ensureNotNull;

/**
 * @author Null-Pointer on 6/6/2015.
 */
public class ConnectionUtils {

    private static final String TAG = ConnectionUtils.class.getSimpleName();
    private static final AtomicBoolean isConnected = new AtomicBoolean(false);

    public static void init(final Context context) {
        if (!initialised) {
            initialised = true;
            setUpConnectionState(null);
            context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    setUpConnectionState(intent);
                }
            }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    private static volatile boolean initialised = false;

    private static void setUpConnectionState(Intent intent) {
        if (intent == null) {
            NetworkInfo networkInfo = getNetworkInfo();
            isConnected.set(networkInfo != null && (networkInfo.isConnected() || networkInfo.isConnectedOrConnecting()));
        } else {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
            if (networkInfo == null) {
                isConnected.set(!intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false));
            } else {
                isConnected.set(networkInfo.isConnected() || networkInfo.isConnectedOrConnecting());
            }
        }
        notifyListeners(isConnected.get());
    }

    public static boolean isConnected() {
        ensureInitialised();
        return isConnected.get() || isConnectedOrConnecting();
    }

    private static boolean isConnectedOrConnecting() {
        NetworkInfo networkInfo = getNetworkInfo();
        return ((networkInfo != null) && networkInfo.isConnectedOrConnecting());
    }

    private static NetworkInfo getNetworkInfo() {
        Context context = Config.getApplicationContext();
        ConnectivityManager manager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return manager.getActiveNetworkInfo();
    }

    public static boolean isWifiConnected() {
        ensureInitialised();
        if (!isConnected()) {
            return false;
        }
        NetworkInfo info = getNetworkInfo();
        int type = info.getType();
        return type == ConnectivityManager.TYPE_WIFI;
    }

    public static boolean isMobileConnected() {
        ensureInitialised();
        if (!isConnected()) {
            return false;
        }
        NetworkInfo info = getNetworkInfo();
        int type = info.getType();
        return type == ConnectivityManager.TYPE_MOBILE;
    }

    public static boolean isActuallyConnected() {
        ensureInitialised();
        ThreadUtils.ensureNotMain();
        return isConnected() && isReallyReallyConnected();
    }

    private static boolean isReallyReallyConnected() {
        HttpURLConnection connection = null;
        //noinspection EmptyCatchBlock
        try {
            connection = (HttpURLConnection) new URL("https://facebook.com").openConnection();
            connection.connect();
            return true;
        } catch (UnknownHostException | SocketTimeoutException e) {
            PLog.e(TAG, e.getMessage(), e);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            PLog.e(TAG, e.getMessage(), e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        PLog.d(TAG, "not connected");
        return false;
    }

    private static final List<WeakReference<ConnectivityListener>> listeners = new ArrayList<>();

    public static void registerConnectivityListener(ConnectivityListener listener) {
        ensureInitialised();
        ensureNotNull(listener);
        synchronized (listeners) {
            for (int i = 0; i < listeners.size(); i++) {
                if (listeners.get(i).get() == listener) {
                    PLog.d(TAG, "already registered");
                    return;
                }
            }
            listeners.add(new WeakReference<>(listener));
            listener.onConnectivityChanged(isConnected());
        }
    }

    public static void unRegisterConnectivityListener(ConnectivityListener listener) {
        ensureInitialised();
        ensureNotNull(listener);
        synchronized (listeners) {
            for (int i = 0; i < listeners.size(); i++) {
                WeakReference<ConnectivityListener> connectivityListenerWeakReference = listeners.get(i);
                if (connectivityListenerWeakReference.get() == listener) {
                    listeners.remove(connectivityListenerWeakReference);
                    return;
                }
            }
            PLog.d(TAG, "listener not found to be unregistered");
        }

    }

    private static void notifyListeners(boolean connected) {
        synchronized (listeners) {
            for (int i = 0; i < listeners.size(); i++) {
                ConnectivityListener weakReference = listeners.get(i).get();
                if (weakReference != null) {
                    weakReference.onConnectivityChanged(connected);
                }
            }
            //clean up dead weak references
            for (int i = 0; i < listeners.size(); i++) {
                WeakReference<ConnectivityListener> weakReference = listeners.get(i);
                if (weakReference.get() == null) {
                    listeners.remove(weakReference);
                }
            }
        }
    }

    public static NetworkUtil getNetworkUtil() {
        return networkUtil;
    }

    private static CustomNetworkUtil networkUtil = new CustomNetworkUtil();

    private static class CustomNetworkUtil implements NetworkUtil, NetworkEventProvider {
        private final ConnectivityListener connectivityListener = new ConnectivityListener() {
            @Override
            public void onConnectivityChanged(boolean connected) {
                PLog.d("customNetworkUtil", connected ? "connected" : "disconnected");
                if (networkUtilListener != null) {
                    networkUtilListener.onNetworkChange(connected);
                } else {
                    if (BuildConfig.DEBUG) {
                        throw new IllegalStateException("no listener");
                    }
                    PLog.d("customNetworkUtil", "listener not available");
                }
            }
        };
        private Listener networkUtilListener;

        @Override
        public void setListener(final Listener listener) {
            this.networkUtilListener = listener;
            registerConnectivityListener(this.connectivityListener);
        }

        @Override
        public boolean isConnected(Context context) {
            return ConnectionUtils.isActuallyConnected();
        }
    }

    public interface ConnectivityListener {
        void onConnectivityChanged(boolean connected);
    }

    private static void ensureInitialised() {
        if (!initialised) {
            throw new IllegalStateException("not initialised");
        }
    }
}
