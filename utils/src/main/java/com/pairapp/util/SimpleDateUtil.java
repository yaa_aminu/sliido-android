package com.pairapp.util;

import android.content.Context;
import android.text.format.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * @author _2am on 9/18/2015.
 */
public class SimpleDateUtil {

    private static SimpleDateFormat dayPrecisionFormatter = new SimpleDateFormat("yyyy/MM/dd", Locale.US),
            secondsPrecissionFormatter = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());

    public static String formatDateRage(Context context, Date fromWhen) {
        return formatInternal(context, fromWhen);
    }

    private static String formatInternal(Context context, Date when) {
        return formatInternal(context, new Formatter(Locale.getDefault()), when);
    }

    private static String formatInternal(Context context, Formatter formatter, Date when) {
        final Date today = new Date();
        long elapsed = today.getTime() - when.getTime();

        final long oneDay = 24 * 60 * 60 * 1000;

        if (elapsed <= 8 * oneDay /*8 days apart (at least same week)*/) { //one more day for correctness
            if (elapsed <= 2 * oneDay/*yesterday*/) { //one more day for correctness.
                if (today.getDay() == when.getDay()) {
                    return context.getString(R.string.today);
                }
                //we are checking for roll-overs for e.g sunday=1 and saturday = 7 in
                //in the western locales
                final long daysBetween = Math.abs(today.getDay() - when.getDay());
                if (daysBetween == 1 || daysBetween > 2 /*handle roll-overs*/) {
                    return context.getString(R.string.yesterday);
                }
            }
            return DateUtils.formatDateTime(context, when.getTime(), DateUtils.FORMAT_SHOW_WEEKDAY);
        }
        return DateUtils.formatDateTime(context, when.getTime(), DateUtils.FORMAT_NUMERIC_DATE);
    }

    public static String formatSessionDate(Date time) {
//        if (time.getTime() > System.currentTimeMillis() + 24 * 60 * 60 * 1000) {
//            throw new IllegalArgumentException("date in the future");
//        }
        return dayPrecisionFormatter.format(time); //this will be unique for every day
    }

    public static String formatSessionDate() {
        return formatSessionDate(new Date());
    }

    public static String timeStampNow() {
        return timeStampNow(new Date());
    }

    public static String timeStampNow(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("null!");
        }
        return secondsPrecissionFormatter.format(date);
    }

    public static String toDateString(double time) {
        if (time < 0 || time >= 24) {
            throw new IllegalArgumentException("date < 0 || date >=24");
        }
        int minutes = getMinutes(time);
        int hours = getHours(time);
        return (hours < 10 ? "0" + hours : "" + hours) + ":" + ((minutes < 10) ? "0" + minutes : "" + minutes);
    }

    public static int getMinutes(double time) {
        if (time < 0 || time >= 24) {
            return 0;
        }
        if (time < 1) { //handle modulo  on zero to avoid NaN
            time += 10;
            double minutes = (time % (int) time); //retrieve number after decimal point. eg 1.75 produces 0.75
            minutes = (minutes * 60);//convert to minutes eg. 0.5 will produce 30 mins
            return (int) Math.round(minutes);
        } else {
            double minutes = (time % (int) time); //retrieve number after decimal point. eg 1.75 produces 0.75
            minutes = minutes * 60;//convert to minutes eg. 0.5 will produce 30 mins
            return (int) Math.round(minutes);
        }
    }

    public static double toTime(int hour, int minutes) {
        double ret = ((double) minutes) / 60; //resolve minutes first to avoid silly arithmetic problems
        return ret + hour;
    }

    public static int getHours(double time) {
        if (time < 0 || time >= 24) {
            return 0;
        }
        return (int) time;
    }
}
