package com.pairapp.util;

import com.path.android.jobqueue.Params;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * by Null-Pointer on 11/28/2015.
 */
public abstract class Task extends com.path.android.jobqueue.Job {
    private static final String TAG = "Task";
    private final boolean isValid;

    public Task(Params params) {
        super(params);
        isValid = true;
    }

    public Task() { //required to deserialize task
        super(new Params(1));
        isValid = false;
    }

    protected final boolean isValid() {
        return isValid;
    }

    protected abstract JSONObject toJSON() throws JSONException;

    protected abstract Task fromJSON(JSONObject jsonObject) throws JSONException;

    @Override
    public void onAdded() {
        PLog.d(TAG, "added %s", this);
    }

    @Override
    protected void onCancel() {
        PLog.d(TAG, "canceled %s", this);
    }
}