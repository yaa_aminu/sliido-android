package com.pairapp.util;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;

/**
 * by Null-Pointer on 2/17/2016.
 */
public class CappedArrayList<T> extends ArrayList<T> {
    int max, oldest;

    CappedArrayList(int max) {
        this(0, max);
    }

    CappedArrayList(int size, int max) {
        super(size);
        GenericUtils.ensureConditionTrue(max > 0 && max >= size, "max <= 0 || max < size");
        oldest = 0;
        this.max = max;
    }

    public T getOldest() {
        return get(oldest);
    }

    @Override
    public boolean add(T object) {
        int size = size();
        if (size < max) {
            super.add(++size, object);
        } else {
            super.add(oldest, object);
        }
        if (++oldest == size) { //note the side-effects of ++oldest
            oldest = 0;
        }
        return true;
    }

    @Override
    public void clear() {
        super.clear();
        oldest = 0;
    }

    @Override
    public T set(int index, T object) {
        oops();
        return null;
    }

    @Override
    public void trimToSize() {
        super.trimToSize();
        ensureOldestValid();
    }

    private void ensureOldestValid() {
        int size = size();
        if (oldest >= size) {
            //noinspection StatementWithEmptyBody
            while (--oldest != size && oldest != 0) ; //trim down oldest
        }
    }

    @Override
    public void ensureCapacity(int minimumCapacity) {
        GenericUtils.ensureConditionTrue(minimumCapacity < max, "minimum capacity > max");
        super.ensureCapacity(minimumCapacity);
        ensureOldestValid();
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        oops();
    }

    @Override
    public boolean remove(Object object) {
        if (super.remove(object)) {
            ensureOldestValid();
            return true;
        }
        return false;
    }

    @Override
    public boolean removeAll(@NonNull Collection<?> collection) {
        for (Object o : collection) {
            remove(o);
        }
        return true;
    }

    @Override
    public boolean retainAll(@NonNull Collection<?> collection) {
        oops();
        return true;
    }

    @Override
    public T remove(int index) {
        T removed = super.remove(index);
        if (removed != null) {
           ensureOldestValid();
        }
        return removed;
    }

    @Override
    public void add(int index, T object) {
        oops();
    }

    private void oops() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        GenericUtils.ensureConditionTrue(collection.size() <= max, "collection.size > max"); //data will be lost
        for (T item : collection) {
            add(item);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> collection) {
        oops();
        return true;
    }
}
