package com.pairapp.util;

import android.view.View;
import android.widget.TextView;

/**
 * @author Null-Pointer on 9/20/2015.
 */
public class ViewUtils {

    public static boolean isViewVisible(View view) {
        return view != null && view.getVisibility() == View.VISIBLE;
    }

    private static void hideViewInternal(View view) {
        if (isViewVisible(view)) {
            view.setVisibility(View.GONE);
        }
    }

    private static void showViewInternal(View view) {
        // TODO: 1/23/2016 check for nullability first?
        if (!isViewVisible(view) && view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void hideViews(View... views) {
        for (View view : views) {
            hideViewInternal(view);
        }
    }

    public static void showViews(View... views) {
        for (View view : views) {
            showViewInternal(view);
        }
    }

    public static void setTypeface(TextView textView, String fontName) {
        if (textView != null) {
            textView.setTypeface(TypeFaceUtil.loadFromAssets(fontName));
        }
    }

//    public static void setTypeface(com.rey.material.widget.EditText editText, String fontName) {
//        if (editText != null) {
//            editText.setTypeface(TypeFaceUtil.loadFromAssets(fontName));
//        }
//    }

    public static void showByFlag(View view, boolean flag) {
        if (view != null) {
            view.setVisibility(flag ? View.VISIBLE : View.GONE);
        }
    }

    public static void showByFlag(boolean flag, View... views) {
        for (int i = 0; i < views.length; i++) {
            showByFlag(views[i], flag);
        }
    }

    public static void toggleVisibility(View view) {
        if (view != null) {
            if (view.getVisibility() == View.VISIBLE) {
                view.setVisibility(View.GONE);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    public static void disableAll(View... views) {
        if (views != null) {
            for (int i = 0; i < views.length; i++) {
                final View view = views[i];
                if (view != null) {
                    view.setEnabled(false);
                }
            }
        }
    }

    public static void enable(View... views) {
        if (views != null) {
            for (int i = 0; i < views.length; i++) {
                final View view = views[i];
                if (view != null) {
                    view.setEnabled(true);
                }
            }
        }
    }
}
