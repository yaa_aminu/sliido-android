package com.pairapp.util;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * author Null-Pointer on 1/12/2016.
 */
public class EventsCenter {
    private static final AtomicBoolean initialised = new AtomicBoolean(false);
    private static com.pairapp.util.EventBus eventbus;

    public synchronized static void init() {
        if (!initialised.getAndSet(true)) {
            eventbus = new com.pairapp.util.EventBus();
        }
    }

    public static com.pairapp.util.EventBus getEventBus() {
        ensureInitialised();
        return eventbus;
    }

    private static void ensureInitialised() {
        if (!initialised.get()) {
            throw new IllegalStateException("did you forget to init()!");
        }
    }
}
