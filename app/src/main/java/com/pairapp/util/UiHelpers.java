package com.pairapp.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.sliido.BuildConfig;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

/**
 * @author by Null-Pointer on 5/28/2015.
 */
public class UiHelpers {

    public static void showErrorDialog(Context context, String message) {
        showErrorDialog(context, message, null);
    }

    public static void showDialog(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setTitle(title)
                .create().show();
    }

    private static void showErrorDialog(Context context, String message, DialogInterface.OnClickListener listener) {
        showErrorDialog(context, message, listener, true);
    }

    private static void showErrorDialog(Context context, String message, DialogInterface.OnClickListener listener, boolean cancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!cancelable && listener == null) {
            if (BuildConfig.DEBUG) {
                throw new RuntimeException();
            }
            cancelable = false;
        }
        builder.setMessage(message).setCancelable(cancelable).setPositiveButton(android.R.string.ok, listener);
        if (listener != null) {
            builder.setNegativeButton(android.R.string.no, listener);
        }
        builder.create().show();
    }


    private static String getString(Context context, int resId) {
        return context.getString(resId);
    }

    public static void showToast(String message) {
        showToast(message, Toast.LENGTH_SHORT);
    }

    public static void showToast(String message, int duration) {
        if (duration != LENGTH_LONG && duration != LENGTH_SHORT) {
            duration = LENGTH_SHORT;
        }
        Toast.makeText(Config.getApplicationContext(), message, duration).show();
    }

    public static void showToast(int message) {
        showToast(getString(Config.getApplicationContext(), message));
    }

}
