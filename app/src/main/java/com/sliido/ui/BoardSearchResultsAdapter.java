package com.sliido.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pairapp.util.GenericUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.channel.Channel;

import butterknife.Bind;

/**
 * Created by yaaminu on 11/3/17.
 */

class BoardSearchResultsAdapter extends SliidoBaseAdapter<Channel> {
    public BoardSearchResultsAdapter(Delegate<Channel> delegate) {
        super(delegate);
    }

    @Override
    protected void doBindHolder(Holder holder, int position) {
        final Channel item = getItem(position);
        ((BoardSearchItemHolder) holder).channelName.setText(item.getName());
        ((BoardSearchItemHolder) holder).createdBy.setText(item.getCreatedBy());
        ((BoardSearchItemHolder) holder).subscribersCount.setText(GenericUtils.getString(R.string.followers, item.getSubscribers()));
    }

    @Override
    protected Holder doCreateHolder(ViewGroup parent, int viewType) {
        return new BoardSearchItemHolder(inflater.inflate(R.layout.board_search_list_item, parent, false));
    }
}

class BoardSearchItemHolder extends SliidoBaseAdapter.Holder {
    @Bind(R.id.tv_board_name)
    TextView channelName;
    @Bind(R.id.tv_channel_created_by_name)
    TextView createdBy;
    @Bind(R.id.tv_subscribers_count)
    TextView subscribersCount;

    public BoardSearchItemHolder(View view) {
        super(view);
    }
}
