package com.sliido.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.dao.data.ShoutOut;
import com.sliido.dao.data.ShoutOutManager;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by yaaminu on 11/12/17.
 */

public class ShoutOutFragment extends BaseFragment {


    private final ShoutOutManager shoutOutManager = ShoutOutManager.getInstance();
    @Bind(R.id.fab_shout_out)
    View shoutOut;

    @Bind(R.id.shout_outs_list)
    RecyclerView recyclerView;
    RealmResults<ShoutOut> shoutouts;
    Realm realm;
    ShoutoutAdapter.Delegate delegate = new ShoutoutAdapter.Delegate() {
        @Override
        public void like(ShoutOut shoutOut) {
            shoutOutManager
                    .like(realm, shoutOut);
        }

        @Override
        public void repost(ShoutOut shoutOut) {
            shoutOutManager
                    .repost(realm, shoutOut);
        }

        @Override
        public void onItemClick(SliidoBaseAdapter<ShoutOut> adapter, View view, int position, long id) {

        }

        @Override
        public boolean onItemLongClick(SliidoBaseAdapter<ShoutOut> adapter, View view, int position, long id) {
            return false;
        }

        @NonNull
        @Override
        public List<ShoutOut> dataSet() {
            return shoutouts;
        }

        @Override
        public boolean showLoadingMore() {
            return false;
        }

        @Override
        public Context context() {
            return getContext();
        }

        @Override
        public void onLoadMoreItems(int position) {

        }
    };
    private ShoutoutAdapter adapter;
    final RealmChangeListener<RealmResults<ShoutOut>> changeListener = new RealmChangeListener<RealmResults<ShoutOut>>() {
        @Override
        public void onChange(RealmResults<ShoutOut> shoutOuts) {
            if (getActivity() != null) {
                adapter.notifyDataChanged();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = ShoutOut.REALM();
        shoutOutManager.loadNewShoutouts();
    }

    @Override
    public void onDestroy() {
        shoutouts.removeChangeListener(changeListener);
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        shoutouts = realm.where(ShoutOut.class).findAllSortedAsync("updatedAt", Sort.DESCENDING);
        shoutouts.addChangeListener(changeListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ShoutoutAdapter(delegate);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_shout_out;
    }


    @OnClick(R.id.fab_shout_out)
    void shoutOut() {
        startActivity(new Intent(getContext(), AddShoutOutActivity.class));
    }
}

