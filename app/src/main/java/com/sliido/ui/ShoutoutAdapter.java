package com.sliido.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LruCache;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pairapp.util.GenericUtils;
import com.pairapp.util.ThreadUtils;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.dao.data.Attachment;
import com.sliido.dao.data.ShoutOut;
import com.sliido.data.post.Post;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.OnClick;

import static com.pairapp.util.TaskManager.execute;
import static com.pairapp.util.TaskManager.executeOnMainThread;

/**
 * Created by yaaminu on 11/15/17.
 */

public class ShoutoutAdapter extends SliidoBaseAdapter<ShoutOut> {

    private final Delegate delegate;
    private final int darkGray;
    private final int colorPrimary;
    LruCache<String, Bitmap> videoThumbnails = new LruCache<>(10);

    public ShoutoutAdapter(Delegate delegate) {
        super(delegate);
        this.delegate = delegate;
        darkGray = ContextCompat.getColor(getContext(), R.color.dark_gray);
        colorPrimary = ContextCompat.getColor(getContext(), R.color.colorPrimary);
    }

    @Override
    public void onViewRecycled(Holder holder) {
        holder.itemView.setTag(null);
        for (int i = 0; i < ((ShoutOutHolder) holder).mediaLayout.getChildCount(); i++) {
            ((ImageView) ((ShoutOutHolder) holder).mediaLayout.getChildAt(i))
                    .setImageDrawable(null);
        }
        super.onViewRecycled(holder);
    }

    @Override
    protected void doBindHolder(Holder holder, int position) {
        ShoutOut shoutOut = getItem(position);
        holder.itemView.setTag(shoutOut);
        GenericUtils.assertThat(shoutOut != null);
        ((ShoutOutHolder) holder)
                .postedBy.setText(shoutOut.getPostedByName());
        if (shoutOut.getState() == ShoutOut.POSTED) {
            long now = System.currentTimeMillis();
            long then = shoutOut.getCreatedAt();
            CharSequence formattedDate;
            formattedDate = ((now - then) < 1000 * 60) ? getContext().getString(R.string.just_now)
                    : DateUtils.getRelativeTimeSpanString(then, now, DateUtils.MINUTE_IN_MILLIS);
            ((ShoutOutHolder) holder).datePosted.setText(formattedDate);
        } else {
            ((ShoutOutHolder) holder).datePosted.setText(shoutOut.getState() == Post.POSTING ? R.string.posting : R.string.post_failed);
        }
        ViewUtils.showByFlag(((ShoutOutHolder) holder).postbody, !GenericUtils.isEmpty(shoutOut.getPostBody()));
        ViewUtils.showByFlag(shoutOut.getState() == ShoutOut.POSTING, ((ShoutOutHolder) holder).progress, ((ShoutOutHolder) holder).cancel);
        ViewUtils.showByFlag(shoutOut.getState() == ShoutOut.POST_FAILED, ((ShoutOutHolder) holder).retry);
        ((ShoutOutHolder) holder).postbody.setText(shoutOut.getPostBody());
        ((ShoutOutHolder) holder).like.setText(getContext().getString(R.string.shout_out_likes, shoutOut.getLikes()));
        ((ShoutOutHolder) holder).repost.setText(getContext().getString(R.string.reposts, shoutOut.getReposts()));
        ((ShoutOutHolder) holder).like.setCompoundDrawablesWithIntrinsicBounds(shoutOut.isLiked() ? R.drawable.ic_favorite_black_24dp :
                R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
        ((ShoutOutHolder) holder).like.setTextColor(shoutOut.isLiked() ? colorPrimary :
                darkGray);

        ((ShoutOutHolder) holder).repost.setCompoundDrawablesWithIntrinsicBounds(shoutOut.isReposted() ? R.drawable.ic_repost_color :
                R.drawable.ic_repost, 0, 0, 0);
        ((ShoutOutHolder) holder).repost.setTextColor(shoutOut.isReposted() ? colorPrimary :
                darkGray);

        ViewUtils.showByFlag(!shoutOut.getAttachments().isEmpty(), ((ShoutOutHolder) holder).mediaLayout);
        if (!shoutOut.getAttachments().isEmpty()) {
            for (int i = 0; i < ((ShoutOutHolder) holder).mediaLayout.getChildCount(); i++) {
                ViewUtils.hideViews(((ShoutOutHolder) holder).mediaLayout.getChildAt(i));
            }
            for (int i = 0; i < shoutOut.getAttachments().size(); i++) {
                loadMedia(shoutOut.getAttachments().get(i), (ImageView) ((ShoutOutHolder) holder).mediaLayout.getChildAt(i));
            }
        }
    }

    private void loadMedia(Attachment attachment, ImageView view) {
        ViewUtils.showViews(view);
        if (attachment.getType() == Post.VIDEO) {
            //don't waste time if it's not downloaded yet
            Bitmap bitmap = !attachment.getUrl().startsWith("http") ? videoThumbnails.get(attachment.getUrl()) : null;
            if (bitmap != null) {
                view.setImageBitmap(bitmap);
            } else {
                view.setImageResource(R.drawable.light_color_primary);
                createThumbnail(attachment.getUrl());
            }
        } else if (attachment.getType() == Post.IMAGE) {
            Picasso.with(getContext())
                    .load(attachment.getUrl())
                    .placeholder(R.drawable.light_color_primary)
                    .error(R.drawable.light_color_primary)
                    .into(view);
        } else {
            throw new AssertionError();
        }
    }

    private void createThumbnail(final String attachmentUrl) {
        if (!attachmentUrl.startsWith("http") && new File(attachmentUrl).exists()) {
            execute(new Runnable() {
                @Override
                public void run() {
                    if (ThreadUtils.isMainThread()) {
                        notifyDataChanged();
                    } else {
                        videoThumbnails.put(attachmentUrl, ThumbnailUtils.createVideoThumbnail(attachmentUrl,
                                MediaStore.Images.Thumbnails.MICRO_KIND));
                        executeOnMainThread(this);
                    }
                }
            }, false);
        }
    }

    @Override
    protected Holder doCreateHolder(ViewGroup parent, int viewType) {
        return new ShoutOutHolder(inflater.inflate(R.layout.shout_out_list_item, parent, false), delegate);
    }

    interface Delegate extends SliidoBaseAdapter.Delegate<ShoutOut> {
        void like(ShoutOut shoutOut);

        void repost(ShoutOut shoutOut);
    }
}

class ShoutOutHolder extends SliidoBaseAdapter.Holder {
    private final ShoutoutAdapter.Delegate delegate;
    @Bind(R.id.tv_posted_by)
    TextView postedBy;
    @Bind(R.id.tv_post_body)
    TextView postbody;
    @Bind(R.id.tv_like_post)
    TextView like;
    @Bind(R.id.repost)
    TextView repost;
    @Bind(R.id.media_layout)
    ViewGroup mediaLayout;
    @Bind(R.id.post_progress)
    View progress;
    @Bind(R.id.tv_date_posted)
    TextView datePosted;
    @Bind(R.id.bt_cancel)
    View cancel;
    @Bind(R.id.bt_retry)
    View retry;

    public ShoutOutHolder(View view, ShoutoutAdapter.Delegate delegate) {
        super(view);
        this.delegate = delegate;
    }

    @OnClick({R.id.repost, R.id.tv_like_post})
    void repostOrLike(View view) {
        switch (view.getId()) {
            case R.id.tv_like_post:
                delegate.like(((ShoutOut) itemView.getTag()));
                break;
            case R.id.repost:
                delegate.repost(((ShoutOut) itemView.getTag()));
                break;
            default:
                throw new AssertionError();
        }
    }

    @OnClick({R.id.media_entry_4, R.id.media_entry_1, R.id.media_entry_2, R.id.media_entry_3})
    void viewImage(View v) {
        ShoutOut shoutOut = ((ShoutOut) itemView.getTag());
        switch (v.getId()) {
            case R.id.media_entry_1:
                openAttachment(shoutOut.getAttachments().get(0));
                break;
            case R.id.media_entry_2:
                openAttachment(shoutOut.getAttachments().get(1));
                break;
            case R.id.media_entry_3:
                openAttachment(shoutOut.getAttachments().get(2));
                break;
            case R.id.media_entry_4:
                openAttachment(shoutOut.getAttachments().get(3));
                break;
            default:
                throw new AssertionError();
        }
    }

    private void openAttachment(Attachment attachment) {
        if (attachment.getType() == Post.IMAGE) {
            Intent intent = new Intent(itemView.getContext(), ImageViewerActivity.class);
            intent.putExtra(ImageViewerActivity.EXTRA_IMAGE, attachment.getUrl());
            itemView.getContext().startActivity(intent);
        } else {
            throw new AssertionError("unsupported attachment type");
        }

    }
}
