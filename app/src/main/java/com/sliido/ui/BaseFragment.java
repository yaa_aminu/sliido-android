package com.sliido.ui;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairapp.util.EventBus;

import butterknife.ButterKnife;

/**
 * author Null-Pointer on 1/15/2016.
 */
public abstract class BaseFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public boolean onBackPressed() {
        return false;
    }

    protected abstract
    @LayoutRes
    int getLayout();

    protected class Listener implements EventBus.EventsListener {
        @Override
        public int threadMode() {
            return EventBus.MAIN;
        }

        @Override
        public void onEvent(EventBus bus, EventBus.Event event) {
            throw new UnsupportedOperationException("override this!");
        }

        @Override
        public boolean sticky() {
            return true;
        }
    }
}
