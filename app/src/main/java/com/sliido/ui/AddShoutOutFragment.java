package com.sliido.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.LruCache;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.pairapp.util.Config;
import com.pairapp.util.FileUtils;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.SimpleDateUtil;
import com.pairapp.util.TaskManager;
import com.pairapp.util.UiHelpers;
import com.sliido.R;
import com.sliido.data.post.Post;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by yaaminu on 11/12/17.
 */

public final class AddShoutOutFragment extends BaseFragment {
    public static final String PHOTO_URL = "photo_url";
    public static final String ATTACHMENTS = "attachments";
    private static final String TAG = "AddShoutOutFragment";
    private static final int REQUEST_CODE_PICK_PIC = 10001;
    private static final int REQUEST_CODE_PICK_VID = 10002;
    private static final int REQUEST_CODE_TAKE_PIC = 10003;
    @Bind(R.id.et_shout_out)
    EditText etPostBody;
    @Nullable
    ArrayList<String> attachments;
    @Bind(R.id.media_layout)
    GridLayout media_layout;
    @NonNull
    android.support.v4.util.LruCache<String, Bitmap> videoThumnails = new LruCache<>(4);
    @Nullable
    private Callbacks callbacks;
    @Nullable
    private Uri photoUri;

    @Override
    protected int getLayout() {
        return R.layout.fragment_add_shout_out;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        GenericUtils.assertThat(context instanceof Callbacks);
        callbacks = ((Callbacks) context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            photoUri = savedInstanceState.getParcelable(PHOTO_URL);
            attachments = savedInstanceState.getStringArrayList(ATTACHMENTS);
        }
        GenericUtils.assertThat(callbacks != null, "must inject callback");
    }

    @OnClick({R.id.media_entry_1, R.id.media_entry_2, R.id.media_entry_3, R.id.media_entry_4})
    void onImageClicked(View v) {
        switch (v.getId()) {
            case R.id.media_entry_1:
                showDialog(0);
                break;
            case R.id.media_entry_2:
                showDialog(1);
                break;
            case R.id.media_entry_3:
                showDialog(2);
                break;
            case R.id.media_entry_4:
                showDialog(3);
                break;
            default:
                throw new AssertionError();
        }
    }

    private void showDialog(final int index) {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.confirm_delete)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (attachments != null) {
                            if (index < attachments.size() - 1) { //not last entry
                                //move higher entries up
                                for (int i = index + 1; i < attachments.size(); i++) {
                                    ((ImageView) media_layout.getChildAt(i - 1))
                                            .setImageDrawable(((ImageView) media_layout.getChildAt(i)).getDrawable());
                                }
                                for (int i = attachments.size() - 1; i < media_layout.getChildCount(); i++) {
                                    final ImageView childAt = (ImageView) media_layout.getChildAt(i);
                                    childAt.setVisibility(View.GONE);
                                    childAt.setImageDrawable(null);
                                }
                                attachments.remove(attachments.size() - 1);
                            } else {
                                attachments.remove(index);
                                final ImageView childAt = (ImageView) media_layout.getChildAt(index);
                                childAt.setVisibility(View.GONE);
                                childAt.setImageDrawable(null);
                            }
                        }
                    }
                }).setNegativeButton(android.R.string.no, null).create().show();
    }

    @OnClick(R.id.tv_post_shoutout)
    void postShoutOut() {
        final String postBody = etPostBody.getText().toString().trim();
        if (!GenericUtils.isEmpty(postBody) || (attachments != null && !attachments.isEmpty())) {
            GenericUtils.assertThat(callbacks != null, "must inject callback");
            callbacks.post(postBody, attachments != null ? attachments : Collections.<String>emptyList());
            etPostBody.setText("");
            attachments = null;
            videoThumnails.evictAll();
            for (int i = 0; i < media_layout.getChildCount(); i++) {
                final ImageView childAt = (ImageView) media_layout.getChildAt(i);
                childAt.setVisibility(View.GONE);
                childAt.setImageDrawable(null);
            }
        } else {
            UiHelpers.showErrorDialog(getContext(), "post cannot be empty");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (photoUri != null) {
            outState.putParcelable(PHOTO_URL, photoUri);
        }
        if (attachments != null && !attachments.isEmpty()) {
            outState.putStringArrayList(ATTACHMENTS, attachments);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_VID || requestCode == REQUEST_CODE_PICK_PIC || requestCode == REQUEST_CODE_TAKE_PIC) {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case REQUEST_CODE_PICK_PIC:
                    case REQUEST_CODE_PICK_VID:
                        handleUri(data.getData());
                        break;
                    case REQUEST_CODE_TAKE_PIC:
                        handleUri(photoUri);
                        break;
                    default:
                        throw new AssertionError("impossible");
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleUri(final Uri uri) {
        final ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.setMessage(getString(R.string.progress_retrieving_file));
        dialog.setCancelable(false);
        dialog.show();
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                final String localPath = com.pairapp.util.FileUtils.resolveContentUriToFilePath(uri, true);
                PLog.d(TAG, "retrieved file path %s", localPath);
                if (FileUtils.getMimeType(localPath).startsWith("video/")) {
                    final Bitmap videoThumbnail = ThumbnailUtils.createVideoThumbnail(localPath, MediaStore.Images.Thumbnails.MICRO_KIND);
                    if (videoThumbnail != null) {
                        videoThumnails.put(localPath, videoThumbnail);
                    }
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        if (attachments == null) {
                            attachments = new ArrayList<>(4);
                        }
                        File file = new File(localPath);
                        if (file.exists()) {
                            if (file.length() <= Post.MAX_ATTACHMENT_SIZE) {
                                attachments.add(file.getAbsolutePath());
                                addImage(file, attachments.size() - 1);
                            } else {
                                UiHelpers.showDialog(getContext(), getString(R.string.error), getString(R.string.file_too_large, Post.MAX_ATTACHMENT_SIZE));
                            }
                        } else {
                            UiHelpers.showDialog(getContext(), getString(R.string.error), getString(R.string.error_file_not_found));
                        }
                    }
                });
            }
        }, false);
    }

    private void addImage(File image, int index) {
        if (index > 3) {
            UiHelpers.showErrorDialog(getContext(), "can't add more than 4 media");
            return;
        }
        if (getView() != null) {
            final ImageView childAt = (ImageView) media_layout.getChildAt(index);
            childAt.setVisibility(View.VISIBLE);
            final Bitmap bitmap = videoThumnails.get(image.getAbsolutePath());
            if (bitmap != null) {
                childAt.setImageBitmap(bitmap);
            } else {
                Picasso.with(getContext())
                        .load(image)
                        .placeholder(R.drawable.light_color_primary)
                        .error(R.drawable.light_color_primary)
                        .into(childAt);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (GenericUtils.wasPermissionGranted(permissions, grantResults)) {
            if (requestCode == REQUEST_CODE_PICK_PIC) {
                pickMedia("image/*");
            } else if (requestCode == REQUEST_CODE_PICK_VID) {
                pickMedia("video/*");
            } else if (requestCode == REQUEST_CODE_TAKE_PIC) {
                takePicture();
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @OnClick({R.id.add_photo, R.id.take_photo, R.id.add_video})
    void attach(View view) {
        if (attachments != null && attachments.size() >= 4) {
            UiHelpers.showErrorDialog(getContext(), "Can't attach more than 4 media");
            return;
        }
        int i = view.getId();
        if (i == R.id.add_photo) {
            if (GenericUtils.hasPermission(this, true, REQUEST_CODE_PICK_PIC, WRITE_EXTERNAL_STORAGE)) {
                pickMedia("image/*");
            }
        } else if (i == R.id.add_video) {
            if (GenericUtils.hasPermission(this, true, REQUEST_CODE_PICK_VID, WRITE_EXTERNAL_STORAGE)) {
                pickMedia("video/*");
            }
        } else if (i == R.id.take_photo) {
            if (GenericUtils.hasPermission(this, true, REQUEST_CODE_TAKE_PIC, WRITE_EXTERNAL_STORAGE, CAMERA)) {
                takePicture();
            }
        } else {
            throw new AssertionError();
        }
    }

    private void takePicture() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            photoUri = Uri.fromFile(new File(Config.getAppBinFilesBaseDir(), "images_shoutout_" + SimpleDateUtil.timeStampNow() + ".jpg"));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PIC);
        } catch (FileNotFoundException e) {
            UiHelpers.showErrorDialog(getContext(), e.getMessage());
        }
    }

    private void pickMedia(String type) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(type);
        startActivityForResult(intent, REQUEST_CODE_PICK_PIC);
    }

    public interface Callbacks {
        void post(String postBody, List<String> attachments);
    }
}
