package com.sliido.ui;

import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.NotificationEntry;

import butterknife.Bind;

/**
 * Created by yaaminu on 11/3/17.
 */

class NotificationsAdapter extends SliidoBaseAdapter<NotificationEntry> {
    public NotificationsAdapter(SliidoBaseAdapter.Delegate<NotificationEntry> delegate) {
        super(delegate);
    }


    @Override
    protected void doBindHolder(Holder holder, int position) {
        NotificationEntry notificationEntry = getItem(position);
        ((NotificationEntryHolder) holder).notificationTitle.setText(notificationEntry
                .getText(getContext()));
        ((NotificationEntryHolder) holder).notificationDate.setText(DateUtils.formatDateTime(getContext(),
                notificationEntry.getDate(), DateUtils.FORMAT_ABBREV_ALL | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME));
        ((NotificationEntryHolder) holder).notificationIcon.setImageResource(notificationEntry.getType() == NotificationEntry.TYPE_COMMENT ?
                R.drawable.ic_comment_black_24dp : R.drawable.ic_favorite_black_48dp);
    }

    @Override
    protected int doGetViewType(int position) {
        return getItem(position).isRead() ? 1 : 2;
    }

    @Override
    protected Holder doCreateHolder(ViewGroup parent, int viewType) {
        return new NotificationEntryHolder(inflater.inflate(viewType == 1 ? R.layout.notifications_list_item_read : R.layout.notifications_list_item, parent, false));
    }
}

class NotificationEntryHolder extends SliidoBaseAdapter.Holder {
    @Bind(R.id.tv_notification_title)
    TextView notificationTitle;
    @Bind(R.id.tv_date)
    TextView notificationDate;
    @Bind(R.id.iv_notification_icon)
    ImageView notificationIcon;

    public NotificationEntryHolder(View view) {
        super(view);
    }


}
