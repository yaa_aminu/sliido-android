package com.sliido.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.sliido.R;


/**
 * Created by yaaminu on 11/15/17.
 */

public class ImageViewerActivity extends AppCompatActivity {

    public static final String EXTRA_IMAGE = "image";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        Fragment fragment = new ImageViewerFragment();
        Bundle args = new Bundle(1);
        args.putString(ImageViewerFragment.IMAGE, getIntent().getStringExtra(EXTRA_IMAGE));
        fragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.image_view_fragment_container, fragment, "image_viewer")
                .commit();
    }
}
