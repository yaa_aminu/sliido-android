package com.sliido.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.ui.channel.ChannelDetailActivity;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by yaaminu on 11/3/17.
 */

public class BoardSearchResultsFragment extends BaseFragment implements EventBus.EventsListener {
    private static final String TAG = "BoardSearchResultsFragm";

    @Bind(R.id.recyclerView)
    RecyclerView searchResults;
    @Bind(R.id.tv_empty_view)
    TextView emptyView;
    List<Channel> channels = Collections.emptyList();
    private BoardSearchResultsAdapter adapter;
    private final RealmChangeListener<Realm> changeListener = new RealmChangeListener<Realm>() {
        @Override
        public void onChange(Realm realm) {
            adapter.notifyDataChanged();
        }
    };
    private volatile boolean searching = false;
    private final SliidoBaseAdapter.Delegate<Channel> delegate = new SliidoBaseAdapter.Delegate<Channel>() {
        @Override
        public void onItemClick(SliidoBaseAdapter<Channel> adapter, View view, int position, long id) {
            // TODO: 11/3/17 make MainActivity handle this click event
            final Channel item = adapter.getItem(position);
            ChannelDetailActivity.viewChannel(getContext(), item.getChannelId(),
                    item.getName(), BoardSearchResultsFragment.class.getName());
            ((MainActivity) getActivity()).closeSearch();
        }

        @Override
        public boolean onItemLongClick(SliidoBaseAdapter<Channel> adapter, View view, int position, long id) {
            return false;
        }

        @NonNull
        @Override
        public List<Channel> dataSet() {
            if (channels.isEmpty()) {
                ViewUtils.showViews(emptyView);
                ViewUtils.hideViews(searchResults);
                emptyView.setText(searching ? R.string.search_in_progress : R.string.nothing_to_show);
            } else {
                ViewUtils.hideViews(emptyView);
                ViewUtils.showViews(searchResults);
            }
            return channels;
        }

        @Override
        public boolean showLoadingMore() {
            return false;
        }

        @Override
        public Context context() {
            return getContext();
        }

        @Override
        public void onLoadMoreItems(int position) {

        }
    };
    Action1<Integer> onResults = new Action1<Integer>() {
        public void call(Integer count) {
            if (searching) {
                searching = false;
                adapter.notifyDataChanged();
            }
        }
    };
    Action1<Throwable> onError = new Action1<Throwable>() {
        public void call(Throwable throwable) {
            PLog.e(TAG, throwable.getMessage(), throwable);
            if (searching) {
                searching = false;
                adapter.notifyDataChanged();
                Toast.makeText(getContext(), throwable.getMessage()
                        , Toast.LENGTH_SHORT).show();
            }
        }
    };
    private Realm realm;

    @Override
    protected int getLayout() {
        return R.layout.fragment_board_search_results;
    }

    @Override
    public void onResume() {
        super.onResume();
        realm.addChangeListener(changeListener);
        EventsCenter.getEventBus()
                .register(MainActivity.SEARCH_TEXT_CHANGED, this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        realm.removeChangeListener(changeListener);
        EventsCenter.getEventBus()
                .unregister(MainActivity.SEARCH_TEXT_CHANGED, this);
        super.onPause();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchResults.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BoardSearchResultsAdapter(delegate);
        searchResults.setAdapter(adapter);
    }

    @Override
    public int threadMode() {
        return EventBus.MAIN;
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        doSearch(((String) event.data));
    }

    private void doSearch(String query) {
        if (GenericUtils.isEmpty(query)) {
            searching = false;
            channels = Collections.emptyList();
            adapter.notifyDataChanged();
            return;
        }
        PLog.d(TAG, query);
        channels = realm.where(Channel.class)
                .contains(Channel.FIELD_NAME, query, Case.INSENSITIVE)
                .findAllSorted(Channel.FIELD_NAME);

        searching = true;
        adapter.notifyDataChanged();
        ChannelManager.search(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onResults, onError);
    }

    @Override
    public boolean sticky() {
        return false;
    }
}
