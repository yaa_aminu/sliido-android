package com.sliido.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.pairapp.Errors.SliidoException;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.NotificationEntry;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.post.Post;
import com.sliido.ui.posts.CommentsActivity;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.Sort;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by yaaminu on 11/3/17.
 */

public class NotificationsFragment extends BaseFragment implements RealmChangeListener<Realm> {
    @Bind(R.id.recyclerView)
    RecyclerView notifications;
    @Bind(R.id.empty_view)
    View emptyView;
    List<NotificationEntry> notificationEntries = Collections.emptyList();
    Realm realm;
    private final SliidoBaseAdapter.Delegate<NotificationEntry> delegate = new SliidoBaseAdapter.Delegate<NotificationEntry>() {
        @Override
        public void onItemClick(final SliidoBaseAdapter<NotificationEntry> adapter, View view, final int position, long id) {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage(getString(R.string.please_wait));
            progressDialog.show();
            Observable.just(adapter.getItem(position).getPostId())
                    .map(new Func1<String, Post>() {
                        @Override
                        public Post call(String postid) {
                            try {
                                return ChannelManager.getPostSync(postid);
                            } catch (SliidoException e) {
                                throw Exceptions.propagate(e);
                            }
                        }
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Post>() {
                        @Override
                        public void call(Post post) {
                            if (getActivity() != null) {
                                progressDialog.dismiss();
                                Intent intent = new Intent(getContext(), CommentsActivity.class);
                                intent.putExtra(CommentsActivity.EXTRA_POST_ID, post.getPostId());
                                getActivity().startActivity(intent);
                                realm.beginTransaction();
                                adapter.getItem(position).setRead(true);
                                realm.commitTransaction();
                            }
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            if (getActivity() != null) {
                                progressDialog.dismiss();
                                UiHelpers.showErrorDialog(getContext(), throwable.getMessage());
                            }
                        }
                    });
        }

        @Override
        public boolean onItemLongClick(SliidoBaseAdapter<NotificationEntry> adapter, View view, int position, long id) {
            return false;
        }

        @NonNull
        @Override
        public List<NotificationEntry> dataSet() {
            ViewUtils.showByFlag(notificationEntries.isEmpty(), emptyView);
            ViewUtils.showByFlag(!notificationEntries.isEmpty(), notifications);
            return notificationEntries;
        }

        @Override
        public boolean showLoadingMore() {
            return false;
        }

        @Override
        public Context context() {
            return getContext();
        }

        @Override
        public void onLoadMoreItems(int position) {

        }
    };
    private NotificationsAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Channel.REALM();
    }

    @Override
    public void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.frament_notifications;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notificationEntries = realm.where(NotificationEntry.class).findAllSorted("date", Sort.DESCENDING);
        notifications.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new NotificationsAdapter(delegate);
        realm.addChangeListener(this);
        notifications.setAdapter(adapter);
    }

    @Override
    public void onChange(Realm realm) {
        adapter.notifyDataChanged();
    }
}
