package com.sliido.ui.settings;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.Errors.ErrorCenter;
import com.pairapp.Errors.SliidoException;
import com.sliido.R;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.user.UserManager;
import com.sliido.ui.BaseFragment;
import com.sliido.ui.MainActivity;

import java.util.Locale;

import butterknife.Bind;
import butterknife.OnCheckedChanged;

public class SettingsFragment extends BaseFragment {

    @Bind(R.id.cb_enable_post_notifications)
    CheckBox enableNotifications;

    @Override
    protected int getLayout() {
        return R.layout.activity_settings;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        enableNotifications.setChecked(ChannelManager.isNotificationsEnabled());
    }

    @SuppressLint("CommitPrefEdits")
    @OnCheckedChanged({R.id.cb_enable_post_notifications})
    public void onChecked(boolean checked) {
        ChannelManager.enableOrDisableNotifications(checked);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.settings_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_feedback:
                intent = new Intent(getContext(), FeedBackActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(getContext(), AboutActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                confirmAndLogout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void confirmAndLogout() {
        Answers.getInstance().logCustom(new CustomEvent("logoutAttempt")
                .putCustomAttribute("locale", Locale.getDefault().getCountry())
                .putCustomAttribute("androidVersion", Build.VERSION.SDK_INT)
                .putCustomAttribute("deviceModel", Build.MODEL));
        new AlertDialog.Builder(getContext())
                .setMessage(getString(R.string.logout_confirmation_prompt))
                .setTitle(getString(R.string.confirm_logout))
                .setNegativeButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        completeLogout();
                    }
                }).setPositiveButton(android.R.string.cancel, null)
                .create().show();
    }

    private void completeLogout() {
        final ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.setMessage(getString(R.string.logging_out));
        dialog.setCancelable(false);
        dialog.show();
        UserManager.getInstance().logout(new UserManager.Callback() {
            @Override
            public void done(SliidoException e) {
                dialog.dismiss();
                if (e == null) {
                    Answers.getInstance().logCustom(new CustomEvent("logoutAttempt")
                            .putCustomAttribute("locale", Locale.getDefault().getCountry())
                            .putCustomAttribute("androidVersion", Build.VERSION.SDK_INT)
                            .putCustomAttribute("deviceModel", Build.MODEL)
                            .putCustomAttribute("success", "true"));
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Answers.getInstance().logCustom(new CustomEvent("logoutAttempt")
                            .putCustomAttribute("locale", Locale.getDefault().getCountry())
                            .putCustomAttribute("androidVersion", Build.VERSION.SDK_INT)
                            .putCustomAttribute("deviceModel", Build.MODEL)
                            .putCustomAttribute("success", "false"));
                    ErrorCenter.reportError("logout.error", e.getUserFriendlyMessage(), ErrorCenter.ReportStyle.DIALOG, ErrorCenter.INDEFINITE);
                }
            }
        });
    }
}

