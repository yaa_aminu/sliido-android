package com.sliido.ui.settings;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.pairapp.util.EventBus;
import com.pairapp.util.UiHelpers;
import com.sliido.BuildConfig;
import com.sliido.R;
import com.sliido.data.user.UserManager;
import com.sliido.ui.SliidoBaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedBackActivity extends SliidoBaseActivity {


    private static final String ADD_SYSTEM_INFO = "addSystemInfo";
    private static final String SUBJECT = "subject";
    private static final String MESSAGE = "message";

    @Bind(R.id.et_feedback_body)
    EditText feedbackBody;

    @Bind(R.id.et_feedback_subject)
    EditText subjectEt;

    @Bind(R.id.add_system_info)
    CheckBox checkBox;

    @Bind(R.id.bt_submit)
    Button submit;

    @Bind(R.id.toolbar)
    Toolbar toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.feedback);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        submit.setOnClickListener(onClickListener);
        if (savedInstanceState != null) {
            String title = savedInstanceState.getString(SUBJECT);
            if (title != null) {
                subjectEt.setText(title);
            }
            title = savedInstanceState.getString(MESSAGE);
            if (title != null) {
                feedbackBody.setText(title);
            }
            checkBox.setChecked(savedInstanceState.getBoolean(ADD_SYSTEM_INFO, false));
        }
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String title = savedInstanceState.getString(SUBJECT);
        if (title != null) {
            subjectEt.setText(title);
        }
        title = savedInstanceState.getString(MESSAGE);
        if (title != null) {
            feedbackBody.setText(title);
        }
        checkBox.setChecked(savedInstanceState.getBoolean(ADD_SYSTEM_INFO, false));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(ADD_SYSTEM_INFO, checkBox.isChecked());
        outState.putString(SUBJECT, subjectEt.getText().toString());
        outState.putString(MESSAGE, feedbackBody.getText().toString());
        super.onSaveInstanceState(outState);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String feedbackBody = FeedBackActivity.this.feedbackBody.getText().toString().trim();
            FeedBackActivity.this.feedbackBody.setText("");
            if (TextUtils.isEmpty(feedbackBody)) {
                UiHelpers.showErrorDialog(FeedBackActivity.this, getString(R.string.error_feedback_body_empty));
                return;
            }

            String title = subjectEt.getText().toString().trim();
            if (TextUtils.isEmpty(title)) {
                title = "no subject";
            } else if (title.length() > 40) {
                UiHelpers.showErrorDialog(FeedBackActivity.this, getString(R.string.error_feedback_subject_too_lonng));
                return;
            }
            subjectEt.setText("");
            JSONObject reportObject = new JSONObject();
            try {
                reportObject.put("body", feedbackBody);
                if (checkBox.isChecked()) {
                    reportObject.put("device", Build.DEVICE);
                    reportObject.put("model", Build.MODEL);
                    reportObject.put("manufacturer", Build.MANUFACTURER);
                    //noinspection ConstantConditions
                    reportObject.put("reportedBy", UserManager.getInstance().getCurrentUser().getEmail());
                    reportObject.put("time", new Date());
                    reportObject.put("version", BuildConfig.VERSION_NAME);
                    reportObject.put("versionCode", BuildConfig.VERSION_CODE);
                    reportObject.put("apiVersion", Build.VERSION.SDK_INT);
                    reportObject.put("title", title);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        reportObject.put("arc", TextUtils.join(",", Build.SUPPORTED_ABIS));
                    } else {
                        //noinspection deprecation
                        String[] abi = {
                                Build.CPU_ABI,
                                Build.CPU_ABI2
                        };
                        reportObject.put("arc", TextUtils.join(",", abi));
                    }
                    reportObject.put("locale", Locale.getDefault().getDisplayCountry());
                }

                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:feedbacks.sliido@gmail.com"));
                intent.putExtra(Intent.EXTRA_TEXT, reportObject.toString());
                intent.putExtra(Intent.EXTRA_SUBJECT, title);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    UiHelpers.showToast(getString(R.string.no_app_for_mails));
                }
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    };

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        //do nothing
    }
}
