package com.sliido.ui.settings;

import android.os.Bundle;

import com.pairapp.util.EventBus;
import com.pairapp.util.PLog;
import com.sliido.R;
import com.sliido.ui.SliidoBaseActivity;

public class AboutActivity extends SliidoBaseActivity {

    private static final String TAG = "AboutActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getFManager().beginTransaction().replace(R.id.container, new AboutFragment()).commit();
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        PLog.d(TAG, "event");
    }
}
