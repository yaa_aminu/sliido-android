package com.sliido.ui.settings;


import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.pairapp.util.PLog;
import com.pairapp.util.UiHelpers;
import com.sliido.BuildConfig;
import com.sliido.R;
import com.sliido.SliidoApplication;
import com.sliido.ui.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends BaseFragment {
    private static final String TAG = AboutFragment.class.getSimpleName();


    @Bind(R.id.tv_app_version)
    TextView tvAppVersion;

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_about;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvAppVersion.setText("v." + BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")");
    }

    @OnClick({R.id.tv_license, R.id.terms_and_agreement, R.id.privacy_policy})
    void onClick(View view) {
        final String url;
        switch (view.getId()) {
            case R.id.privacy_policy:
                url = SliidoApplication.PRIVACY_POLICY;
                break;
            case R.id.terms_and_agreement:
                url = SliidoApplication.TERMS_URL;
                break;
            case R.id.tv_license:
                url = SliidoApplication.HTTP_SLIIDO_COM_LEGAL_OPENSOURCE_LISCENSE;
                break;
            default:
                throw new AssertionError();
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            PLog.d(TAG, e.getMessage(), e);
            UiHelpers.showErrorDialog(getContext(), getString(R.string.no_browser_installed));
        }
    }
}
