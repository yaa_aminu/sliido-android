package com.sliido.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.pairapp.util.GenericUtils;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;

import butterknife.Bind;

/**
 * Created by yaaminu on 11/15/17.
 */

public class ImageViewerFragment extends BaseFragment {

    public static final String IMAGE = "image";

    @Bind(R.id.image)
    ImageView imageView;
    @Bind(R.id.progress)
    View progress;

    @Override
    protected int getLayout() {
        return R.layout.fragment_image_viewer;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String imageUrl = getArguments().getString(IMAGE);
        GenericUtils.assertThat(imageUrl != null, "image url is required");
        RequestCreator creator;
        if (new File(imageUrl).exists()) {
            creator = Picasso.with(getContext())
                    .load(new File(imageUrl));
        } else {
            creator = Picasso.with(getContext())
                    .load(imageUrl);
        }

        creator.placeholder(R.drawable.light_color_primary)
                .error(R.drawable.light_color_primary)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        ViewUtils.hideViews(progress);
                        // TODO: 11/15/17 cache this image to the file system
                    }

                    @Override
                    public void onError() {
                        ViewUtils.hideViews(progress);
                        UiHelpers.showToast("Unable to load image");
                    }
                });
    }
}
