package com.sliido.ui.auth;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.data.user.UserManager;
import com.sliido.ui.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerificationFragment extends BaseFragment {


    @Bind(R.id.progress_view)
    View progressView;

    private Callbacks callback;
    private final Listener listener = new Listener() {
        @Override
        public void onEvent(EventBus bus,EventBus.Event event) {
            ViewUtils.hideViews(progressView);
            if (event.error != null) {
                UiHelpers.showErrorDialog(getActivity(), event.error.getUserFriendlyMessage());
            }
        }
    };

    public VerificationFragment() {
        // Required empty public constructor
    }


    @Override
    protected int getLayout() {
        return R.layout.fragment_verification;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (callback == null) throw new IllegalStateException("inject callback");
        Bundle args = getArguments();
        if (args != null) {
            ViewUtils.showByFlag(progressView, args.getBoolean(LoginActivity.LOGIN_ACTIVE, false));
        }
        EventsCenter.getEventBus().register(listener, UserManager.VERIFICATION_EVENT, UserManager.DELETEACCOUNT_EVENT);
    }

    public void setCallbacks(Callbacks callback) {
        this.callback = callback;
    }

    @OnClick({R.id.back_to_login, R.id.complete_setup})
    public void onclick(View view) {
        if (ViewUtils.isViewVisible(progressView)) {
            return;
        }
        if (view.getId() == R.id.back_to_login) {
            ViewUtils.showViews(progressView);
            callback.backToLogin();
        } else if (view.getId() == R.id.complete_setup) {
            ViewUtils.showViews(progressView);
            callback.verifyClickedLink();
        } else {
            throw new AssertionError();
        }
    }

    interface Callbacks {
        void backToLogin();

        void resendActivationLink();

        void verifyClickedLink();
    }
}
