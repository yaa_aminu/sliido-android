package com.sliido.ui.auth;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.pairapp.Errors.SliidoException;
import com.pairapp.util.Config;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.TaskManager;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.SliidoApplication;
import com.sliido.data.user.UserManager;
import com.sliido.ui.BaseFragment;

import butterknife.Bind;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import butterknife.OnTouch;

import static com.sliido.data.user.UserManager.LOGIN_EVENT;
import static com.sliido.data.user.UserManager.isValidEmail;
import static com.sliido.data.user.UserManager.isValidPassword;
import static com.sliido.data.user.UserManager.isValidUserName;

/**
 * author Null-Pointer on 1/15/2016.
 */
public class AuthFragment extends BaseFragment {
    public static final String ACTION_SIGNUP = "signup";
    public static final String ACTION = "action";
    public static final String ACTION_LOGIN = "login";
    private static final String TAG = "AuthFragment";
    @Bind(R.id.et_password)
    EditText etPassword;
    @Bind(R.id.et_email)
    EditText etEmail;
    @Nullable
    @Bind(R.id.et_username)
    EditText etUsername;
    @Bind(R.id.show_password_pane)
    View showPasswordView;
    @Bind(R.id.login_button)
    Button facebookLoginButton;
    @Bind(R.id.cb_show_password)
    CheckBox checkBox;
    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (checkBox != null) {
                checkBox.setChecked(false);
            }
        }
    };
    @Bind(R.id.progress_view)
    View progressView;
    @Nullable
    @Bind(R.id.help_text)
    View help;
    ProgressDialog dialog;
    private int passwordInputType;
    private Callbacks callback;
    private EventBus.EventsListener listener = new Listener() {
        @Override
        public void onEvent(EventBus bus, EventBus.Event event) {
            if (event.error != null) {
                UiHelpers.showErrorDialog(getActivity(), event.error.getUserFriendlyMessage());
            }
            ViewUtils.enable(etPassword, etEmail, etUsername, checkBox);
            ViewUtils.hideViews(progressView);
            ViewUtils.showViews(help);
        }
    };
    private Handler handler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        handler = new Handler();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventsCenter.getEventBus().register(LOGIN_EVENT, listener);
        SharedPreferences preferences = Config.getPreferences(LoginActivity.AUTH_PREFS);
        final String password = preferences.getString("password", ""),
                email = preferences.getString("email", ""),
                name = preferences.getString("name", "");
        restoreFields(email, password, name);
    }

    private void restoreFields(String email, String password, String name) {
        etEmail.setText(email);
        etEmail.setSelection(email.length());
        etPassword.setText(password);
        etPassword.setSelection(password.length());
        if (etUsername != null) {
            etUsername.setText(name);
            etUsername.setSelection(name.length());
        }
    }

    @Override
    public void onPause() {
        final String email = etEmail.getText().toString();
        final String password = etPassword.getText().toString();
        String temp = "";
        if (etUsername != null) {
            temp = etUsername.getText().toString();
        }
        final String username = temp;
        TaskManager.executeNow(new Runnable() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void run() {
                EventsCenter.getEventBus().unregister(LOGIN_EVENT, listener);
                SharedPreferences.Editor editor = Config.getPreferences(LoginActivity.AUTH_PREFS).edit();
                editor.putString("email", email);
                editor.putString("password", password);
                if (etUsername != null) { //don't overwrite existing value if we are not taking username
                    editor.putString("name", username);
                }
                editor.commit();
            }
        }, false);
        super.onPause();
    }

    public void setCallback(Callbacks callback) {
        GenericUtils.ensureNotNull(callback);
        this.callback = callback;
    }

    private boolean isEmailValid(String email) {
        String error = isValidEmail(email);
        if (error != null) {
            etEmail.setError(error);
        }
        return error == null;
    }

    private boolean isPasswordValid(String password) {
        String error = isValidPassword(password);
        if (error != null) {
            etPassword.setError(error);
        }
        return error == null;
    }

    private boolean isValidUsername(String userName) {
        String error = isValidUserName(userName);
        if (error != null) {
            assert etUsername != null;
            etUsername.setError(error);
        }
        return error == null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        passwordInputType = etPassword.getInputType();
        if (callback == null) {
            throw new IllegalStateException("callback not injected, inject the callback through setCallback() method");
        }
        facebookLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onLoginWithFacebook();
            }
        });
    }

    @OnTouch(R.id.cb_show_password)
    public boolean onTouch(View v) {
        return ((SliidoApplication) getActivity().getApplication()).isLoginOrSignupActive.get();
    }

    @OnFocusChange(R.id.et_password)
    public void onFocusChanged(boolean hasFocus) {
        ViewUtils.showByFlag(showPasswordView, hasFocus);
    }

    @SuppressWarnings("NullableProblems")
    @Nullable
    @OnClick({R.id.terms, R.id.tv_signup, R.id.bt_login, R.id.bt_signup, R.id.back_to_login, R.id.help_text})
    public void Onclick(View v) {
        if (v.getId() == R.id.terms) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(SliidoApplication.TERMS_URL));
            getActivity().startActivity(intent);
            return;
        }
        if (ViewUtils.isViewVisible(progressView)) {
            return;
        }
        if (v.getId() == R.id.bt_login) {
            attemptLogin();
        } else if (v.getId() == R.id.bt_signup) {
            attemptSignup();
        } else if (v.getId() == R.id.tv_signup) {
            callback.gotoSignupScreen();
        } else if (v.getId() == R.id.back_to_login) {
            callback.gotoLoginScreen();
        } else if (v.getId() == R.id.help_text) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.reset_password);
            @SuppressLint("InflateParams") View view = LayoutInflater.from(getActivity()).inflate(R.layout.text_field_dialog, null);
            builder.setView(view)
                    .setNegativeButton(R.string.cancel, null);
            final EditText editText = (EditText) view.findViewById(R.id.et_email_reset_password);
            String text = etEmail.getText().toString().trim();
            editText.setText(text);
            editText.setSelection(text.length());
            builder.setPositiveButton(R.string.reset_password, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (i == DialogInterface.BUTTON_POSITIVE) {
                        resetPassword(editText.getText().toString().trim());
                    }
                }
            });
            final AlertDialog dialog = builder.create();
            editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if (i == EditorInfo.IME_NULL || keyEvent != null) {
                        dialog.dismiss();
                        resetPassword(editText.getText().toString());
                        return true;
                    }
                    return false;
                }
            });
            dialog.show();
        } else {
            throw new AssertionError();
        }
    }

    private void resetPassword(final String email) {
        String validEmail = isValidEmail(email);
        if (validEmail == null) {
            if (dialog == null) {
                dialog = new ProgressDialog(getActivity());
                dialog.setMessage(getString(R.string.progress_dialog_message));
                dialog.setCancelable(false);
            }
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog.show();
            UserManager.getInstance().resetPassword(email, new UserManager.Callback() {
                @Override
                public void done(SliidoException e) {
                    dialog.dismiss();
                    if (e != null) {
                        UiHelpers.showDialog(getActivity(), getString(R.string.error), e.getUserFriendlyMessage());
                    } else {
                        UiHelpers.showDialog(getActivity(), getString(R.string.passoword_reset_link_title), getString(R.string.password_reset_link_sent_message, email));
                    }
                }
            });
        } else {
            UiHelpers.showDialog(getActivity(), getString(R.string.error), validEmail);
        }
    }

    @OnCheckedChanged(R.id.cb_show_password)
    public void onCheckChanged(boolean checked) {
        if (checked) {
            etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            etPassword.setInputType(passwordInputType);
        }
        etPassword.setSelection(etPassword.getText().length());

        handler.removeCallbacks(runnable);
        if (checked) {
            handler.postDelayed(runnable, 10000);
        }
    }

    private void attemptLogin() {
        String email = etEmail.getText().toString().trim().toLowerCase();
        String password = etPassword.getText().toString().trim();
        if (isEmailValid(email) && isPasswordValid(password)) {
            ViewUtils.disableAll(etEmail, etPassword, etUsername, checkBox);
            ViewUtils.showViews(progressView);
            ViewUtils.hideViews(help, facebookLoginButton);
            callback.onLogin(email, password);
        }
    }

    @Nullable
    @OnTextChanged(R.id.et_username)
    void onTextChanged(CharSequence text) {
        if (etUsername != null) {
            if (!GenericUtils.isCapitalised(text.toString())) {
                etUsername.setText(GenericUtils.capitalise(text.toString()));
                etUsername.setSelection(text.length());
            }
        }
    }

    private void attemptSignup() {
        String email = etEmail.getText().toString().trim().toLowerCase();
        String password = etPassword.getText().toString().trim();
        assert etUsername != null;
        String username = etUsername.getText().toString().trim();

        if (isValidUsername(username) && isEmailValid(email) && isPasswordValid(password)) {
            ViewUtils.disableAll(etEmail, etPassword, etUsername, checkBox);
            ViewUtils.showViews(progressView);
            ViewUtils.hideViews(help, facebookLoginButton);
            callback.onSignUp(username, email, password);
        }
    }

    @Override
    protected
    @LayoutRes
    int getLayout() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (ACTION_SIGNUP.equals(bundle.getString(ACTION))) {
                return R.layout.activity_sign_up;
            }
        }
        return R.layout.activity_login;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments != null) {
            final boolean isActive = arguments.getBoolean(LoginActivity.LOGIN_ACTIVE, false);
            if (isActive) {
                ViewUtils.showViews(progressView);
                ViewUtils.hideViews(help, facebookLoginButton);
                ViewUtils.disableAll(etEmail, etPassword, etUsername, help, checkBox);
            } else {
                ViewUtils.hideViews(progressView);
                ViewUtils.showViews(help, facebookLoginButton);
                ViewUtils.enable(etEmail, etPassword, etUsername, help, checkBox);
            }
        }
    }

    interface Callbacks {
        void onLogin(String email, String password);

        void onSignUp(String userName, String email, String password);

        void gotoLoginScreen();

        void gotoSignupScreen();

        void onLoginWithFacebook();
    }

}
