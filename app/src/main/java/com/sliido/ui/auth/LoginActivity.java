package com.sliido.ui.auth;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentTransaction;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.pairapp.Errors.SliidoException;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.PLog;
import com.pairapp.util.UiHelpers;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.sliido.R;
import com.sliido.SliidoApplication;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;
import com.sliido.ui.MainActivity;
import com.sliido.ui.SliidoBaseActivity;

import java.util.Arrays;

import static com.pairapp.util.EventBus.Event;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends SliidoBaseActivity implements LogInCallback {

    public static final String TAG = LoginActivity.class.getSimpleName();
    public static final String LOGIN_ACTIVE = "isloginactive";
    public static final int STARTING = -1, LOGGIN_IN = 0, SIGNING_UP = 1, VERIFYING = 3, COMPLETE = 4;
    static final String AUTH_PREFS = "auth prefs";
    private static final String LOGIN_STAGE = "com.loginactivity.islogginin";
    private static final String AUTH_FRAGMENT = "authFragment";
    private static final String SIGNUP_AUTH_FRAGMENT = "signupAuthFragment";
    private static final String LOGIN_AUTH_FRAMENT = "loginAuthFrament";
    private int stage;
    private CallbacksImpl callback = new CallbacksImpl();
    private boolean stateSaved;
    private UserManager userManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);
        userManager = UserManager.getInstance();
    }

    @Override
    public void done(ParseUser user, ParseException e) {
        if (e == null) {
            completeRegistration(user);
        } else {
            PLog.e(TAG, e.getMessage(), e);
            UiHelpers.showErrorDialog(LoginActivity.this, e.getMessage());
        }
    }

    private void completeRegistration(ParseUser user) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        UserManager.getInstance().completeFacebookRegistration(user, new UserManager.Callback() {
            @Override
            public void done(SliidoException e) {
                progressDialog.dismiss();
                if (e != null) {
                    UiHelpers.showErrorDialog(LoginActivity.this, e.getUserFriendlyMessage());
                } else {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    private void setUpAuthFragment(String action) {
        String tag = action != null && action.equals(AuthFragment.ACTION_SIGNUP) ? SIGNUP_AUTH_FRAGMENT : LOGIN_AUTH_FRAMENT;
        AuthFragment f = ((AuthFragment) getFManager().findFragmentByTag(tag));
        if (f == null) {
            f = new AuthFragment();
        }
        if (f.isAdded()) {
            return;
        }
        f.setCallback(callback);
        Bundle bundle = new Bundle();
        bundle.putBoolean(LOGIN_ACTIVE, ((SliidoApplication) getApplication()).isLoginOrSignupActive.get());
        bundle.putString(AuthFragment.ACTION, action == null ? AuthFragment.ACTION_LOGIN : action);
        f.setArguments(bundle);
        getFManager().beginTransaction()
                .replace(R.id.container, f, tag)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        stateSaved = false;
        stage = getPreferences(Context.MODE_PRIVATE).getInt(LOGIN_STAGE, STARTING);
        //ensure the stage matches with reality!
        if (userManager.isCurrentUserVerified()) {
            stage = COMPLETE;
        } else {
            User currrentUser = userManager.getCurrentUser();
            if (currrentUser != null) {
                stage = VERIFYING;
            } else if (stage != LOGGIN_IN && stage != SIGNING_UP) {
                stage = STARTING;
            }
        }
        next(stage);
    }

    private void next(int stage) {
        if (stateSaved) {
            PLog.d(TAG, "not executing any fragment transaction because onsaveinstantstate() has been called");
            return;
        }
        switch (stage) {
            case VERIFYING:
                VerificationFragment fragment = new VerificationFragment();
                fragment.setCallbacks(callback);
                Bundle bundle = new Bundle();
                bundle.putBoolean(LOGIN_ACTIVE, ((SliidoApplication) getApplication()).isLoginOrSignupActive.get());
                fragment.setArguments(bundle);
                getFManager().beginTransaction().replace(R.id.container, fragment).commit();
                EventsCenter.getEventBus().register(this, UserManager.DELETEACCOUNT_EVENT, UserManager.VERIFICATION_EVENT);
                break;
            case STARTING:
                this.stage = LOGGIN_IN;
            case LOGGIN_IN:
                setUpAuthFragment(AuthFragment.ACTION_LOGIN);
                EventsCenter.getEventBus().register(UserManager.LOGIN_EVENT, this);
                break;
            case SIGNING_UP:
                setUpAuthFragment(AuthFragment.ACTION_SIGNUP);
                EventsCenter.getEventBus().register(UserManager.LOGIN_EVENT, this);
                break;
            case COMPLETE:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                getPreferences(MODE_PRIVATE).edit().remove(LOGIN_STAGE).apply();
                finish();
                break;
            default:
                throw new AssertionError();

        }
    }

    @Override
    protected void onPause() {
        EventsCenter.getEventBus().unregister(UserManager.LOGIN_EVENT, this);
        saveStage();
        super.onPause();
    }

    private void saveStage() {
        getPreferences(Context.MODE_PRIVATE).edit()
                .putInt(LOGIN_STAGE, stage).apply();
    }

    @Override
    public void onEvent(EventBus bus, Event event) {
        final EventBus eventBus = EventsCenter.getEventBus();
        ((SliidoApplication) getApplication()).isLoginOrSignupActive.set(false);
        eventBus.removeStickyEvent(event);
        if (event.error != null) {
            // TODO: 1/17/2016 inspect error and take appropriate action if possible
            //let the fragments handle how to show the error
        } else {
            if (event.tag.equals(UserManager.DELETEACCOUNT_EVENT)) {
                stage = LOGGIN_IN;
                Answers.getInstance().logCustom(new CustomEvent("verification").putCustomAttribute("success", "" + Boolean.FALSE));
            } else if (event.tag.equals(UserManager.LOGIN_EVENT)) {
                if (stage == LOGGIN_IN && userManager.isCurrentUserVerified()) {
                    stage = COMPLETE;
                    Answers.getInstance().logLogin(new LoginEvent().putMethod("emailPassword").putSuccess(true));
                } else {
                    stage = VERIFYING;
                    Answers.getInstance().logSignUp(new SignUpEvent().putMethod("emailPassword").putSuccess(true));
                }
            } else if (event.tag.equals(UserManager.VERIFICATION_EVENT)) {
                stage = COMPLETE;
                Answers.getInstance().logCustom(new CustomEvent("verification").putCustomAttribute("success", "" + Boolean.TRUE));
            }
            next(stage);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        stateSaved = true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        stateSaved = true;
    }

    private final class CallbacksImpl implements AuthFragment.Callbacks, VerificationFragment.Callbacks {

        @Override
        public void backToLogin() {
            ((SliidoApplication) getApplication()).isLoginOrSignupActive.set(true);
            userManager.reset();
        }

        @Override
        public void verifyClickedLink() {
            ((SliidoApplication) getApplication()).isLoginOrSignupActive.set(true);
            userManager.verifyCurrentUser();
        }

        @Override
        public void resendActivationLink() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void onLogin(String email, String password) {
            ((SliidoApplication) getApplication()).isLoginOrSignupActive.set(true);
            Intent intent = new Intent(LoginActivity.this, Worker.class);
            intent.setAction(Worker.ACTION_LOGIN);
            intent.putExtra(Worker.EXTRA_EMAIL, email);
            intent.putExtra(Worker.EXTRA_PASSWORD, password);
            startService(intent);
        }

        @Override
        public void onSignUp(String userName, String email, String password) {
            ((SliidoApplication) getApplication()).isLoginOrSignupActive.set(true);
            Intent intent = new Intent(LoginActivity.this, Worker.class);
            intent.setAction(Worker.ACTION_SIGNUP);
            intent.putExtra(Worker.EXTRA_USERNAME, userName);
            intent.putExtra(Worker.EXTRA_EMAIL, email);
            intent.putExtra(Worker.EXTRA_PASSWORD, password);
            startService(intent);
        }

        @Override
        public void gotoLoginScreen() {
            stage = LOGGIN_IN;
            setUpAuthFragment(AuthFragment.ACTION_LOGIN);
        }

        @Override
        public void gotoSignupScreen() {
            stage = SIGNING_UP;
            setUpAuthFragment(AuthFragment.ACTION_SIGNUP);
        }

        @Override
        public void onLoginWithFacebook() {
            ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginActivity.this, Arrays.asList("public_profile", "email"),
                    LoginActivity.this);
        }

    }
}
