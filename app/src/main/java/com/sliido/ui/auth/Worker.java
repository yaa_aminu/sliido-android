package com.sliido.ui.auth;

import android.app.IntentService;
import android.content.Intent;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.sliido.data.user.UserManager;

/**
 * @author aminu on 9/22/2016.
 */
public class Worker extends IntentService {
    public static final String ACTION_LOGIN = "login", ACTION_SIGNUP = "signup";
    public static final String EXTRA_PASSWORD = "password";
    public static final String EXTRA_EMAIL = "email";
    public static final String EXTRA_USERNAME = "username";

    public Worker() {
        super("worker");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        String email = intent.getStringExtra(EXTRA_EMAIL), password = intent.getStringExtra(EXTRA_PASSWORD),
                username = intent.getStringExtra(EXTRA_USERNAME);
        switch (action) {
            case ACTION_LOGIN:
                Answers.getInstance().logCustom(new CustomEvent("attemptLogin"));
                UserManager.getInstance().login(email, password);
                break;
            case ACTION_SIGNUP:
                Answers.getInstance().logCustom(new CustomEvent("attemptSignUp"));
                UserManager.getInstance().signUp(username, email, password);
                break;
        }

    }
}
