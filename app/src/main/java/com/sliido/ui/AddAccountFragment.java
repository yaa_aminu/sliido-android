package com.sliido.ui;

import com.sliido.R;

import butterknife.OnClick;

/**
 * Created by yaaminu on 10/21/17.
 */

public class AddAccountFragment extends BaseFragment {
    @Override
    protected int getLayout() {
        return R.layout.fragment_add_account;
    }

    @OnClick(R.id.bt_login)
    void login() {
        MainActivity.goToLogin(getContext());
    }

}
