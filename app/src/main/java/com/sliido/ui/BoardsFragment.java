package com.sliido.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.sliido.R;
import com.sliido.data.user.UserManager;
import com.sliido.ui.channel.MyBoardsFragment;
import com.sliido.ui.channel.SubChannelsFragment;
import com.sliido.ui.channel.UnSubChannelsFragment;

import butterknife.Bind;

/**
 * Created by yaaminu on 10/21/17.
 */

public class BoardsFragment extends BaseFragment {
    @Override
    protected int getLayout() {
        return R.layout.fragment_boards;
    }

    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.tab_layout)
    TabLayout tabLayout;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final boolean currentUserVerified = UserManager.getInstance().isCurrentUserVerified();
        BaseFragment[] fragments = {
                new UnSubChannelsFragment(),
                currentUserVerified ? new SubChannelsFragment() : new AddAccountFragment(),
                currentUserVerified ?
                        new MyBoardsFragment() : new AddAccountFragment()
        };

        BoardsPagerAdapter pagerAdapter = new BoardsPagerAdapter(getChildFragmentManager(),
                fragments, getResources().getStringArray(R.array.channels_pager_titles));
        pager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(pager);
    }


    static class BoardsPagerAdapter extends FragmentPagerAdapter {

        private final BaseFragment[] fragments;
        private final String[] titles;

        BoardsPagerAdapter(FragmentManager fm, BaseFragment[] fragments, String[] titles) {
            super(fm);
            this.fragments = fragments;
            this.titles = titles;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }
}
