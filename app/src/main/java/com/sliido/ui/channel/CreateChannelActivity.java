package com.sliido.ui.channel;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.ui.SliidoBaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateChannelActivity extends SliidoBaseActivity {

    @Bind(R.id.et_channel_name)
    EditText etChannelName;

    @Bind(R.id.bt_create_channel)
    View buttonCreateChannel;

    @Bind(R.id.progress_view)
    View progressView;

    @Bind(R.id.cb_public)
    CheckBox isPublic;

    @Bind(R.id.iv_channel_icon)
    ImageView channelIcon;

    @OnClick({R.id.bt_create_channel, R.id.back})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.bt_create_channel:
                String name = etChannelName.getText().toString().trim();
                String error = ChannelManager.isValidChannelName(name);
                if (error != null) {
                    etChannelName.setError(error);
                } else {
                    view.setEnabled(false);
                    etChannelName.setEnabled(false);
                    isPublic.setEnabled(false);
                    channelIcon.setClickable(false);
                    ViewUtils.showViews(progressView);
                    ChannelManager.CreateChannel(name, null, isPublic.isChecked());
                }
                break;
            case R.id.back:
                finish();
                break;
            default:
                throw new AssertionError();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_channel);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventsCenter.getEventBus().register(ChannelManager.EVENT_CREATE_CHANNEL, this);
    }

    @Override
    protected void onPause() {
        EventsCenter.getEventBus().unregister(ChannelManager.EVENT_CREATE_CHANNEL, this);
        super.onPause();
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        if (event.tag.equals(ChannelManager.EVENT_CREATE_CHANNEL)) {
            buttonCreateChannel.setEnabled(true);
            etChannelName.setEnabled(true);
            isPublic.setEnabled(true);
            channelIcon.setClickable(false);
            ViewUtils.hideViews(progressView);
            //handle event
            yourBus.removeStickyEvent(event);
            if (event.error != null) {
                UiHelpers.showDialog(this, getString(R.string.error), event.error.getUserFriendlyMessage());
            } else {
                //channel created, go back to main activity
                ChannelDetailActivity.viewChannel(this, ((Channel) event.data).getChannelId(), ((Channel) event.data).getName(), CreateChannelActivity.class.getSimpleName());
                finish();
            }
        }
    }
}
