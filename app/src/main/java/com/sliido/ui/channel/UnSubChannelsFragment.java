package com.sliido.ui.channel;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.Errors.SliidoException;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.user.UserManager;
import com.sliido.ui.posts.NewsFeedActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Sort;

/**
 * by null-pointer on 2/25/2016.
 */
public class UnSubChannelsFragment extends BaseChannelFragment {

    static final String TAG = UnSubChannelsFragment.class.getSimpleName();

    @Bind(R.id.empty_view)
    View emptyView;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Bind(R.id.empty_view_desc)
    TextView emptyViewText;

    private SliidoBaseAdapter<Channel> adapter;
    private List<Channel> channels;
    UnsubChannelAdapter.Delegate delegate = new UnsubChannelAdapter.Delegate() {

        @Override
        public void onLoadMoreItems(int position) {
            if (!loading) {
                loading = true;
                adapter.notifyDataChanged();
                ChannelManager.findLowSubscribedChannels(false);
            }
        }

        @Override
        public void onSubscribe(final Channel channel) {
            if (channel.isSubscribing()) {
                return;
            }
            UserManager.Callback callback = new UserManager.Callback() {
                @Override
                public void done(SliidoException e) {
                    if (getActivity() != null && isAdded()) {
                        adapter.notifyDataChanged();
                        if (e != null) {
                            UiHelpers.showDialog(getActivity(), getString(R.string.error), e.getUserFriendlyMessage());
                        } else {
                            UiHelpers.showToast(R.string.subscribe_success);
                            adapter.notifyDataChanged();
                            NewsFeedActivity.viewPostsFor(getContext(), channel);
                        }
                    }
                }
            };
            ChannelManager.subscribeToChannel(channel.getChannelId(), callback);
            adapter.notifyDataChanged();
        }

        @Override
        public boolean isSubscribing(Channel channel) {
            return ChannelManager.isSubScribing(channel.getChannelId());
        }

        @Override
        public Context context() {
            return getActivity();
        }

        @Override
        public void onItemClick(SliidoBaseAdapter<Channel> adapter, View view, int position, long id) {
            NewsFeedActivity.viewPostsFor(getContext(), adapter.getItem(position));
        }

        @Override
        public boolean onItemLongClick(SliidoBaseAdapter<Channel> adapter, View view, int position, long id) {
            Answers.getInstance().logCustom(new CustomEvent(UnSubChannelsFragment.class.getSimpleName() + ":vainLongClickAttempt"));
            return false;
        }

        @SuppressWarnings("ConstantConditions")
        @NonNull
        @Override
        public List<Channel> dataSet() {
            ViewUtils.showByFlag(channels.isEmpty(), emptyView);
            ViewUtils.showByFlag(!channels.isEmpty(), recyclerView);
            if (channels.isEmpty()) {
                ViewUtils.showByFlag(ButterKnife.findById(emptyView, R.id.no_feed_empty_view), !loading);
                ViewUtils.showByFlag(ButterKnife.findById(emptyView, R.id.loading_empty_view_root), loading);
            }
            return channels;
        }

        @Override
        public boolean showLoadingMore() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                return recyclerView.canScrollVertically(1) && loading;
            } else {
                return channels.size() > 10 && loading;
            }
        }
    };

    static Fragment make() {
        return new UnSubChannelsFragment();
    }

    @NonNull
    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_channels;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (currentUserEmail == null) {
            channels = channelRealm.where(Channel.class)
                    .findAllSorted(Channel.FIELD_SUBSCRIBERS, Sort.DESCENDING,
                            Channel.FIELD_NAME, Sort.ASCENDING);
        } else {
            channels = channelRealm.where(Channel.class).equalTo(Channel.FIELD_SUBSCRIBED, false)
                    .notEqualTo(Channel.FIELD_CREATED_BY, currentUserEmail)
                    .findAllSorted(Channel.FIELD_SUBSCRIBERS, Sort.DESCENDING,
                            Channel.FIELD_NAME, Sort.ASCENDING);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        emptyViewText.setText(R.string.no_channel_text);
        adapter = new UnsubChannelAdapter(delegate);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), getResources().getInteger(R.integer.column_count_channel_grid), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        ViewUtils.hideViews(ButterKnife.findById(getView(), R.id.fab));

    }

    @NonNull
    @Override
    public SliidoBaseAdapter<Channel> getAdapter() {
        return adapter;
    }

    @Override
    protected boolean isSubscribedFragments() {
        return false;
    }
}

