package com.sliido.ui.channel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.channel.Channel;
import com.sliido.data.user.UserManager;

import butterknife.Bind;

import static com.pairapp.util.GenericUtils.getString;

/**
 * Created by null-pointer on 2/25/2016.
 */
class SubChannelsAdapter extends SliidoBaseAdapter<Channel> {

    @Nullable
    private String currentUserEmail = null;

    SubChannelsAdapter(Delegate delegate) {
        super(delegate);
        if (UserManager.getInstance().isCurrentUserVerified()) {
            //noinspection ConstantConditions
            currentUserEmail = UserManager.getInstance().getCurrentUser().getEmail();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void doBindHolder(Holder holder, int position) {
        final Channel ch = getItem(position);
        ((ViewHolder) holder).tvChannelName.setText(ch.getName());
        ((ViewHolder) holder).tvSubscribers.setText(" " + getString(R.string.subscribers, ch.getSubscribers()));
        ((ViewHolder) holder).subscribeOrUnsubscribe.setText(ch.isSubscribed() || ch.isSubscribing() ||
                (currentUserEmail != null && currentUserEmail.equals(ch.getCreatedBy())) ? R.string.following : R.string.subscribe);
        ((ViewHolder) holder).subscribeOrUnsubscribe.setCompoundDrawablesWithIntrinsicBounds(ch.isSubscribed() ||
                (currentUserEmail != null && currentUserEmail.equals(ch.getCreatedBy()))
                ? R.drawable.ic_notifications_active_color_primary_24dp : R.drawable.ic_notifications_black_18dp, 0, 0, 0);
        if (ch.getNewPostCount() > 0) {
            ((ViewHolder) holder).newPostCount.setText(String.valueOf(ch.getNewPostCount()));
            ViewUtils.showViews(((ViewHolder) holder).newPostCount);
        } else {
            ViewUtils.hideViews(((ViewHolder) holder).newPostCount);
        }
    }

    @Override
    public Holder doCreateHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_channel_list_item, parent, false));
    }

    interface Delegate extends SliidoBaseAdapter.Delegate<Channel> {
        Context context();
    }


    static class ViewHolder extends Holder {


        @Bind(R.id.tv_channel_name)
        TextView tvChannelName;

        @Bind(R.id.tv_subscribers_count)
        TextView tvSubscribers;

        @Bind(R.id.tv_subscribe)
        TextView subscribeOrUnsubscribe;
        @Bind(R.id.tv_new_post_count)
        TextView newPostCount;

        ViewHolder(View v) {
            super(v);
        }

    }

}
