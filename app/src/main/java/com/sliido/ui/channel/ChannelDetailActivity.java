package com.sliido.ui.channel;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.Errors.SliidoException;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.UiHelpers;
import com.sliido.R;
import com.sliido.SliidoApplication;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;
import com.sliido.ui.MainActivity;
import com.sliido.ui.SliidoBaseActivity;
import com.sliido.ui.posts.NewsFeedActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;

public class ChannelDetailActivity extends SliidoBaseActivity implements ChannelInfoProvider {

    public static final String EXTRA_CHANNEL_ID = "channelId";
    public static final String TAG = ChannelDetailActivity.class.getSimpleName();
    public static final String EXTRA_CHANNEL_NAME = "channelName";
    public static final String EVENT_REFRESH_DISPLAY = "refreshDisplay";
    public static final String EXTRA_SOURCE = "source";
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.tabstrip)
    TabLayout tabLayout;
    @Nullable
    ProgressDialog dialog;
    private Realm realm;
    private String inChannelID;
    private String inChannelName;
    private Channel inChannel;
    private final RealmChangeListener<Realm> changeListener = new RealmChangeListener<Realm>() {
        @Override
        public void onChange(Realm element) {
            refreshDisplay();
        }
    };
    private final UserManager.Callback callback = new UserManager.Callback() {
        @Override
        public void done(SliidoException e) {
            if (dialog != null) {
                dialog.dismiss();
            }
            if (!realm.isClosed()) {
                //activity not destroyed!!!
                if (e != null) {
                    UiHelpers.showErrorDialog(ChannelDetailActivity.this, e.getUserFriendlyMessage());
                } else {
                    //success!!!
                    UiHelpers.showToast((inChannel != null && inChannel.isSubscribed()) ? R.string.un_subscribe_success : R.string.subscribe_success);
                    NewsFeedActivity.viewPostsFor(ChannelDetailActivity.this, inChannel);
                }
                refreshDisplay();
            }
        }
    };
    private User currentUser;
    private int channelLoadingState = CHANNEL_LOADING;

    public static void viewChannel(Context context, String channelId, String channelName, String source) {
        Intent intent = new Intent(context, ChannelDetailActivity.class);
        intent.putExtra(EXTRA_CHANNEL_ID, channelId);
        intent.putExtra(EXTRA_CHANNEL_NAME, channelName);
        intent.putExtra(EXTRA_SOURCE, source);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserManager userManager = UserManager.getInstance();
        realm = Channel.REALM();
        if (userManager.isCurrentUserVerified()) {
            setContentView(R.layout.activity_channel_detail);
            ButterKnife.bind(this);
            currentUser = UserManager.getInstance().getCurrentUser();
            Intent intent = getIntent();
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                inChannelID = intent.getData().getQueryParameter("id");
                if (GenericUtils.isEmpty(inChannelID)) {
                    Answers.getInstance().logCustom(new CustomEvent("viewChannel").putCustomAttribute("deepLink", "true").
                            putCustomAttribute("linkValid", "false"));
                    finish();
                    return;
                }
                Answers.getInstance().logCustom(new CustomEvent("viewChannel").putCustomAttribute("deeplink", "true").putCustomAttribute("linkValid", "true"));
                inChannelName = "";
            } else {
                Answers.getInstance().logCustom(new CustomEvent("viewChannel").putCustomAttribute("deeplink", "false")
                        .putCustomAttribute("linkValid", "true").putCustomAttribute("from", intent.getStringExtra(EXTRA_SOURCE)));
                inChannelID = intent.getStringExtra(EXTRA_CHANNEL_ID);
                inChannelName = intent.getStringExtra(EXTRA_CHANNEL_NAME);
            }

            EventsCenter.getEventBus().register(this, ChannelManager.EVENT_CHANNEL_REFRESHED);
            ChannelManager.refreshChannel(inChannelID);
            pager.setAdapter(new MyFragmentStatePagerAdapter(getSupportFragmentManager()));
            tabLayout.setupWithViewPager(pager);
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                dialog = new ProgressDialog(this);
                dialog.setMessage(getString(R.string.subcription_in_progress_v2));
                dialog.setCancelable(false);
                dialog.show();
                ChannelManager.subscribeToChannel(inChannelID, callback);
            }
            refreshDisplay();
        } else {
            MainActivity.goToLogin(this);
            finish();
        }
    }

    void refreshDisplay() {
        inChannel = realm.where(Channel.class).equalTo(Channel.FIELD_CHANNEL_ID,
                inChannelID).findFirst();
        EventsCenter.getEventBus().post(new EventBus.Event(EVENT_REFRESH_DISPLAY, null, inChannel));
    }

    @Override
    protected void onDestroy() {
        EventsCenter.getEventBus().unregister(ChannelManager.EVENT_CHANNEL_REFRESHED, this);
        realm.close();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        realm.addChangeListener(changeListener);
    }

    @Override
    protected void onPause() {
        realm.removeChangeListener(changeListener);
        super.onPause();
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        try {
            if (ChannelManager.EVENT_CHANNEL_REFRESHED.equals(event.tag)) {
                handleChannelRefreshedEvent(event);
            } else {
                PLog.d(TAG, "received unknown event with tag: " + event.tag);
            }
        } finally {
            yourBus.removeStickyEvent(event);
        }
    }

    private void handleChannelRefreshedEvent(EventBus.Event event) {
        if (inChannelID.equals(event.data)) {
            if (event.error != null) {
                channelLoadingState = CHANNEL_LOADED_FAILURE;
                UiHelpers.showToast(event.error.getUserFriendlyMessage(), Toast.LENGTH_LONG);
            } else {
                channelLoadingState = CHANNEL_LOADED_SUCCESS;
            }
            refreshDisplay();
        } else {
            PLog.d(TAG, "unexpected channel id: %s, we are expecting %s ", event.data, inChannelID);
        }
    }

    @Override
    public String getChannelName() {
        return inChannel != null ? inChannel.getName() : inChannelName;
    }

    @Override
    public int getChannelSubscribersCount() {
        return inChannel == null ? -1 : inChannel.getSubscribers();
    }

    @Override
    public int channelLoaded() {
        return channelLoadingState;
    }

    @Override
    public void onShareInviteLink() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,
                getString(R.string.invitation_message, inChannel.getName(), "\n\n" + SliidoApplication.BASE_URL + "invites/ch?id=" + inChannel.getChannelId()));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException ignored) {
            UiHelpers.showErrorDialog(this, getString(R.string.no_app_for_sharing_stuffs));
        }
    }

    @Override
    public int getSubscriptionState() {
        if (channelLoaded() != CHANNEL_LOADED_SUCCESS) return SUSBCRIPTION_UNKNOWN;

        if (inChannel.getCreatedBy().equals(currentUser.getEmail()))
            return PERSONAL_CHANNEL;

        //always check for isUnsubscribing or isSubscribing before checking channel#isSubscribed
        if (ChannelManager.isUnsubScribing(inChannelID)) {
            return UNSUBSCRIBING;
        }
        if (ChannelManager.isSubScribing(inChannelID)) {
            return SUBSCRIBING;
        }
        return inChannel.isSubscribed() ? SUBSCRIBED : UNSUBSCRIBED;
    }

    @Override
    public void onSubscribeOrUnSubscribe() {
        if (inChannel != null) {
            if (inChannel.isSubscribed()) {
                ChannelManager.unSubscribeToChannel(inChannelID, callback);
            } else {
                //subscribe
                ChannelManager.subscribeToChannel(inChannelID, callback);
            }
            refreshDisplay();
        }
    }

    @Override
    public Channel getChannel() {
        return inChannel;
    }

    private static class MyFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

        MyFragmentStatePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ChannelBasicInfoFragment();
                default:
                    return new ChannelSettingsFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return GenericUtils.getString(position == 0 ? R.string.about_channel : R.string.channel_settings);
        }
    }

}
