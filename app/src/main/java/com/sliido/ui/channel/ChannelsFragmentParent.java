package com.sliido.ui.channel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.sliido.R;
import com.sliido.ui.BaseFragment;

import butterknife.Bind;

public class ChannelsFragmentParent extends BaseFragment {
    @Bind(R.id.tabstrip)
    TabLayout layout;

    @Bind(R.id.pager)
    ViewPager pager;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        layout.addTab(layout.newTab(), 0, true);
        layout.addTab(layout.newTab(), 1, false);
        pager.setAdapter(new MyPagerAdapter());
        layout.setupWithViewPager(pager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_new_channel) {
            Intent intent = new Intent(getActivity(), CreateChannelActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.channels_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_channels;
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        private final Fragment[] fragments;

        public MyPagerAdapter() {
            super(getFragmentManager());
            this.fragments = new Fragment[2];
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = fragments[position];
            if (fragment == null) {
                fragment = position == 0 ? SubChannelsFragment.make() : UnSubChannelsFragment.make();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return position == 0 ? getString(R.string.subscribed) : getString(R.string.find_channels);
        }
    }


}
