package com.sliido.ui.channel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.data.channel.Channel;
import com.sliido.ui.BaseFragment;
import com.sliido.ui.posts.NewPostActivity;
import com.sliido.ui.view.ProgressWheel;

import butterknife.Bind;
import butterknife.OnClick;

import static com.sliido.ui.channel.ChannelInfoProvider.CHANNEL_LOADED_SUCCESS;
import static com.sliido.ui.channel.ChannelInfoProvider.CHANNEL_LOADING;
import static com.sliido.ui.channel.ChannelInfoProvider.PERSONAL_CHANNEL;
import static com.sliido.ui.channel.ChannelInfoProvider.SUBSCRIBING;
import static com.sliido.ui.channel.ChannelInfoProvider.UNSUBSCRIBED;
import static com.sliido.ui.channel.ChannelInfoProvider.UNSUBSCRIBING;

/**
 * Created by aminu on 10/9/2016.
 */

public class ChannelHeaderFragment extends BaseFragment {

    @SuppressWarnings("NullableProblems")
    @NonNull
    private ChannelInfoProvider channelInfoProvider;

    @Bind(R.id.tv_channel_name)
    TextView channelName;

    @Bind(R.id.tv_un_subscribe)
    TextView subScriberUnsubScribe;

    @Bind(R.id.progress_view)
    ProgressWheel progressWheel;

    @Bind(R.id.new_post)
    View newPost;

    @Bind(R.id.card_subscribers)
    View subscribersPanel;

    @Bind(R.id.subscribing_progress)
    View subscriptionProgress;

    @Bind(R.id.tv_subscribers_count)
    TextView subscribersCount;
    private Listener listener = new Listener() {
        @Override
        public void onEvent(EventBus bus, EventBus.Event event) {
            if (event.tag.equals(ChannelDetailActivity.EVENT_REFRESH_DISPLAY)) {
                if (event.error == null) {
                    refreshDisplay();
                }
            }
        }
    };

    @Override
    protected int getLayout() {
        return com.sliido.R.layout.channel_header_layout;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ChannelInfoProvider) {
            this.channelInfoProvider = (ChannelInfoProvider) context;
        } else {
            throw new ClassCastException("parent activity must implement " + ChannelInfoProvider.class.getName());
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        refreshDisplay();
        EventsCenter.getEventBus().register(listener, ChannelDetailActivity.EVENT_REFRESH_DISPLAY);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventsCenter.getEventBus().unregister(ChannelDetailActivity.EVENT_REFRESH_DISPLAY, listener);
    }

    @SuppressLint("SetTextI18n")
    void refreshDisplay() {
        int subscriptionState = channelInfoProvider.getSubscriptionState();
        channelName.setText(channelInfoProvider.getChannelName());
        ViewUtils.showByFlag(channelInfoProvider.channelLoaded() == CHANNEL_LOADING, progressWheel);
        ViewUtils.showByFlag(subscriptionState == PERSONAL_CHANNEL, newPost);
        ViewUtils.showByFlag(channelInfoProvider.getChannel() != null, subscribersPanel);
        ViewUtils.showByFlag(subscriptionState == SUBSCRIBING || subscriptionState == UNSUBSCRIBING
                , subscriptionProgress);
        ViewUtils.showByFlag(subscriptionState == UNSUBSCRIBED, subScriberUnsubScribe);

        if (channelInfoProvider.getChannel() != null) {
            subscribersCount.setText(" " + getString(R.string.subscribers, channelInfoProvider.getChannelSubscribersCount()));
        }
    }


    @OnClick(R.id.tv_un_subscribe)
    void click() {
        if (channelInfoProvider.channelLoaded() == CHANNEL_LOADED_SUCCESS) {
            channelInfoProvider.onSubscribeOrUnSubscribe();
        }
    }

    @OnClick(R.id.new_post)
    void clike2() {
        if (channelInfoProvider.channelLoaded() == CHANNEL_LOADED_SUCCESS) {
            Answers.getInstance().logCustom(new CustomEvent(ChannelHeaderFragment.class.getSimpleName() + ":newPost"));
            Channel channel = channelInfoProvider.getChannel();
            NewPostActivity.createNewPost(getActivity(),
                    channel.getChannelId(),
                    channel.getName());
        }
    }

    @OnClick(R.id.back)
    void goBack() {
        getActivity().finish();
    }

}
