package com.sliido.ui.channel;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;
import com.sliido.ui.posts.NewsFeedActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Sort;

/**
 * by null-pointer on 2/25/2016.
 */
public class SubChannelsFragment extends BaseChannelFragment {

    public static final String TAG = SubChannelsFragment.class.getSimpleName();
    private final User currentUser = UserManager.getInstance().getCurrentUser();

    @Bind(R.id.empty_view)
    View emptyView;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    private SliidoBaseAdapter<Channel> adapter;
    private List<Channel> channels;
    SubChannelsAdapter.Delegate delegate = new SubChannelsAdapter.Delegate() {

        @Override
        public Context context() {
            return getActivity();
        }


        @Override
        public void onLoadMoreItems(int position) {
            if (!loading) {
                loading = true;
                adapter.notifyDataChanged();
                ChannelManager.findLowSubscribedChannels(true);
            }
        }

        @Override
        public void onItemClick(SliidoBaseAdapter<Channel> adapter, View view, int position, long id) {
            Channel channel = adapter.getItem(position);
            assert channel != null;
            if (channel.getNewPostCount() > 0) {
                ChannelManager.clearNewPostCountAsync(channel.getChannelId(), channelRealm);
            }
            NewsFeedActivity.viewPostsFor(getContext(), channel);
        }

        @Override
        public boolean onItemLongClick(SliidoBaseAdapter<Channel> adapter, View view, int position, long id) {
            Answers.getInstance().logCustom(new CustomEvent(SubChannelsFragment.class.getSimpleName() + ":vainLongClickAttempt"));
            return false;
        }

        @SuppressWarnings("ConstantConditions")
        @NonNull
        @Override
        public List<Channel> dataSet() {
            ViewUtils.showByFlag(channels.isEmpty(), emptyView);
            ViewUtils.showByFlag(!channels.isEmpty(), recyclerView);
            if (channels.isEmpty()) {
                ViewUtils.showByFlag(ButterKnife.findById(emptyView, R.id.no_feed_empty_view), !loading);
                ViewUtils.showByFlag(ButterKnife.findById(emptyView, R.id.loading_empty_view_root), loading);
            }
            return channels;
        }

        @Override
        public boolean showLoadingMore() {
            return recyclerView.canScrollVertically(1) && loading;
        }
    };

    static Fragment make() {
        return new SubChannelsFragment();
    }

    @NonNull
    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @NonNull
    @Override
    public SliidoBaseAdapter<Channel> getAdapter() {
        return adapter;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_channels;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //noinspection ConstantConditions
        channels = channelRealm.where(Channel.class).equalTo(Channel.FIELD_SUBSCRIBED, true)
                .findAllSorted(Channel.FIELD_NEW_POST_COUNT, Sort.DESCENDING, Channel.FIELD_UPDATED_AT, Sort.DESCENDING);
        ChannelManager.findNewFeeds();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new SubChannelsAdapter(delegate);
        final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), getResources().getInteger(R.integer.column_count_channel_grid), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected boolean isSubscribedFragments() {
        return true;
    }
}

