package com.sliido.ui.channel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sliido.R;
import com.sliido.data.channel.Channel;

import butterknife.OnClick;

/**
 * Created by null-pointer on 2/25/2016.
 */
public class UnsubChannelAdapter extends SubChannelsAdapter {

    private final Delegate delegate;

    UnsubChannelAdapter(Delegate delegate) {
        super(delegate);
        this.delegate = delegate;
    }

    @Override
    public Holder doCreateHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_channel_list_item, parent, false));
        holder.itemView.setTag(this);
        return holder;
    }

    public interface Delegate extends SubChannelsAdapter.Delegate {
        void onSubscribe(Channel channel);

        boolean isSubscribing(Channel channel);

        Context context();
    }

    public static class ViewHolder extends SubChannelsAdapter.ViewHolder {

        ViewHolder(View v) {
            super(v);
        }

        @OnClick(R.id.tv_subscribe)
        void subscribe() {
            final UnsubChannelAdapter unsubChannelAdapter = ((UnsubChannelAdapter) itemView.getTag());
            if (unsubChannelAdapter != null) {
                unsubChannelAdapter.delegate.onSubscribe(unsubChannelAdapter.getItem(getAdapterPosition()));
            }
        }
    }

}
