package com.sliido.ui.channel;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.data.channel.ChannelManager;
import com.sliido.ui.BaseFragment;

import butterknife.Bind;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by aminu on 10/11/2016.
 */
public class ChannelSettingsFragment extends BaseFragment {
    private final Listener listener = new Listener() {
        @Override
        public void onEvent(EventBus bus, EventBus.Event event) {
            if (event.tag.equals(ChannelDetailActivity.EVENT_REFRESH_DISPLAY)) {
                if (event.error == null) {
                    refreshDisplay();
                }
            }
        }
    };
    private ChannelInfoProvider provider;

    @Bind(R.id.cb_enable_post_notifications)
    CheckBox checkBox;

    @Bind(R.id.tv_unsubscribe_channel)
    TextView unSubcribeChannel;

    @Bind(R.id.tv_share_invite_link)
    View shareInviteLink;

    public ChannelSettingsFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.provider = (ChannelInfoProvider) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("parent must implement interface " + ChannelInfoProvider.class.getName());
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_channel_settings_channel;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshDisplay();
        EventsCenter.getEventBus().register(listener, ChannelDetailActivity.EVENT_REFRESH_DISPLAY);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventsCenter.getEventBus().unregister(ChannelDetailActivity.EVENT_REFRESH_DISPLAY, listener);
    }


    private void refreshDisplay() {
        if (provider.channelLoaded() == ChannelInfoProvider.CHANNEL_LOADED_SUCCESS) {
            int subscriptionState = provider.getSubscriptionState();
            checkBox.setEnabled(subscriptionState == ChannelInfoProvider.SUBSCRIBED);
            ViewUtils.showByFlag(unSubcribeChannel, subscriptionState == ChannelInfoProvider.SUBSCRIBED
                    || subscriptionState == ChannelInfoProvider.UNSUBSCRIBING);
            if (subscriptionState == ChannelInfoProvider.UNSUBSCRIBING) {
                unSubcribeChannel.setEnabled(true);
                unSubcribeChannel.setText(getString(R.string.un_subscribing));
            } else if (subscriptionState == ChannelInfoProvider.SUBSCRIBED) {
                unSubcribeChannel.setEnabled(true);
                unSubcribeChannel.setText(getString(R.string.un_subscribe));
            }
            checkBox.setChecked(ChannelManager.isNotificationsEnabled(provider.getChannel().getChannelId()));
        } else {
            checkBox.setEnabled(false);
            unSubcribeChannel.setEnabled(false);
        }
        shareInviteLink.setEnabled(true);
        ViewUtils.showByFlag(provider.getChannel() != null, shareInviteLink);
    }


    @OnCheckedChanged(R.id.cb_enable_post_notifications)
    void onChecked(CompoundButton button, boolean checked) {
        if (provider.channelLoaded() == ChannelInfoProvider.CHANNEL_LOADED_SUCCESS) {
            if (provider.getSubscriptionState() == ChannelInfoProvider.SUBSCRIBED) {
                ChannelManager.enableOrDisableNotifications(provider.getChannel().getChannelId(), checked);
            }
        } else {
            if (button.isEnabled())
                UiHelpers.showToast(getString(R.string.channel_not_loaded));
        }

    }

    @OnClick({R.id.tv_unsubscribe_channel, R.id.tv_share_invite_link})
    void click(View view) {
        switch (view.getId()) {
            case R.id.tv_unsubscribe_channel:
                if (provider.channelLoaded() == ChannelInfoProvider.CHANNEL_LOADED_SUCCESS) {
                    provider.onSubscribeOrUnSubscribe();
                }
                break;
            case R.id.tv_share_invite_link:
                Answers.getInstance().logCustom(new CustomEvent(ChannelSettingsFragment.class.getSimpleName() + ":shareChannelInviteLink"));
                provider.onShareInviteLink();
                break;
            default:
                throw new AssertionError();
        }
    }

}
