package com.sliido.ui.channel;

import android.content.Context;
import android.text.format.DateUtils;
import android.widget.TextView;

import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.sliido.R;
import com.sliido.data.channel.Channel;
import com.sliido.ui.BaseFragment;

import butterknife.Bind;

import static com.sliido.ui.channel.ChannelInfoProvider.CHANNEL_LOADED_FAILURE;

/**
 * Created by aminu on 10/9/2016.
 */

public class ChannelBasicInfoFragment extends BaseFragment {

    @Bind(R.id.tv_channel_created_by_name)
    TextView channelCreatedByName;
    @Bind(R.id.tv_channel_domain)
    TextView channelDomain;
    @Bind(R.id.tv_channel_created_at)
    TextView createdAt;
    private ChannelInfoProvider channelInfoProvider;
    private final Listener listener = new Listener() {
        @Override
        public void onEvent(EventBus bus, EventBus.Event event) {
            if (ChannelDetailActivity.EVENT_REFRESH_DISPLAY.equals(event.tag)) {
                refreshDisplay();
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ChannelInfoProvider) {
            this.channelInfoProvider = (ChannelInfoProvider) context;
        } else {
            throw new ClassCastException("parent activity must implement " + ChannelInfoProvider.class.getName());
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_about_channel;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshDisplay();
        EventsCenter.getEventBus().register(listener, ChannelDetailActivity.EVENT_REFRESH_DISPLAY);
    }

    @Override
    public void onPause() {
        EventsCenter.getEventBus().unregister(ChannelDetailActivity.EVENT_REFRESH_DISPLAY, listener);
        super.onPause();
    }

    private void refreshDisplay() {
        if (channelInfoProvider.getChannel() != null) {
            Channel channel = channelInfoProvider.getChannel();
            channelCreatedByName.setText(channel.getCreatedByName());
            createdAt.setText(DateUtils.formatDateTime(getContext(), channel.getCreatedAt()
                    , DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_ABBREV_ALL));
        } else {
            String progressText = channelInfoProvider.channelLoaded() == CHANNEL_LOADED_FAILURE ?
                    getString(R.string.loading_failed) : getString(R.string.loading_in_progress);
            channelCreatedByName.setText(progressText);
            channelDomain.setText(progressText);
            createdAt.setText(progressText);
        }
    }
}
