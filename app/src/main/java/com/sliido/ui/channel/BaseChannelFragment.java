package com.sliido.ui.channel;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.UiHelpers;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.user.UserManager;
import com.sliido.ui.BaseFragment;

import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;

/**
 * Created by null-pointer on 2/28/2016.
 */
public abstract class BaseChannelFragment extends BaseFragment {

    private final RealmChangeListener<Realm> changeListener = new RealmChangeListener<Realm>() {

        @Override
        public void onChange(Realm element) {
            getAdapter().notifyDataChanged();
        }

    };
    protected boolean loading = false;
    private final EventBus.EventsListener evListener = new Listener() {
        @Override
        public void onEvent(EventBus bus, EventBus.Event event) {
            if (event.tag.equals(ChannelManager.EVENT_CHANNELS_SYNCED)) {
                bus.removeStickyEvent(event);
                if (getActivity() == null) return;
                if (ChannelManager.EVENT_DATA_FETCH_NEW_CHANNELS.equals(event.data) || ChannelManager.EVENT_DATA_FETCH_OLD_CHANNELS.equals(event.data)) {
                    loading = false;
                    getAdapter().notifyDataChanged();
                }
                if (event.error != null) {
                    UiHelpers.showToast(event.error.getUserFriendlyMessage());
                }
            }
        }
    };
    @Nullable
    protected String currentUserEmail;
    Realm channelRealm;

    @NonNull
    protected abstract SliidoBaseAdapter<Channel> getAdapter();

    protected abstract boolean isSubscribedFragments();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        channelRealm = Channel.REALM();
        //noinspection ConstantConditions
        if (UserManager.getInstance().isCurrentUserVerified()) {
            currentUserEmail = UserManager.getInstance().getCurrentUser().getEmail();
        }

    }

    @Override
    public void onDestroy() {
        channelRealm.close();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventsCenter.getEventBus().register(ChannelManager.EVENT_CHANNELS_SYNCED, evListener);
        channelRealm.addChangeListener(changeListener);
        fetchNewChannels();
        getAdapter().notifyDataChanged();
    }

    @Override
    public void onPause() {
        EventsCenter.getEventBus().unregister(ChannelManager.EVENT_CHANNELS_SYNCED, evListener);
        channelRealm.removeChangeListener(changeListener);
        super.onPause();
    }

    @NonNull
    protected abstract RecyclerView getRecyclerView();


    @OnClick(R.id.tv_refresh)
    public void clicked(View v) {
        fetchNewChannels();
    }

    private void fetchNewChannels() {
        loading = true;
        getAdapter().notifyDataChanged();
        ChannelManager.findHighSubscribedChannels(isSubscribedFragments());
    }

}
