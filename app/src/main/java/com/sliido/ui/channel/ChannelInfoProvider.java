package com.sliido.ui.channel;

import com.sliido.data.channel.Channel;

/**
 * Created by aminu on 10/9/2016.
 */
interface ChannelInfoProvider {
    int SUBSCRIBING = 1, UNSUBSCRIBING = 2, SUBSCRIBED = 3, UNSUBSCRIBED = 4, PERSONAL_CHANNEL = 5, SUSBCRIPTION_UNKNOWN = -1;

    int CHANNEL_LOADING = 0, CHANNEL_LOADED_SUCCESS = 1, CHANNEL_LOADED_FAILURE = 2;

    String getChannelName();

    int getChannelSubscribersCount();

    int channelLoaded();

    int getSubscriptionState();

    void onSubscribeOrUnSubscribe();

    Channel getChannel();

    void onShareInviteLink();
}
