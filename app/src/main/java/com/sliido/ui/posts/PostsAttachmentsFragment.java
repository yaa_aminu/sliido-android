package com.sliido.ui.posts;

import com.sliido.R;
import com.sliido.ui.BaseFragment;

/**
 * Created by aminu on 11/6/2016.
 */
public class PostsAttachmentsFragment extends BaseFragment {
    @Override
    protected int getLayout() {
        return R.layout.fragment_posts_attachment;
    }
}
