package com.sliido.ui.posts;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.Errors.SliidoException;
import com.pairapp.util.Config;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.TaskManager;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.post.Post;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;
import com.sliido.ui.BaseFragment;
import com.sliido.ui.ImageViewerActivity;
import com.sliido.ui.MainActivity;
import com.sliido.ui.auth.LoginActivity;
import com.sliido.ui.channel.ChannelDetailActivity;
import com.sliido.ui.channel.CreateChannelActivity;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

import static butterknife.ButterKnife.findById;

public class PostsFragment extends BaseFragment {

    public static final int WRITE_STORAGE_PERMISSION_REQUEST_CODE = 3001;
    private static final String TAG = PostsFragment.class.getSimpleName();
    private final User currentUser = UserManager.getInstance().getCurrentUser();
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.empty_view)
    View emptyView;
    @Bind(R.id.empty_view_desc)
    TextView tvEmptyViewDesc;
    @Bind(R.id.tv_refresh)
    TextView tvRefresh;
    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;
    @Bind(R.id.fab_write_new_post)
    View fabWriteNewPost;
    private String filter = "";
    private RealmResults<Post> posts;
    private Realm channelsRealm;
    private Delegate postsFragmentDelegate;
    private Realm postsRealm;
    private boolean loadingFeeds = false;
    private FeedsAdapter postAdapter;
    private final RealmChangeListener changeListener = new RealmChangeListener() {
        @Override
        public void onChange(Object item) {
            if (getActivity() != null) {
                if (GenericUtils.isEmpty(filter)) {
                    postAdapter.notifyDataChanged();
                }
                getActivity().supportInvalidateOptionsMenu();
            }
        }
    };
    private EventBus.EventsListener listener = new Listener() {
        @Override
        public void onEvent(EventBus bus, EventBus.Event event) {
            bus.removeStickyEvent(event);
            if (getActivity() == null) return;
            if (event.tag.equals(ChannelManager.EVENT_FEEDS_SYNCED)) {
                PLog.d(TAG, "sync complete");
                if (ChannelManager.EVENT_DATA_FETCH_OLD_POSTS.equals(event.data)) {
                    loadingFeeds = false;
                    if (event.error != null) {
                        notifyError(event.error);
                    }
                    postAdapter.notifyDataChanged();
                } else if (ChannelManager.EVENT_DATA_FETCH_NEW_POSTS.equals(event.data)) {
                    loadingFeeds = false;
                    postAdapter.notifyDataChanged();
                    onCompleteRefresh();
                    if (event.error != null) {
                        //noinspection ConstantConditions
                        notifyError(event.error);
                    }
                }
            } else if (MainActivity.SEARCH_TEXT_CHANGED.equals(event.tag)) {
                filter = (String) event.data;
                postAdapter.notifyDataChanged();
            }
        }
    };
    @Nullable
    private String channelID;
    private final FeedsAdapter.Delegate delegate = new FeedsAdapter.Delegate() {
        @Override
        public void onLikePost(Post post) {
            //noinspection ConstantConditions
            if (!UserManager.getInstance().isCurrentUserVerified()) {
                notifyAndGoToLogin(getContext(), getString(R.string.login_to_like));
            } else if (post.getPostedBy().equals(currentUser.getEmail()) ||
                    ChannelManager.getSubScribedChannels().contains(post.getChannelId())
                    || post.getChannelName().equalsIgnoreCase(Config.appName())) {
                ChannelManager.incrementLikes(post.getPostId());
            } else {
                showLockedOutDiaolog(getString(R.string.like), post);
            }
        }

        @Override
        public void onSeeMore(Post post) {
            PostDetailActivity.viewPostDetails(getContext(), post);
        }

        @Override
        public void onViewChannel(Post post) {
            if (UserManager.getInstance().isCurrentUserVerified()) {
                ChannelDetailActivity.viewChannel(getContext(), post.getChannelId(), post.getChannelName(), PostsFragment.class.getSimpleName());
            } else {
                notifyAndGoToLogin(getContext(), getString(R.string.login_to_view_channels));
            }
        }

        @Override
        public void onCommentOnPost(Post post) {
            //noinspection ConstantConditions
            if (!UserManager.getInstance().isCurrentUserVerified()) {
                notifyAndGoToLogin(getContext(), getString(R.string.login_to_comment));
            } else if (post.getPostedBy().equals(currentUser.getEmail()) ||
                    ChannelManager.getSubScribedChannels().contains(post.getChannelId())
                    || post.getChannelName().equalsIgnoreCase(Config.appName())) {
                CommentsActivity.commentOnPost(getContext(), post);
            } else {
                showLockedOutDiaolog(getString(R.string.reply_to), post);

            }
        }

        @Override
        public void onDownloadItem(Post post) {
            if (GenericUtils.hasPermission(PostsFragment.this, true, WRITE_STORAGE_PERMISSION_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                DownloadManager manager = ((DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE));
                //noinspection ConstantConditions
                manager.enqueue(new DownloadManager.Request(
                        Uri.parse(post.getAttachment()))
                        .setDescription(post.getAttachmentTitle())
                        .setTitle(getString(R.string.downloading))
                        .setDestinationUri(Uri.fromFile(new File(post.getPath())))
                );
            }
        }

        @Override
        public void onViewPhoto(Post post) {
            Intent intent = new Intent(getContext(), ImageViewerActivity.class);
            intent.putExtra(ImageViewerActivity.EXTRA_IMAGE, post.getAttachment());
            startActivity(intent);
        }

        @Override
        public void retryPosting(Post post) {
            ChannelManager.retryPosting(post);
        }

        @Override
        public void cancelPost(Post post) {
            ChannelManager.cancelPost(post);
        }

        @Override
        public boolean showLoadingMore() {
            return recyclerView != null && recyclerView.canScrollVertically(1) && loadingFeeds;
        }

        @Override
        public void onViewPost(Post post) {
            Answers.getInstance().logCustom(new CustomEvent("viewPost"));
            PostDetailActivity.viewPostDetails(getContext(), post);
        }

        @Override
        public Context context() {
            return getContext();
        }

        @Override
        public void onItemClick(SliidoBaseAdapter<Post> adapter, View view, int position, long id) {
            //do nothing
        }

        @Override
        public boolean onItemLongClick(SliidoBaseAdapter<Post> adapter, View view, int position, long id) {
            //do nothing
            Answers.getInstance().logCustom(new CustomEvent(PostsFragment.class.getSimpleName() + ":vainLongClickAttempt"));
            return false;
        }

        @Override
        public void onLoadMoreItems(int position) {
            if (!loadingFeeds) {
                loadingFeeds = true;
                postAdapter.notifyDataChanged();
                ChannelManager.findOldFeeds(channelID);
            }
        }

        @NonNull
        @Override
        public List<Post> dataSet() {
            ViewUtils.showByFlag(posts.isEmpty(), emptyView);
            ViewUtils.showByFlag(!posts.isEmpty(), refreshLayout);
            if (posts.isEmpty()) {
                //why are there no posts?
                ViewUtils.showByFlag(GenericUtils.isEmpty(filter) && loadingFeeds, findById(emptyView, R.id.loading_posts_empty_view));
                ViewUtils.showByFlag(GenericUtils.isEmpty(filter) && !loadingFeeds, findById(emptyView, R.id.no_feed_empty_view));
                ViewUtils.showByFlag(!GenericUtils.isEmpty(filter), findById(emptyView, R.id.no_results_found));
            }
            return posts;
        }

        @Override
        public void onSharePost(Post post) {
            PostDetailActivity.sharePost(getContext(), post);
            Answers.getInstance().logCustom(new CustomEvent("sharePost"));
        }

    };
    @Nullable
    private Channel channel;

    public PostsFragment() {
    }

    public static void notifyAndGoToLogin(final Context context, String text) {
        new AlertDialog.Builder(context)
                .setMessage(text)
                .setTitle(context.getString(R.string.locked_out))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                    }
                }).create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == WRITE_STORAGE_PERMISSION_REQUEST_CODE) {
            if (GenericUtils.wasPermissionGranted(permissions, grantResults)) {
                UiHelpers.showToast(getString(R.string.notice_download_attachment));
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void notifyError(@NonNull SliidoException error) {
        UiHelpers.showToast(error.getUserFriendlyMessage());
    }

    private void showLockedOutDiaolog(String verb, final Post post) {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.locked_out))
                .setMessage(getString(R.string.locked_out_message, verb))
                .setPositiveButton(R.string.subscribe, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        postsFragmentDelegate.onSubscribeToChannel(post.getChannelId());
                    }
                }).setNegativeButton(R.string.cancel, null)
                .create().show();
    }

    @OnClick({R.id.tv_refresh, R.id.fab_write_new_post})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_write_new_post:
                if (UserManager.getInstance().isCurrentUserVerified()) {
                    if (channel != null) {
                        NewPostActivity.createNewPost(getActivity(), channelID, channel.getName());
                    } else {
                        //load all channels, present them in a
                        showChannelPicker();
                    }
                } else {
                    notifyAndGoToLogin(getContext(), getString(R.string.login_in_to_post));
                }
                break;
            case R.id.tv_refresh:
                loadingFeeds = true;
                postAdapter.notifyDataChanged();
                ChannelManager.findNewFeeds(channelID);
                break;

        }
    }

    private void showChannelPicker() {
        final User currentUser = UserManager.getInstance().getCurrentUser();
        GenericUtils.assertThat(currentUser != null);
        final RealmResults<Channel> realmResults = channelsRealm.where(Channel.class)
                .equalTo(Channel.FIELD_CREATED_BY, currentUser.getEmail())
                .findAllSorted(Channel.FIELD_NAME);
        if (realmResults.isEmpty()) {
            new AlertDialog.Builder(getContext())
                    .setCancelable(false)
                    .setMessage(R.string.no_board_notice)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO: 10/28/17 go to create page activity.
                            startActivity(new Intent(getActivity(), CreateChannelActivity.class));
                        }
                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            }).create().show();
        } else {
            if (realmResults.size() == 1) {
                NewPostActivity.createNewPost(getActivity(), realmResults.get(0).getChannelId(), realmResults.get(0).getName());
                return;
            }
            new AlertDialog.Builder(getContext())
                    .setCancelable(false)
                    .setTitle(R.string.post_on)
                    .setAdapter(new ArrayAdapter<Channel>(getContext(), android.R.layout.simple_list_item_1, realmResults) {
                        @NonNull
                        @Override
                        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            ((TextView) v).setText(getItem(position).getName());
                            return v;
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            NewPostActivity.createNewPost(getActivity(), realmResults.get(which).getChannelId(), realmResults.get(which).getName());
                        }
                    }).create().show();
        }
    }

    private void onCompleteRefresh() {
        if (getActivity() != null && isAdded()) {
            if (!tvRefresh.isEnabled()) {
                tvRefresh.setText(R.string.tap_to_refresh);
                tvRefresh.setEnabled(true);
                ViewUtils.showViews(tvEmptyViewDesc);
            }
            if (refreshLayout != null) {
                try {
                    // TODO: 3/2/2016 fix this better
                    refreshLayout.setRefreshing(false); //quick fix
                } catch (NullPointerException ignored) {
                }
            }
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_feeds;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LinearLayoutManager lManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        postAdapter = new FeedsAdapter(delegate);
        recyclerView.setAdapter(postAdapter);
        recyclerView.setLayoutManager(lManager);
        if (refreshLayout != null) {
            refreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorAccent);
            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    refreshLayout.setRefreshing(true);
                    ChannelManager.findNewFeeds(channelID);
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (refreshLayout != null) {
                        refreshLayout.setRefreshing(false);
                    }

                }
            }, 3000);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postsFragmentDelegate = ((PostsFragment.Delegate) getActivity()); //might throw a ClassCastException
        setHasOptionsMenu(true);
        Bundle arguments = getArguments();
        if (arguments != null) {
            channelID = arguments.getString(NewsFeedActivity.EXTRA_CHANNEL_ID);
        }
        postsRealm = Post.REALM();
        channelsRealm = Channel.REALM();
        if (channelID != null) {
            channel = channelsRealm.where(Channel.class)
                    .equalTo(Channel.FIELD_CHANNEL_ID, channelID).findFirst();
            GenericUtils.assertThat(channel != null);
        }

        posts = findPosts(channelID);
        loadingFeeds = true;
        ChannelManager.findNewFeeds(channelID);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final User currentUser = UserManager.getInstance().getCurrentUser();
        if (channelID != null) {
            ViewUtils.showByFlag(channel != null && currentUser != null && channel.getCreatedBy().equals(currentUser.getEmail()), fabWriteNewPost);
        } else {
            ViewUtils.showViews(fabWriteNewPost);
        }
    }

    @NonNull
    private RealmResults<Post> findPosts(@Nullable String fromChannel) {
        RealmQuery<Post> query = postsRealm.where(Post.class);
        if (!GenericUtils.isEmpty(fromChannel)) {
            query.equalTo(Post.FIELD_CHANNEL_ID, fromChannel);
        }
        RealmResults<Post> results = query.findAllSortedAsync(Post.FIELD_STATE, Sort.ASCENDING, Post.FIELD_DATE_CREATED,
                Sort.DESCENDING);
        //noinspection unchecked
        results.addChangeListener(changeListener);
        return results;
    }

    @Override
    public void onResume() {
        super.onResume();
        //noinspection unchecked
        posts.addChangeListener(changeListener);
        EventsCenter.getEventBus().register(listener, ChannelManager.EVENT_FEEDS_SYNCED, MainActivity.SEARCH_TEXT_CHANGED);
        postAdapter.notifyDataChanged();
    }

    @Override
    public void onPause() {
        EventBus eventBus = EventsCenter.getEventBus();
        eventBus.unregister(ChannelManager.EVENT_FEEDS_SYNCED, listener);
        eventBus.unregister(MainActivity.SEARCH_TEXT_CHANGED, listener);
        posts.removeChangeListener(changeListener);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        postsRealm.close();
        channelsRealm.close();
        super.onDestroy();
    }

    public interface Delegate {
        void onSubscribeToChannel(String channelID);
    }


}
