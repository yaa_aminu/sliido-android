package com.sliido.ui.posts;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.post.Comment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by aminu on 10/16/2016.
 */
class CommentsAdapter extends SliidoBaseAdapter<Comment> {

    private final Delegate delegate;

    CommentsAdapter(Delegate delegate) {
        super(delegate);
        this.delegate = delegate;
    }

    @Override
    protected void doBindHolder(Holder holder, int position) {
        Comment comment = getItem(position);
        ((ViewHolder) holder).commenterName.setTag(comment);
        ((ViewHolder) holder).commenterName.setText(comment.getPostedByName());
        ((ViewHolder) holder).commentBody.setText(comment.getCommentBody());
        if (delegate.isPosting(comment)) {
            ((ViewHolder) holder).datePosted.setText(R.string.posting);
        } else {
            long now = System.currentTimeMillis(), then = comment.getCreatedAt();
            CharSequence formattedDate = ((now - then) < 1000 * 60) ? delegate.context().getString(R.string.just_now)
                    : DateUtils.getRelativeTimeSpanString(then, now, DateUtils.MINUTE_IN_MILLIS);
            ((ViewHolder) holder).datePosted.setText(formattedDate);
        }
    }

    @Override
    protected Holder doCreateHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.comments_list_item, parent, false), delegate);
    }

    interface Delegate extends SliidoBaseAdapter.Delegate<Comment> {
        void onViewCommenter(Comment comment);

        boolean isPosting(Comment comment);

        Context context();
    }

    class ViewHolder extends Holder {
        private final Delegate delegate;
        @Bind(R.id.tv_commenter_name)
        TextView commenterName;
        @Bind(R.id.tv_date_posted)
        TextView datePosted;
        @Bind(R.id.tv_comment_body)
        TextView commentBody;

        public ViewHolder(View view, Delegate delegate) {
            super(view);
            this.delegate = delegate;
        }

        @OnClick(R.id.tv_commenter_name)
        void onclick(View v) {
            delegate.onViewCommenter(((Comment) v.getTag()));
        }
    }
}
