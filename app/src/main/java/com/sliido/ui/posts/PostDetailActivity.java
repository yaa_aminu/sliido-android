package com.sliido.ui.posts;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.util.EventBus;
import com.pairapp.util.UiHelpers;
import com.sliido.R;
import com.sliido.SliidoApplication;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.post.Post;
import com.sliido.ui.SliidoBaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;

public class PostDetailActivity extends SliidoBaseActivity {
    public static final String EXTRA_POST_ID = "postid";

    @Bind(R.id.tv_post_body)
    TextView postBody;
    @Bind(R.id.tv_channel_name)
    TextView channelName;
    @Bind(R.id.tv_date_posted)
    TextView datePosted;

    @Bind(R.id.tv_like_post)
    TextView likePost;

    @Bind(R.id.tv_reply)
    TextView replies;

    private Realm postRealm;
    private Post post;
    private final RealmChangeListener<Realm> changeListener = new RealmChangeListener<Realm>() {
        @Override
        public void onChange(Realm realm) {
            refreshDisplay();
        }
    };

    public static void viewPostDetails(Context context, Post post) {
        Intent intent = new Intent(context, PostDetailActivity.class);
        intent.putExtra(EXTRA_POST_ID, post.getPostId());
        context.startActivity(intent);
    }

    public static void sharePost(Context context, Post post) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, post.getPostBody() + "\n\n\n" +
                context.getString(R.string.share_post_attribution, post.getChannelName(), SliidoApplication.BASE_URL + "ch?id=" + post.getChannelId()));
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException ignored) {
            UiHelpers.showErrorDialog(context, context.getString(R.string.no_app_for_sharing_stuffs));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        ButterKnife.bind(this);
        String postId = getIntent().getStringExtra(EXTRA_POST_ID);
        if (TextUtils.isEmpty(postId)) {
            throw new IllegalArgumentException("no post id passed");
        }

        postRealm = Post.REALM();
        post = postRealm.where(Post.class).equalTo(Post.FIELD_POST_ID, postId).findFirst();
        if (post == null) {
            Answers.getInstance().logCustom(new CustomEvent("missingPost"));
            finish();
            return;
        }
        refreshDisplay();
        ChannelManager.refreshPost(postId);
    }

    @SuppressLint("SetTextI18n")
    private void refreshDisplay() {
        postBody.setText(TextFormattingUtils.formatText(post.getPostBody(), '*', '_'));
        channelName.setText(" " + post.getChannelName());
        Resources resources = getResources();
        likePost.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        likePost.setCompoundDrawablesWithIntrinsicBounds(post.isLiked() ? R.drawable.ic_favorite_black_24dp : R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
        likePost.setTextColor(post.isLiked() ? resources.getColor(R.color.colorAccent) : resources.getColor(R.color.colorPrimary));
        replies.setText("  " + post.getCommentCount());
        likePost.setText("  " + post.getLikes());
        long now = System.currentTimeMillis();
        long then = post.getCreatedAt();
        CharSequence formattedDate = ((now - then) < 1000 * 60) ? getString(R.string.just_now)
                : DateUtils.getRelativeTimeSpanString(then, now, DateUtils.MINUTE_IN_MILLIS);
        datePosted.setText(formattedDate);
        postRealm.addChangeListener(changeListener);
    }

    @OnClick({R.id.v_share_post, R.id.tv_like_post, R.id.tv_reply, R.id.ib_exit})
    void OnClick(View v) {
        if (post.getState() != Post.POSTED && v.getId() != R.id.v_share_post) {
            UiHelpers.showToast(getString(R.string.not_available));
            return;
        }
        switch (v.getId()) {
            case R.id.tv_like_post:
                if (com.sliido.data.user.UserManager.getInstance().isCurrentUserVerified()) {
                    ChannelManager.incrementLikes(post.getPostId());
                } else {
                    PostsFragment.notifyAndGoToLogin(this, getString(R.string.login_to_like));
                }
                break;
            case R.id.v_share_post:
                sharePost(this, post);
                break;
            case R.id.tv_reply:
                if (com.sliido.data.user.UserManager.getInstance().isCurrentUserVerified()) {
                    CommentsActivity.commentOnPost(this, post);
                } else {
                    PostsFragment.notifyAndGoToLogin(this, getString(R.string.login_to_comment));
                }
                break;
            case R.id.ib_exit:
                finish();
                break;
            default:
                throw new AssertionError();
        }
    }

    @Override
    protected void onDestroy() {
        postRealm.removeChangeListener(changeListener);
        postRealm.close();
        super.onDestroy();
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        //do nothing
    }
}
