package com.sliido.ui.posts;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.TaskManager;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.NotificationEntry;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.post.Comment;
import com.sliido.data.post.Post;
import com.sliido.ui.SliidoBaseActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.sliido.R.id.recyclerView;

public class CommentsActivity extends SliidoBaseActivity {

    public static final String EXTRA_POST_ID = "extraPostID";
    public static final String FROM_STATUS_BAR = "fromStatusBar";
    @Bind(recyclerView)
    RecyclerView commentsRecyclerView;

    @Bind(R.id.et_comment_box)
    EditText commentBox;

    @Bind(R.id.ib_reply)
    View ibReply;

    @Bind(R.id.tv_like_post)
    TextView likePost;

    @Bind(R.id.tv_reply)
    TextView replies;

    @Bind(R.id.tv_empty_view_no_loading_text)
    TextView emptyViewText;

    @Bind(R.id.tv_loading_text)
    TextView loadingText;

    @Bind(R.id.empty_view)
    View emptyView;

    Realm realm;
    RealmResults<Comment> comments;
    private Post post;
    private SliidoBaseAdapter<Comment> commentSliidoBaseAdapter;
    private boolean isLoading = false;
    private final RealmChangeListener changeListener = new RealmChangeListener() {
        @Override
        public void onChange(Object element) {
            refreshDisplay();
        }
    };
    private final CommentsAdapter.Delegate delegate = new CommentsAdapter.Delegate() {

        @Override
        public void onLoadMoreItems(int position) {
            if (!isLoading) {
                isLoading = true;
                refreshDisplay();
                ChannelManager.loadComments(comments.get(position).getPostID(), true);
            }
        }

        @Override
        public void onItemClick(SliidoBaseAdapter<Comment> adapter, View view, int position, long id) {

        }

        @Override
        public boolean onItemLongClick(SliidoBaseAdapter<Comment> adapter, View view, int position, long id) {
            return false;
        }

        @NonNull
        @Override
        public List<Comment> dataSet() {
            return comments;
        }

        @Override
        public boolean showLoadingMore() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                return commentsRecyclerView.canScrollVertically(1) && isLoading;
            } else {
                return comments.size() > 10 && isLoading;
            }
        }

        @Override
        public void onViewCommenter(Comment comment) {
            //do nothing
            Answers.getInstance().logCustom(new CustomEvent("viewUser"));
        }

        @Override
        public boolean isPosting(Comment comment) {
            return ChannelManager.isPosting(comment);
        }

        @Override
        public Context context() {
            return CommentsActivity.this;
        }
    };

    public static void commentOnPost(Context context, Post post) {
        Intent intent = new Intent(context, CommentsActivity.class);
        intent.putExtra(EXTRA_POST_ID, post.getPostId());
        context.startActivity(intent);
    }

    private static void clearNotificationsForParentPost(final String postId) {
        TaskManager.execute(new Runnable() {
            @Override
            public void run() {
                Realm realm = Channel.REALM();
                try {
                    NotificationEntry entry = realm.where(NotificationEntry.class)
                            .equalTo("postId", postId)
                            .findFirst();
                    if (entry != null) {
                        realm.beginTransaction();
                        entry.setRead(true);
                        realm.commitTransaction();
                    }
                } finally {
                    realm.close();
                }
            }
        }, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Post.REALM();
        setContentView(R.layout.fragment_comments);
        ButterKnife.bind(this);
        String postID = getIntent().getStringExtra(EXTRA_POST_ID);
        GenericUtils.ensureNotEmpty(postID);
        post = realm.where(Post.class).equalTo(Post.FIELD_POST_ID, postID).findFirst();
        GenericUtils.ensureConditionTrue(post != null, "post not found");
        comments = realm.where(Comment.class).equalTo(Comment.FIELD_POST_ID, postID).findAllSortedAsync(Comment.FIELD_CREATED_AT, Sort.DESCENDING);

        loadingText.setText(R.string.loading_comments);
        emptyViewText.setText(R.string.no_comments);

        commentSliidoBaseAdapter = new CommentsAdapter(delegate);
        commentsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));
        commentsRecyclerView.setAdapter(commentSliidoBaseAdapter);
        EventsCenter.getEventBus().register(ChannelManager.EVENT_COMMENTS_LOADED, this);
        isLoading = true;
        ChannelManager.loadComments(postID, false);
        if (getIntent().hasExtra(FROM_STATUS_BAR)) {
            clearNotificationsForParentPost(postID);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshDisplay();
        post.addChangeListener(changeListener);
        comments.addChangeListener(changeListener);
    }

    @Override
    protected void onPause() {
        post.removeChangeListener(changeListener);
        comments.removeChangeListener(changeListener);
        super.onPause();
    }

    @OnClick({R.id.tv_like_post, R.id.v_share_post, R.id.ib_reply, R.id.ib_exit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_like_post:
                ChannelManager.incrementLikes(post.getPostId());
                break;
            case R.id.ib_reply:
                String comment = commentBox.getText().toString().trim();
                if (GenericUtils.isEmpty(comment)) {
                    return;
                } else if (comment.length() > getResources().getInteger(R.integer.max_comment_length)) {
                    commentBox.setError(getString(R.string.error_comment_too_long, getResources().getInteger(R.integer.max_comment_length)));
                } else {
                    ChannelManager.addComment(post.getPostId(), post.getChannelId(), comment);
                    commentBox.setText("");
                }
                break;
            case R.id.v_share_post:
                PostDetailActivity.sharePost(this, post);
                break;
            case R.id.ib_exit:
                finish();
                break;
            default:
                throw new AssertionError();
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();
        EventsCenter.getEventBus().unregister(ChannelManager.EVENT_COMMENTS_LOADED, this);
        super.onDestroy();
    }

    private void refreshDisplay() {
        likePost.setText("  " + post.getLikes());
        likePost.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        likePost.setCompoundDrawablesWithIntrinsicBounds(post.isLiked() ? R.drawable.ic_favorite_black_24dp : R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
        likePost.setTextColor(post.isLiked() ? getResources().getColor(R.color.colorAccent) : getResources().getColor(android.R.color.black));
        replies.setText("  " + post.getCommentCount());
        ViewUtils.showByFlag(commentsRecyclerView, !comments.isEmpty());
        ViewUtils.showByFlag(emptyView, comments.isEmpty());
        if (comments.isEmpty()) {
            ViewUtils.showByFlag(post.getCommentCount() > 0 && isLoading, ButterKnife.findById(this, R.id.loading_empty_view_root));
            ViewUtils.showByFlag(post.getCommentCount() <= 0 || !isLoading, ButterKnife.findById(this, R.id.tv_empty_view_no_loading_text));
            emptyViewText.setText(post.getCommentCount() > 0 ? getString(R.string.unable_to_load_comments) : getString(R.string.no_comments));
        } else {
            commentSliidoBaseAdapter.notifyDataChanged();
        }
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        yourBus.removeStickyEvent(event);
        if (ChannelManager.EVENT_COMMENTS_LOADED.equals(event.tag)) {
            isLoading = false;
            if (event.error != null) {
                UiHelpers.showToast(event.error.getUserFriendlyMessage());
            }
            refreshDisplay();
        }
    }
}
