package com.sliido.ui.posts;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pairapp.Errors.SliidoException;
import com.pairapp.util.EventBus;
import com.pairapp.util.FileUtils;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.pairapp.util.TaskManager;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.post.Post;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;
import com.sliido.ui.MainActivity;
import com.sliido.ui.SliidoBaseActivity;
import com.sliido.ui.channel.CreateChannelActivity;

import java.io.File;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class NewPostActivity extends SliidoBaseActivity {

    public static final int PDF = 0;
    public static final int OFFICE_DOC = 1;
    public static final int POWERPOINT_PRESENTATION = 2, SPREADSHEET = 3;
    public static final int REQUEST_CODE_PICK_FILE = 1002;
    public static final String TAG = NewPostActivity.class.getSimpleName();
    public static final String EXTRA_CHANNEL_ID = "channelId";
    public static final String EXTRA_CHANNEL_NAME = "channelName";
    public static final int STORAGE_PERMISSION_ATTACH_PDF = 1004;
    public static final int STORAGE_PERMISSION_ATTACH_DOCX = 1006;
    public static final String EXTRA_TEXT = "text", EXTRA_ATTACHMENT = "attachment";
    private static final int STORAGE_PERMISSION_PICK_FROM_DEVICE = 1005;
    @Bind(R.id.et_post_body)
    EditText etPostBody;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.tv_attachement_title)
    TextView attachmentTitle;
    @Bind(R.id.tv_file_size)
    TextView attachementSise;
    @Bind(R.id.tv_file_type)
    TextView fileType;
    @Bind(R.id.iv_download)
    ImageButton download;
    @Bind(R.id.attachment_pannel_parent)
    View attachmentPanelParent;
    ProgressDialog progressDialog;
    @Nullable
    private String attachmentFilePath;
    @Nullable
    private String channelId, channelName;
    private String text;
    private Realm realm;

    public static void createNewPost(Activity context, String channelID, String channelName) {
        Intent intent = new Intent(context, NewPostActivity.class);
        intent.putExtra(EXTRA_CHANNEL_ID, channelID);
        intent.putExtra(EXTRA_CHANNEL_NAME, channelName);
        context.startActivityForResult(intent, MainActivity.REQUEST_NEW_POST);
    }

    public static void createNewPost(Activity context, String text) {
        Intent intent = new Intent(context, NewPostActivity.class);
        intent.putExtra(EXTRA_TEXT, text);
        context.startActivity(intent);
    }

    public static void createNewPostWithAttachment(Activity context, Uri attachment) {
        Intent intent = new Intent(context, NewPostActivity.class);
        intent.putExtra(EXTRA_ATTACHMENT, attachment);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Channel.REALM();
        setContentView(R.layout.activity_new_post);
        ButterKnife.bind(this);
        channelId = getIntent().getStringExtra(EXTRA_CHANNEL_ID);
        channelName = getIntent().getStringExtra(EXTRA_CHANNEL_NAME);
        if (getIntent().hasExtra(EXTRA_TEXT)) {
            text = getIntent().getStringExtra(EXTRA_TEXT);
            etPostBody.setText(text);
            showChannelPicker();
        } else if (getIntent().hasExtra(EXTRA_ATTACHMENT)) {
            new Handler()
                    .postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            retrieveFile((Uri) getIntent().getParcelableExtra(EXTRA_ATTACHMENT), true);
                        }
                    }, TimeUnit.MILLISECONDS.toMillis(750));
        } else if (channelId == null || channelName == null) {
            if (GenericUtils.isEmpty(channelId)) {
                throw new IllegalArgumentException("no channel, pass one  via Intent#putExtra(NewPostActivity#Extra_CHANNEL_ID,channelId)");
            }
            if (GenericUtils.isEmpty(channelName)) {
                throw new IllegalArgumentException("no channel, pass one  via Intent#putExtra(NewPostActivity#Extra_CHANNEL_NAME,channelName)");
            }
        }
        toolbar.setTitle(R.string.new_post);
        setSupportActionBar(toolbar);
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        download.setImageDrawable(getResources().getDrawable(R.drawable.ic_clear_white_24dp));
        ViewUtils.hideViews(attachmentPanelParent);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachmentFilePath = "";
                ViewUtils.hideViews(attachmentPanelParent);
            }
        });
    }

    private void showChannelPicker() {
        final User currentUser = UserManager.getInstance().getCurrentUser();
        GenericUtils.assertThat(currentUser != null);
        final RealmResults<Channel> realmResults = realm.where(Channel.class)
                .equalTo(Channel.FIELD_CREATED_BY, currentUser.getEmail())
                .findAllSorted(Channel.FIELD_NAME);
        if (realmResults.isEmpty()) {
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage(R.string.no_board_notice)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO: 10/28/17 go to create page activity.
                            startActivity(new Intent(NewPostActivity.this, CreateChannelActivity.class));
                        }
                    }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }).create().show();
        } else {
            if (realmResults.size() == 1) {
                channelName = realmResults.get(0).getName();
                channelId = realmResults.get(0).getChannelId();
                UiHelpers.showToast(getString(R.string.post_hint, channelName));
                return;
            }
            new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle(R.string.post_on)
                    .setAdapter(new ArrayAdapter<Channel>(this, android.R.layout.simple_list_item_1, realmResults) {
                        @NonNull
                        @Override
                        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View v = super.getView(position, convertView, parent);
                            ((TextView) v).setText(getItem(position).getName());
                            return v;
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            channelId = realmResults.get(which).getChannelId();
                            channelName = realmResults.get(which).getName();
                        }
                    }).create().show();
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        //do nothing
    }

    @OnClick({R.id.iv_add_photo_from_device, R.id.iv_attach_pdf, R.id.iv_attach_docx})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add_photo_from_device:
                if (GenericUtils.hasPermission(this, true,
                        STORAGE_PERMISSION_PICK_FROM_DEVICE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    pickFile("image/*");
                }
                break;
            case R.id.iv_attach_pdf:
                if (GenericUtils.hasPermission(this, true,
                        STORAGE_PERMISSION_ATTACH_PDF, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    pickFile("application/pdf");
                }
                break;
            case R.id.iv_attach_docx:
                if (GenericUtils.hasPermission(this, true,
                        STORAGE_PERMISSION_ATTACH_DOCX, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    pickFile("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                }
                break;
            default:
                throw new AssertionError();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (GenericUtils.wasPermissionGranted(permissions, grantResults)) {
            if (requestCode == STORAGE_PERMISSION_ATTACH_PDF) {
                pickFile("application/pdf");
            } else if (requestCode == STORAGE_PERMISSION_ATTACH_DOCX) {
                pickFile("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            } else if (requestCode == STORAGE_PERMISSION_PICK_FROM_DEVICE) {
                pickFile(("image/*"));
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void pickFile(String mimeType) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(mimeType);
        try {
            startActivityForResult(intent, REQUEST_CODE_PICK_FILE);
        } catch (ActivityNotFoundException e) {
            UiHelpers.showToast(getString(R.string.no_app_to_open_file_type));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_PICK_FILE) {
                Uri uri = data.getData();
                retrieveFile(uri, false);
            }
        }
    }

    private void retrieveFile(final Uri uri, final boolean showPicker) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.progress_retrieving_file));
        dialog.setCancelable(false);
        dialog.show();
        TaskManager.executeNow(new Runnable() {
            @Override
            public void run() {
                final String localPath = com.pairapp.util.FileUtils.resolveContentUriToFilePath(uri, true);
                PLog.d(TAG, "retrieved file path %s", localPath);
                if (FileUtils.getMimeType(localPath).startsWith("image/")) {
                    //TODO resize the image
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        File file = new File(localPath);
                        if (file.exists()) {
                            if (file.length() <= Post.MAX_ATTACHMENT_SIZE) {
                                ViewUtils.showViews(attachmentPanelParent);
                                attachmentFilePath = file.getAbsolutePath();
                                String fileName = file.getName();
                                attachmentTitle.setText(fileName.substring(0, fileName.length() - 4));
                                attachementSise.setText(FileUtils.sizeInLowestPrecision(file.length()));
                                fileType.setText(FileUtils.getExtension(fileName));
                                if (showPicker) {
                                    showChannelPicker();
                                }
                            } else {
                                UiHelpers.showDialog(NewPostActivity.this, getString(R.string.error), getString(R.string.file_too_large, Post.MAX_ATTACHMENT_SIZE));
                            }
                        } else {
                            UiHelpers.showDialog(NewPostActivity.this, getString(R.string.error), getString(R.string.error_file_not_found));
                        }
                    }
                });
            }
        }, true);
    }

//    @OnTextChanged(R.id.et_post_body)
//    void onTextChanged(Editable text) {
//        if (etPostBody.getTag() != null) {  //self changed
//            etPostBody.setTag(null);
//        } else {
//            int index = etPostBody.getSelectionEnd();
//            etPostBody.setTag(index);
//            etPostBody.setText(text);
//            etPostBody.setSelection(text.length());
//        }
//    }

    @OnClick(R.id.publish)
    void publish() {
        String postBody = etPostBody.getText().toString().trim();
        if (TextUtils.isEmpty(attachmentFilePath) && TextUtils.isEmpty(postBody)) {
            etPostBody.setError(getString(R.string.error_field_required));
        } else if (postBody.length() < Post.MIN_POST_LENGTH && TextUtils.isEmpty(attachmentFilePath)) {
            etPostBody.setError(getString(R.string.error_post_too_short, Post.MIN_POST_LENGTH));
        } else {
            progressDialog.show();
            if (TextUtils.isEmpty(postBody)) {
                postBody = getString(R.string.empty_post_body_placeholder, channelName, getDescription(attachmentFilePath));
            }
            ChannelManager.createPost(postBody, attachmentFilePath, channelId, channelName, new UserManager.Callback() {
                @Override
                public void done(SliidoException e) {
                    progressDialog.dismiss();
                    if (e == null) {
                        UiHelpers.showToast(getString(R.string.post_submitted));
                        Intent intent = new Intent(NewPostActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        UiHelpers.showErrorDialog(NewPostActivity.this, e.getUserFriendlyMessage());
                    }
                }
            });
        }
    }

    private String getDescription(String attachmentFilePath) {
        switch (Post.extensionToType(attachmentFilePath)) {
            case Post.IMAGE:
                return getString(R.string.picture);
            case Post.VIDEO:
                return getString(R.string.video);
            case Post.AUDIO:
                return getString(R.string.audio);
            case Post.OTHER:
                return getString(R.string.document);
        }
        return null;
    }

}
