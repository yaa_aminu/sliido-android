package com.sliido.ui.posts;

import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.SparseIntArray;

/**
 * Created by yaaminu on 10/28/17.
 */

public class TextFormattingUtils {
    public static final int IN = 1, OUT = 0;

    private static CharacterStyle getStyle(char token) {
        switch (token) {
            case '*':
                return new StyleSpan(Typeface.BOLD);
            case '_':
                return new StyleSpan(Typeface.ITALIC);
            default:
                throw new AssertionError();
        }
    }

    public static CharSequence formatText(CharSequence text, char... tokens) {
        SparseIntArray states = new SparseIntArray(tokens.length);
        for (char token : tokens) {
            states.put(token,-1);
        }
        SpannableStringBuilder ssb = new SpannableStringBuilder(text);
        for (int i = 0; i < text.length(); i++) {
            for (char token : tokens) {
                if (text.charAt(i) == token) {
                    if (states.get(token) != -1) { //in the word
                        ssb.setSpan(getStyle(token), states.get(token), i, 0);
                        states.put(token, -1); //out
                    } else {
                        states.put(token, i); //we are now in a word
                    }
                }
            }
        }
        return ssb;
    }

}
