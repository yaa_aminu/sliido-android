package com.sliido.ui.posts;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.pairapp.Errors.SliidoException;
import com.pairapp.util.EventBus;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.user.User;
import com.sliido.data.user.UserManager;
import com.sliido.ui.SliidoBaseActivity;
import com.sliido.ui.channel.ChannelDetailActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;

public class NewsFeedActivity extends SliidoBaseActivity implements PostsFragment.Delegate {

    public static final String POSTSF_FRAGMENT = "postsfFragment";
    public static final String EXTRA_CHANNEL_ID = "channelID",
            EXTRA_CHANNEL_NAME = "channelName";
    public static final String CREATED_BY = "createdBy";

    String channelID, channelName, createdBy;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.fab_subscribe)
    View subscribeFab;

    Realm channelRealm;
    private Channel channel;
    private RealmChangeListener<Realm> changeListener = new RealmChangeListener<Realm>() {
        @Override
        public void onChange(Realm element) {
            refreshDisplay(channel);
        }
    };

    public static void viewPostsFor(Context context, Channel channel) {
        if (UserManager.getInstance().isCurrentUserVerified()) {
            Intent intent = new Intent(context, NewsFeedActivity.class);
            intent.putExtra(EXTRA_CHANNEL_ID, channel.getChannelId());
            intent.putExtra(EXTRA_CHANNEL_NAME, channel.getName());
            intent.putExtra(CREATED_BY, channel.getCreatedBy());
            context.startActivity(intent);
        } else {
            UiHelpers.showDialog(context, context.getString(R.string.locked_out), "You need to login first");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feed);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        channelRealm = Channel.REALM();
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        channelID = bundle.getString(EXTRA_CHANNEL_ID);
        channelName = bundle.getString(EXTRA_CHANNEL_NAME);
        createdBy = bundle.getString(CREATED_BY);

        getSupportActionBar().setTitle(channelName);

        PostsFragment postsFragment = new PostsFragment();
        postsFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, postsFragment, POSTSF_FRAGMENT)
                .commit();
        channel = channelRealm.where(Channel.class).equalTo(Channel.FIELD_CHANNEL_ID, channelID).findFirst();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshDisplay(channel);
        channelRealm.addChangeListener(changeListener);
    }

    @Override
    protected void onPause() {
        channelRealm.removeChangeListener(changeListener);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        //warning other components rely on the fact that channelRealm.close() is always called in onDestroy() and before super.onDestroy()
        channelRealm.close();
        super.onDestroy();
    }

    private void refreshDisplay(Channel channel) {
        User currentUser = UserManager.getInstance().getCurrentUser();
        //noinspection ConstantConditions
        ViewUtils.showByFlag(!createdBy.equals(currentUser.getEmail()) && (channel == null || !channel.isSubscribed()), subscribeFab);
    }

    @OnClick(R.id.fab_subscribe)
    void subscribeToChannel() {
        doSubscribeToChannel(channelID);
    }

    private void doSubscribeToChannel(String channelID) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.subcription_in_progresss, channelName));
        progressDialog.setCancelable(false);
        progressDialog.show();
        ChannelManager.subscribeToChannel(channelID, new UserManager.Callback() {
            @Override
            public void done(SliidoException e) {
                progressDialog.dismiss();
                if (!channelRealm.isClosed()) {
                    if (e != null) {
                        UiHelpers.showErrorDialog(NewsFeedActivity.this, e.getUserFriendlyMessage());
                    }
                    refreshDisplay(channel);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.news_feed, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_channel_detail:
                viewChannelDetails();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
    }

    @OnClick(R.id.toolbar)
    void viewChannelDetails() {
        ChannelDetailActivity.viewChannel(this, channelID, channelName, NewsFeedActivity.class.getSimpleName());
    }

    @Override
    public void onSubscribeToChannel(String channelID) {
        doSubscribeToChannel(channelID);
    }
}
