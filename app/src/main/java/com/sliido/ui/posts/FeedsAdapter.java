package com.sliido.ui.posts;

import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pairapp.util.Config;
import com.pairapp.util.FileUtils;
import com.pairapp.util.GenericUtils;
import com.pairapp.util.UiHelpers;
import com.pairapp.util.ViewUtils;
import com.sliido.R;
import com.sliido.adapters.SliidoBaseAdapter;
import com.sliido.data.post.Post;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.io.File;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

import static com.pairapp.util.ViewUtils.showByFlag;

/**
 * Created by null-pointer on 2/27/2016.
 */
class FeedsAdapter extends SliidoBaseAdapter<Post> {

    private final Delegate delegate;
    private final int maxPostLength;
    private final int darkGray;
    private final int colorPrimary;


    FeedsAdapter(Delegate delegate) {
        super(delegate);
        this.delegate = delegate;
        maxPostLength = delegate.context().getResources().getInteger(R.integer.post_length_max_no_truncate);
        darkGray = ContextCompat.getColor(getContext(), R.color.dark_gray);
        colorPrimary = ContextCompat.getColor(getContext(), R.color.colorPrimary);
    }

    @Override
    protected void doBindHolder(final Holder holder, int position) {
        final Post post = getItem(position);
        GenericUtils.assertThat(post != null);
        holder.itemView.setTag(post);
        ((ViewHolder) holder).tvChannelName.setText(post.getChannelName()); //see layout to see why we insert space in front
        ((ViewHolder) holder).tvLikePost.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        ((ViewHolder) holder).tvLikePost.setCompoundDrawablesWithIntrinsicBounds(post.isLiked() ? R.drawable.ic_favorite_black_24dp : R.drawable.ic_favorite_border_black_24dp, 0, 0, 0);
        ((ViewHolder) holder).tvLikePost.setTextColor(post.isLiked() ? colorPrimary : darkGray);
        ((ViewHolder) holder).tvLikePost.setText(GenericUtils.getString(R.string.likes, post.getLikes()));
        ((ViewHolder) holder).replies.setText(GenericUtils.getString(R.string.replies, post.getCommentCount()));

        //see the three dots we append to it
        boolean flag = post.getPostBody().length() > maxPostLength - 3;
        if (flag) {
            ((ViewHolder) holder).tvPostBody.setText(TextFormattingUtils.formatText(post.getPostBody().substring(0, maxPostLength - 3), '*', '_'));
            ((ViewHolder) holder).tvPostBody.append("...");
        } else {
            ((ViewHolder) holder).tvPostBody.setText(TextFormattingUtils.formatText(post.getPostBody(), '*', '_'));
        }
        showByFlag(flag, ((ViewHolder) holder).tvSeeMore);
        if (post.getState() == Post.POSTED) {
            long now = System.currentTimeMillis();
            long then = post.getCreatedAt();
            CharSequence formattedDate;
            formattedDate = ((now - then) < 1000 * 60) ? delegate.context().getString(R.string.just_now)
                    : DateUtils.getRelativeTimeSpanString(then, now, DateUtils.MINUTE_IN_MILLIS);
            ((ViewHolder) holder).tvDatePosted.setText(formattedDate);
        } else {
            ((ViewHolder) holder).tvDatePosted.setText(post.getState() == Post.POSTING ? R.string.posting : R.string.post_failed);
        }
        ((ViewHolder) holder).tvChannelName.setClickable(!post.getChannelName().equalsIgnoreCase(Config.appName()));
        showByFlag(post.getState() == Post.POSTING, ((ViewHolder) holder).progressBar, ((ViewHolder) holder).cancelPost);
        showByFlag(((ViewHolder) holder).postActionBar, post.getState() == Post.POSTED);
        showByFlag(((ViewHolder) holder).retry, post.getState() == Post.POST_FAILED);
        ViewUtils.hideViews(((ViewHolder) holder).attachmentPannel, ((ViewHolder) holder).feedPhoto);
        if (!TextUtils.isEmpty(post.getAttachment())) {
            if (post.getType() == Post.IMAGE) {
                RequestCreator creator = new File(post.getAttachment()).exists() ? Picasso.with(getContext())
                        .load(new File(post.getAttachment())) : Picasso.with(getContext())
                        .load(Uri.parse(post.getAttachment()));
                creator.placeholder(R.drawable.light_color_primary).into(((ViewHolder) holder).feedPhoto, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {

                    }
                });
                ViewUtils.hideViews(((ViewHolder) holder).attachmentPannel);
                ViewUtils.showViews(((ViewHolder) holder).feedPhoto);
            } else {
                ViewUtils.showViews(((ViewHolder) holder).attachmentPannel);
                ViewUtils.hideViews(((ViewHolder) holder).feedPhoto);
                ((ViewHolder) holder).attachmentTitle.setText(post.getAttachmentTitle());
                ((ViewHolder) holder).attachmentSise.setText(FileUtils.sizeInLowestPrecision(post.getAttachmentSize()));
                ((ViewHolder) holder).fileType.setText(FileUtils.getExtension(post.getAttachmentTitle()).toUpperCase(Locale.US));
                showByFlag(!post.isDownloaded(), ((ViewHolder) holder).download);
            }
        }
    }


    @Override
    protected Holder doCreateHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.feed_list_item, parent, false), delegate);
    }

    interface Delegate extends SliidoBaseAdapter.Delegate<Post> {
        void onLikePost(Post post);

        void onSeeMore(Post post);

        void onViewPost(Post post);

        void onSharePost(Post post);

        void onViewChannel(Post post);

        void onCommentOnPost(Post post);

        void retryPosting(Post post);

        void cancelPost(Post post);

        void onDownloadItem(Post post);

        void onViewPhoto(Post path);
    }

    static class ViewHolder extends Holder {
        private final Delegate delegate;
        @Bind(R.id.tv_post_body)
        TextView tvPostBody;
        @Bind(R.id.bt_retry)
        View retry;
        @Bind(R.id.tv_like_post)
        TextView tvLikePost;
        @Bind(R.id.feed_photo)
        ImageView feedPhoto;
        @Bind(R.id.post_actions_bar)
        View postActionBar;
        @Bind(R.id.post_progress)
        ProgressBar progressBar;
        @Bind(R.id.attachment_pannel_parent)
        View attachmentPannel;
        @Bind(R.id.tv_attachement_title)
        TextView attachmentTitle;
        @Bind(R.id.tv_file_size)
        TextView attachmentSise;
        @Bind(R.id.tv_file_type)
        TextView fileType;
        @Bind(R.id.iv_download)
        ImageButton download;
        @Bind(R.id.tv_channel_name)
        TextView tvChannelName;
        @Bind(R.id.tv_date_posted)
        TextView tvDatePosted;
        @Bind(R.id.tv_see_more)
        TextView tvSeeMore;
        @Bind(R.id.tv_reply)
        TextView replies;
        @Bind(R.id.bt_cancel)
        View cancelPost;

        ViewHolder(View view, Delegate delegate) {
            super(view);
            this.delegate = delegate;
        }

        @OnClick(R.id.feed_photo)
        void viewImage() {
            delegate.onViewPhoto(((Post) itemView.getTag()));
        }

        @OnClick(R.id.bt_retry)
        void retry(View v) {
            delegate.retryPosting(((Post) itemView.getTag()));
        }

        @OnClick(R.id.bt_cancel)
        void cancel(View v) {
            delegate.cancelPost(((Post) itemView.getTag()));
        }

        @OnClick(R.id.iv_download)
        void doDownloadFile(View v) {
            delegate.onDownloadItem(((Post) itemView.getTag()));
        }

        @OnClick(R.id.attachment_view)
        void openItem(View v) {
            Post post = (Post) itemView.getTag();
            if (post.isDownloaded()) {
                FileUtils.open(v.getContext(), post.getPath());
            }
        }

        @OnClick({R.id.tv_see_more,
                R.id.v_share_post,
                R.id.tv_like_post,
                R.id.tv_post_body,
                R.id.tv_channel_name,
                R.id.tv_reply
        })
        void OnClick1(View v) {
            Post post = (Post) itemView.getTag();
            if (post.getState() != Post.POSTED && v.getId() != R.id.v_share_post && v.getId() != R.id.tv_see_more && v.getId() != R.id.tv_channel_name) {
                UiHelpers.showToast(v.getContext().getString(R.string.not_available));
                return;
            }
            switch (v.getId()) {
                case R.id.tv_like_post:
                    delegate.onLikePost(post);
                    break;
                case R.id.tv_see_more:
                    delegate.onSeeMore(post);
                    break;
                case R.id.v_share_post:
                    delegate.onSharePost(post);
                    break;
                case R.id.tv_post_body:
                    delegate.onViewPost(post);
                    break;
                case R.id.tv_channel_name:
                    delegate.onViewChannel(post);
                    break;
                case R.id.tv_reply:
                    delegate.onCommentOnPost(post);
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }
}
