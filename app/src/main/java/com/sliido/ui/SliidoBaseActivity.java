package com.sliido.ui;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.pairapp.Errors.ErrorCenter;
import com.pairapp.util.Config;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.NavigationManager;
import com.pairapp.util.UiHelpers;
import com.sliido.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * author Null-Pointer on 1/15/2016.
 */
public abstract class SliidoBaseActivity extends AppCompatActivity implements EventBus.EventsListener {
    AlertDialog alertDialog;
    String currentErrorId = "";
    private final ErrorCenter.ErrorShower errorShower = new ErrorCenter.ErrorShower() {
        @Override
        public void showError(ErrorCenter.Error error) {
            alertDialog = new AlertDialog.Builder(SliidoBaseActivity.this)
                    .setMessage(error.message)
                    .setPositiveButton(android.R.string.ok, null)
                    .setTitle(getString(R.string.alert))
                    .create();
            currentErrorId = error.id;
            alertDialog.show();
        }


        @Override
        public void disMissError(String errorId) {
            if (alertDialog != null && currentErrorId.equals(errorId)) {
                alertDialog.dismiss();
            }
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NavigationManager.onCreate(this);
    }


    public android.support.v4.app.FragmentManager getFManager() {
        return getSupportFragmentManager();
    }

    @Override
    protected void onPause() {
        NavigationManager.onPause(this);
        super.onPause();
        Config.appOpen(false);
        ErrorCenter.unRegisterErrorShower(errorShower);
    }

    @Override
    protected void onStart() {
        super.onStart();
        NavigationManager.onStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NavigationManager.onResume(this);
        Config.appOpen(true);
        ErrorCenter.registerErrorShower(errorShower);
        if (Build.VERSION.SDK_INT >= 8) {
            PackageInfo info = null;
            try {
                info = getPackageManager().getPackageInfo("com.google.android.gsf", 0);
            } catch (PackageManager.NameNotFoundException e) {
                // do nothing
            }
            if (info == null) {
                notifyNoGooglePlayServicesAvailable();
            }
        } else {
            notifyNoGooglePlayServicesAvailable();
        }
    }

    private void notifyNoGooglePlayServicesAvailable() {
        UiHelpers.showErrorDialog(this, getString(R.string.no_google_play_services));
    }

    @Override
    protected void onStop() {
        NavigationManager.onStop(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        NavigationManager.onDestroy(this);
        super.onDestroy();
    }

    @Override
    public int threadMode() {
        return EventBus.MAIN;
    }

    @Override
    public boolean sticky() {
        return true;
    }

    protected void registerForEvent(Object tag) {
        EventsCenter.getEventBus().register(this, tag);
    }

    protected void unregisterForEvents(Object tag) {
        EventsCenter.getEventBus().unregister(tag, this);
    }
}
