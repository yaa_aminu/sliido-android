package com.sliido.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.pairapp.util.GenericUtils;
import com.sliido.R;
import com.sliido.dao.data.ShoutOutManager;

import java.util.List;

/**
 * Created by yaaminu on 11/15/17.
 */

public class AddShoutOutActivity extends AppCompatActivity implements AddShoutOutFragment.Callbacks {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shoutout);
    }

    @Override
    public void post(String postBody, List<String> attachments) {
        if (!GenericUtils.isEmpty(postBody)) {
            postBody = getString(R.string.empty_post_text, com.sliido.data.user.UserManager.getInstance().getCurrentUser().getName());
        }
        ShoutOutManager.getInstance()
                .postShoutout(postBody, attachments);
        setResult(Activity.RESULT_OK, new Intent());
        finish();
    }

}
