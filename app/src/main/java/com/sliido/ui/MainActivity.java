package com.sliido.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SearchEvent;
import com.pairapp.util.EventBus;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.PLog;
import com.pairapp.util.ViewUtils;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.sliido.R;
import com.sliido.data.NotificationEntry;
import com.sliido.data.channel.Channel;
import com.sliido.data.user.UserManager;
import com.sliido.ui.auth.LoginActivity;
import com.sliido.ui.channel.ChannelDetailActivity;
import com.sliido.ui.posts.NewPostActivity;
import com.sliido.ui.posts.PostsFragment;
import com.sliido.ui.settings.SettingsFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import io.realm.Realm;
import io.realm.RealmChangeListener;

import static com.pairapp.util.ViewUtils.isViewVisible;


public class MainActivity extends SliidoBaseActivity
        implements PostsFragment.Delegate, OnTabSelectListener {

    public static final int REQUEST_NEW_POST = 0xC;
    public static final String CURRENT_FRAGMENT = "currentFragment";
    public static final String SEARCH_TEXT_CHANGED = "search.text.field.change";
    public static final String KEY_SLIIDO_USER_FIRST_START = "sliido.user.firstStart";
    public static final String BACK_STACK_SEARCH = "search";
    private static final String TAG = MainActivity.class.getSimpleName();
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.search_view)
    View searchView;

    @Bind(R.id.et_filter_text)
    EditText searchEditText;

    @Bind(R.id.clear_search)
    View clearText;

    Handler eventPostHandler = new Handler();
    long lastPosted = 0;
    @NonNull
    String filterText = "";
    Runnable eventPostRunnable = new Runnable() {
        @Override
        public void run() {
            if (SystemClock.uptimeMillis() - lastPosted >= 750) {
                EventsCenter.getEventBus().post(new EventBus.Event(SEARCH_TEXT_CHANGED, null, filterText));
            }
        }
    };
    @Bind(R.id.bottom_bar)
    BottomBar bottomBar;
    TextView newNotifications;
    private int currentFragment = 0;
    private View.OnClickListener navigationOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isViewVisible(searchView)) {
                closeSearch();
            }
        }
    };
    private Realm realm;
    private final RealmChangeListener<Realm> changeListener = new RealmChangeListener<Realm>() {
        @Override
        public void onChange(Realm realm) {
            setupBadges();
        }
    };

    @Nullable
    private Fragment fragment;

    public static void goToLogin(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Channel.REALM();
        if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {
            String path = getIntent().getData().getPath();
            if (path.equals("/ch")) {
                Intent intent = new Intent(this, ChannelDetailActivity.class);
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(getIntent().getData());
                startActivity(intent);

                finish();
            }
        } else if (Intent.ACTION_SEND.equals(getIntent().getAction())) {
            String text = getIntent().getStringExtra(Intent.EXTRA_TEXT);
            if (getIntent().hasExtra(Intent.EXTRA_TEXT)) {
                text = text == null ? "" : text;
                NewPostActivity.createNewPost(this, text);
            } else {
                Uri data = getIntent().getData();
                if (data == null) {
                    data = getIntent().getParcelableExtra(Intent.EXTRA_STREAM);
                    if (data == null) {
                        finish();
                        return;
                    }
                }
                NewPostActivity.createNewPostWithAttachment(this, data);
            }
            finish();
        } else {
            setContentView(R.layout.activity_main);
            ButterKnife.bind(this);
            toolbar.setTitle(R.string.app_name);
            toolbar.setNavigationIcon(R.drawable.tab_home);
            setSupportActionBar(toolbar);
            bottomBar.setBadgesHideWhenActive(false);
            bottomBar.setBadgeBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_light));
            bottomBar.setOnTabSelectListener(this);
            EventsCenter.getEventBus().register(this, UserManager.LOGOUT_EVENT);
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_FRAGMENT, currentFragment);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (currentFragment == 0) {
            currentFragment = savedInstanceState.getInt(CURRENT_FRAGMENT, R.id.tab_home);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (currentFragment == 0) {
            currentFragment = R.id.tab_home;
        }
        realm.addChangeListener(changeListener);
        restoreState();
    }

    @Override
    protected void onPause() {
        realm.removeChangeListener(changeListener);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        EventsCenter.getEventBus().unregister(UserManager.LOGOUT_EVENT, this);
        super.onDestroy();
    }

    private void restoreState() {
        addFragment(currentFragment);
        bottomBar.selectTabWithId(currentFragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_NEW_POST:
                    currentFragment = R.id.tab_home;
                    break;
                default:
                    PLog.d(TAG, "unknown request code from onActivityResult");
                    super.onActivityResult(requestCode, resultCode, data);
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if ((fragment != null) && (fragment instanceof BaseFragment) && ((BaseFragment) fragment).onBackPressed()) {
            return;
        }
        if (isViewVisible(searchView)) {
            closeSearch();
        } else {
            if (currentFragment != R.id.tab_home) {
                currentFragment = R.id.tab_home;
                addFragment(currentFragment);
                bottomBar.selectTabWithId(R.id.tab_home);
                supportInvalidateOptionsMenu();
            } else {
                super.onBackPressed();
            }
        }
    }

    @OnClick(R.id.clear_search)
    void clear() {
        searchEditText.setText("");
    }

    @OnTextChanged(R.id.et_filter_text)
    void onTextChanged(CharSequence text) {
        String theText = text.toString().trim();
        ViewUtils.showByFlag(clearText, theText.length() > 0);
        if (theText.equals(filterText)) {
            return;
        }
        filterText = theText;
        lastPosted = SystemClock.uptimeMillis();
        eventPostHandler.removeCallbacks(eventPostRunnable);
        eventPostHandler.postDelayed(eventPostRunnable, 750);
    }

    @Override
    public void onTabSelected(int item) {
        currentFragment = item;
        addFragment(currentFragment);
    }

    private void addFragment(int id) {
        fragment = getFManager().findFragmentByTag(id + "");
        boolean addToBackStack = false;
        if (fragment == null) {
            switch (id) {
                case -1:
                    fragment = new BoardSearchResultsFragment(); // TODO: 11/3/17 use a parent search fragment instead
                    addToBackStack = true;
                    break;
                case R.id.tab_slides:
                    fragment = new SlidesFragment();
                    break;
                case R.id.tab_boards:
                    fragment = new BoardsFragment();
                    break;
                case R.id.tab_discover:
                    fragment = new DiscoverFragment();
                    break;
                case R.id.tab_settings:
                    fragment = new SettingsFragment();
                    break;
                case R.id.tab_home:
                    fragment = new ShoutOutFragment();
                    break;
                default:
                    throw new AssertionError();
            }
        }
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, TAG);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(BACK_STACK_SEARCH);
        }
        fragmentTransaction.commit();
        supportInvalidateOptionsMenu();
    }

    @Override
    public void onEvent(EventBus yourBus, EventBus.Event event) {
        if (UserManager.LOGOUT_EVENT.equals(event.tag)) {
            if (event.error == null) {
                finish();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final MenuItem menuItem = menu.findItem(R.id.action_notifications);

        View actionView = menuItem.getActionView();
        newNotifications = actionView.findViewById(R.id.notification_badge);

        setupBadges();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void setupBadges() {
        if (newNotifications != null) {
            long num = realm.where(NotificationEntry.class)
                    .equalTo("read", false)
                    .count();
            if (num > 0) {
                ViewUtils.showViews(newNotifications);
                newNotifications.setText(String.valueOf(Math.min(num, 99)));
            } else {
                ViewUtils.hideViews(newNotifications);
            }
        }
        Number sum = realm.where(Channel.class)
                .sum(Channel.FIELD_NEW_POST_COUNT);
        bottomBar.getTabAtPosition(0).setBadgeCount(Math.min(99, sum.intValue()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            openSearch();
            return true;
        } else if (item.getItemId() == R.id.action_notifications) {
            startActivity(new Intent(this, NotificationsActivity.class));
            return true;
        } else if (item.getItemId() == R.id.tab_settings) {
            currentFragment = R.id.tab_settings;
            addFragment(R.id.tab_settings);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_search).setVisible(!ViewUtils.isViewVisible(searchView) && currentFragment != R.id.tab_settings);
        menu.findItem(R.id.action_notifications).setVisible(!ViewUtils.isViewVisible(searchView) && currentFragment != R.id.tab_settings);
        return super.onPrepareOptionsMenu(menu);
    }

    private void openSearch() {
        toolbar.setNavigationOnClickListener(navigationOnClickListener);
        ViewUtils.hideViews(clearText);
        ViewUtils.showViews(searchView);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        filterText = "";
        searchEditText.requestFocus();
        searchEditText.setText(""); //force the key board to show up
        addFragment(-1);
        supportInvalidateOptionsMenu();
    }

    public void closeSearch() {
        if (searchEditText.getText().toString().trim().length() >= 3) {
            Answers.getInstance().logSearch(new SearchEvent().putQuery(filterText));
        }
        toolbar.setNavigationOnClickListener(null);
        toolbar.setNavigationIcon(R.drawable.tab_home);
        clear();
        ViewUtils.hideViews(searchView);
        supportInvalidateOptionsMenu();
        getSupportFragmentManager()
                .popBackStack(BACK_STACK_SEARCH, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onSubscribeToChannel(String channelID) {
        throw new UnsupportedOperationException();
    }
}
