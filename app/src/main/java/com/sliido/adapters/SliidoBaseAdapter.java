package com.sliido.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairapp.util.GenericUtils;
import com.pairapp.util.PLog;
import com.sliido.R;

import java.util.List;

import butterknife.ButterKnife;

/**
 * the base class of all Adapters in this app.
 * to use it create an instance of {@link Delegate} and let your view holders subclass
 * <p>
 * <p>
 * avoid calling {@link #notifyDataSetChanged()} directly on this adapter as it can put it
 * into an undefined state. use {@link #notifyDataChanged} rather
 * {@link Holder}
 * author Null-Pointer on 1/23/2016.
 */
public abstract class SliidoBaseAdapter<T> extends RecyclerView.Adapter<SliidoBaseAdapter.Holder> {

    private static final int LOADING_MORE_VIEW_TYPE = -1;
    private static final String TAG = SliidoBaseAdapter.class.getSimpleName();
    protected final LayoutInflater inflater;
    private final Delegate<T> delegate;
    private List<T> items;
    private int lastKnowPosition = RecyclerView.NO_POSITION;
    private final Runnable resetLastKnownPosition = new Runnable() {
        @Override
        public void run() {
            lastKnowPosition = RecyclerView.NO_POSITION;
        }
    };
    private boolean showLoadingMore;

    public SliidoBaseAdapter(Delegate<T> delegate) {
        this.items = delegate.dataSet();
        showLoadingMore = delegate.showLoadingMore();
        this.delegate = delegate;
        inflater = LayoutInflater.from(delegate.context());
    }

    /**
     * a hook for subclasses to notify us that they are now
     * in sync so we should refresh the  data set.
     * <p>
     * subclasses should always call this method some  time  after they return false in
     * {@link #outOfSync()}.
     */
    protected final void onInSync() {
        notifyDataChanged();
    }

    /**
     * returns the current item T in the data set
     *
     * @param position the  position
     * @return The current item in the data set or <code>null</code> if {@link Delegate#showLoadingMore()} is true and we are at end
     * of the items.
     * @throws ArrayIndexOutOfBoundsException if the position is out of bounds
     */
    @Nullable
    public final T getItem(int position) {
        if (items.size() == position) {
            return null;
        } else {
            return items.get(position);
        }
    }

    protected int doGetViewType(int position) {
        return 0;
    }

    @Override
    public final int getItemViewType(int position) {
        if (getItem(position) == null) return LOADING_MORE_VIEW_TYPE;
        final int viewType = doGetViewType(position);
        GenericUtils.assertThat(viewType != LOADING_MORE_VIEW_TYPE, "this view type is reserved");
        return viewType;
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public final int getItemCount() {
        return items.size() + (delegate.showLoadingMore() ? 1 : 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void onBindViewHolder(final Holder holder, final int position) {
        if (holder.getItemViewType() == LOADING_MORE_VIEW_TYPE) {
            return;
        }
        doBindHolder(holder, position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delegate.onItemClick(SliidoBaseAdapter.this, view, holder.getAdapterPosition(), getItemId(holder.getAdapterPosition()));
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return delegate.onItemLongClick(SliidoBaseAdapter.this, view, holder.getAdapterPosition(), getItemId(holder.getAdapterPosition()));
            }
        });
        //position starts from zero
        if (lastKnowPosition != holder.getAdapterPosition() && items.size() == (1 + holder.getAdapterPosition())) {
            holder.itemView.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        lastKnowPosition = holder.getAdapterPosition();
                        delegate.onLoadMoreItems(holder.getAdapterPosition());
                    } catch (Exception e) { //likely that the containing activity/fragment has bee destroyed
                        PLog.e(TAG, e.getMessage(), e);
                        if (com.sliido.BuildConfig.DEBUG) {
                            throw e;
                        }
                    }
                }
            });
        }
    }

    protected final Context getContext() {
        return delegate.context();
    }

    @Override
    public final Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == LOADING_MORE_VIEW_TYPE) {
            return new Holder(inflater.inflate(R.layout.loading_more_view_type, parent, false));
        }
        return doCreateHolder(parent, viewType);
    }

    /**
     * notify the adapter that it's dataset has changed
     * please use this  instead of {@link #notifyDataSetChanged()} for correct behaviour
     */
    public void notifyDataChanged() {
        //the best place to do this is in notifyDataSetChanged() but its final so we cannot override it
        if (outOfSync()) {
            PLog.v(TAG, "out of sync");
            return;
        }
        items = delegate.dataSet();
        showLoadingMore = delegate.showLoadingMore();
        notifyDataSetChanged();
    }

    /**
     * returns true if the adapter is out sync. this is used to determine whether
     * {@link super#notifyDataSetChanged()} should be called
     *
     * @return true if the adapter is out sync, false otherwise
     */
    protected boolean outOfSync() {
        return false;
    }

    /**
     * a hook for subclasses to bind their view holder to the adapter
     *
     * @param holder   the holder returned in {@link #onBindViewHolder(Holder, int)}
     * @param position the current position in the adapter
     */
    protected abstract void doBindHolder(Holder holder, int position);

    protected abstract Holder doCreateHolder(ViewGroup parent, int viewType);

    /**
     * an interface that this adapter uses to delegate some duties.
     *
     * @param <T> the model class used in this adapter
     */
    public interface Delegate<T> {
        /**
         * @param adapter  the adapter to which this view is bound
         * @param view     the current view that has been clicked
         * @param position the current position in the data set
         * @param id       the id of the current item
         */
        void onItemClick(SliidoBaseAdapter<T> adapter, View view, int position, long id);

        /**
         * @param adapter  the adapter to which this view is bound
         * @param view     the current view that has been clicked
         * @param position the current position in the data set
         * @param id       the id of the current item
         */
        boolean onItemLongClick(SliidoBaseAdapter<T> adapter, View view, int position, long id);

        /**
         * @return the dataset used to back this adapter, may not be null
         */
        @NonNull
        List<T> dataSet();

        boolean showLoadingMore();

        Context context();

        void onLoadMoreItems(int position);
    }

    /**
     * a {@link android.support.v7.widget.RecyclerView.ViewHolder} implementation that uses
     * butterKnife to bind views to it.. just subclass it and use all the nice butterKnife
     * annotations. Because they will work :P
     */
    public static class Holder extends RecyclerView.ViewHolder {
        public Holder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
