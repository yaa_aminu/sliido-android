package com.sliido;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.facebook.FacebookSdk;
import com.pairapp.util.Config;
import com.pairapp.util.ConnectionUtils;
import com.pairapp.util.EventsCenter;
import com.pairapp.util.PLog;
import com.pairapp.util.TaskManager;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.di.DependencyInjector;
import com.sliido.data.ParseClient;

import java.util.concurrent.atomic.AtomicBoolean;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * author Null-Pointer on 1/12/2016.
 */
public class SliidoApplication extends Application {

    public static final String TAG = SliidoApplication.class.getSimpleName();
    public static final String BASE_URL = "http://sliido.herokuapp.com/";
    public static final String HTTP_SLIIDO_COM_HELP_LOGIN_SIGNUP = BASE_URL + "support?login-signup";
    public static final String HTTP_SLIIDO_COM_DOWNLOAD = BASE_URL + "dl";
    public static final String HTTP_SLIIDO_COM_LEGAL_OPENSOURCE_LISCENSE = BASE_URL + "legal/opensource";
    public static final String HTTP_SLIIDO_COM_HELP_VERIFICATION = BASE_URL + "support?verification";
    public static final String TERMS_URL = BASE_URL + "legal/terms";
    public static final String PRIVACY_POLICY = BASE_URL + "legal/privacy";


    public final AtomicBoolean isLoginOrSignupActive = new AtomicBoolean(false);

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Fabric.with(SliidoApplication.this, new Answers(), new Crashlytics());
        PLog.setLogLevel(BuildConfig.DEBUG ? PLog.LEVEL_VERBOSE : PLog.LEVEL_FATAL);
        Config.init(this, getString(R.string.app_name));
        FacebookSdk.sdkInitialize(this);
        DependencyInjector injector = new DependencyInjector() {
            @Override
            public void inject(Job job) {
                PLog.d(TAG, "dummy injector");
            }
        };
        ConnectionUtils.init(this);
        TaskManager.init(this, injector);
        EventsCenter.init();
        ParseClient.init(this, BuildConfig.DEBUG ? Parse.LOG_LEVEL_VERBOSE : Parse.LOG_LEVEL_NONE);
        ParseFacebookUtils.initialize(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/roboto_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
