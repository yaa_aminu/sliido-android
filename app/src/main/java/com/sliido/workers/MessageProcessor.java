package com.sliido.workers;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.pairapp.Errors.SliidoException;
import com.pairapp.util.BuildConfig;
import com.pairapp.util.PLog;
import com.parse.ParseUser;
import com.sliido.R;
import com.sliido.data.NotificationEntry;
import com.sliido.data.channel.Channel;
import com.sliido.data.channel.ChannelManager;
import com.sliido.data.post.Post;
import com.sliido.ui.MainActivity;
import com.sliido.ui.posts.CommentsActivity;
import com.sliido.ui.posts.PostDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;

import static android.text.TextUtils.join;
import static com.pairapp.Errors.ErrorCenter.showNotification;
import static com.sliido.data.NotificationEntry.TYPE_COMMENT;
import static com.sliido.data.NotificationEntry.TYPE_LIKE;
import static com.sliido.data.user.UserManager.getInstance;
import static java.util.Arrays.asList;

/**
 * @author Null-Pointer on 2/3/2016.
 */
public class MessageProcessor extends IntentService {

    public static final String EXTRA_DATA = "data";
    public static final String PUSH_BODY = "body";
    public static final String PUSH_TYPE = "type";
    public static final String PUSH_TYPE_POST = "post";
    public static final String NOTIFICATION = "notification";
    private static final String TAG = MessageProcessor.class.getSimpleName();

    public MessageProcessor() {
        super("messageProcessor");
    }

    private static void testMessageProcessor(Context context, String json) {
        Intent intent = new Intent(context, MessageProcessor.class);
        intent.putExtra(EXTRA_DATA, json);
        context.startService(intent);
    }

    @SuppressWarnings("unused") //used for testing
    public static void testHandleNewPost(Context context) {
        try {
            JSONObject object = new JSONObject(), object1 = new JSONObject();
            object.put(PUSH_TYPE, "post");
            object1.put("preview", "preview of post will be here and may be truncated if too long");
            object1.put(Post.FIELD_CHANNEL_NAME, "Channel Name");
            object1.put(Post.FIELD_POST_ID, "AYI78jaYT");
            object.put(PUSH_BODY, object1.toString());
            testMessageProcessor(context, object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private static void doHandleMessage(Context context, String message) throws JSONException {
        JSONObject object = new JSONObject(message);
        String action = object.getString(PUSH_TYPE);
        Object tmp = object.get(PUSH_BODY);
        if (tmp instanceof String) {
            object = new JSONObject((String) tmp);
        } else if (tmp instanceof JSONObject) {
            object = ((JSONObject) tmp);
        } else {
            throw new JSONException("unable to deserialize json data");
        }
        if (PUSH_TYPE_POST.equals(action)) {
            String postId = object.getString(Post.FIELD_POST_ID);
            try {
                Post post = ChannelManager.refreshPostSync(postId);
                if (!post.getPostedBy().equals(getInstance().getCurrentUser().getEmail())
                        && ChannelManager.isNotificationsEnabled(post.getChannelId())) {
                    Intent pIntent = new Intent(context, PostDetailActivity.class);
                    pIntent.putExtra(PostDetailActivity.EXTRA_POST_ID, postId);
                    PendingIntent pendingIntent =
                            TaskStackBuilder.create(context)
                                    .addParentStack(MainActivity.class)
                                    .addNextIntent(pIntent)
                                    .getPendingIntent(-1, PendingIntent.FLAG_UPDATE_CURRENT);
                    String preview = post.getPostBody().length() > 50 ? post.getPostBody().substring(0, 47) + "..." : post.getPostBody();
                    showNotification(context, NotificationIdOffsets.NOT_ID_NEW_POST, context.getString(R.string.new_from, post.getChannelName()),
                            preview, 0, pendingIntent, true, false, true);
                    Vibrator vibrate = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
                    vibrate.vibrate(TimeUnit.SECONDS.toMillis(1));
                }
            } catch (SliidoException e) {
                PLog.e(TAG, e.getMessage(), e);
            }
        } else if (NOTIFICATION.equals(action)) {
            handleNotification(context, object);
        } else {
            PLog.d(TAG, "unknown push message %s", object.toString());
        }
    }

    public static void testHandleNotifications(Context context) {
        try {
            JSONObject jsonObject = new JSONObject()
                    .put(PUSH_TYPE, "notification");
            jsonObject.put(PUSH_BODY, new JSONObject()
                    .put("action", "comment")
                    .put("postId", "XdA3fA8t1g"));// TODO: 11/4/17 use a real post
            testMessageProcessor(context, jsonObject.toString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private static void handleNotification(Context context, JSONObject object) throws JSONException {
        Realm realm = Channel.REALM();
        try {
            // TODO: 11/3/17 use a job
            String postId = object.getString("postId");
            String action = object.getString("action");
            String by = object.getString("by");
            if (ParseUser.getCurrentUser() != null && ParseUser.getCurrentUser().getObjectId().equals(by)) {
                PLog.w(TAG, "action taken by this user, no need to notify");
                return;
            }
            Post post = ChannelManager.refreshPostSync(postId);
            List<String> names = getInstance().getNames(asList("comment".equals(action) ? post.getCommenters().split(",") : post.getLikers().split(",")));
            realm.beginTransaction();
            Number notificationId = realm.where(NotificationEntry.class).max("notificationId");
            NotificationEntry entry = new NotificationEntry(postId + action, postId, "comment".equals(action) ?
                    TYPE_COMMENT : TYPE_LIKE, join(",", names), post.getUpdatedAt(),
                    Math.min(NotificationIdOffsets.COMMENTS_NOTIFICATION_IDS,
                            notificationId == null ? NotificationIdOffsets.COMMENTS_NOTIFICATION_IDS : notificationId.intValue() + 1));
            realm.copyToRealmOrUpdate(entry);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "comments");
            builder.setVibrate(new long[]{TimeUnit.SECONDS.toMillis(1), TimeUnit.SECONDS.toMillis(1)});
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            builder.setContentText(entry.getText(context));
            builder.setTicker(context.getString(R.string.new_comment));
            builder.setContentTitle(context.getString(R.string.app_name));
            builder.setSmallIcon(R.drawable.ic_stat_notifications_active);
            builder.setAutoCancel(true);
            Intent intent = new Intent(context, CommentsActivity.class);
            intent.putExtra(CommentsActivity.EXTRA_POST_ID, postId);
            intent.putExtra(CommentsActivity.FROM_STATUS_BAR, true);
            builder.setContentIntent(PendingIntent.getActivity(context, 1001, intent, PendingIntent.FLAG_UPDATE_CURRENT));
            NotificationManagerCompat.from(context)
                    .notify(entry.getNotificationId(), builder.build());
            realm.commitTransaction();
        } catch (SliidoException e) {
            PLog.w(TAG, e.getMessage(), e);
        } finally {
            realm.close();
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String message = intent.getStringExtra(EXTRA_DATA);

        Log.d(TAG, "push received: " + message);

        final PowerManager manager = ((PowerManager) getSystemService(POWER_SERVICE));
        PowerManager.WakeLock lock = manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        lock.acquire();

        try {
            doHandleMessage(this, message);
        } catch (JSONException e) {
            PLog.d(TAG, e.getMessage(), e);
            if (BuildConfig.DEBUG) {
                throw new RuntimeException();
            }
        } finally {
            lock.release();
        }
    }
}
