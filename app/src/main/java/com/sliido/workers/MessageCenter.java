package com.sliido.workers;

import android.content.Context;
import android.content.Intent;

import com.pairapp.util.PLog;
import com.parse.ParsePushBroadcastReceiver;

/**
 * @author Null-Pointer on 1/30/2016.
 */
public class MessageCenter extends ParsePushBroadcastReceiver {
    private static final String TAG = MessageCenter.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String pushData = intent.getStringExtra(KEY_PUSH_DATA);
        PLog.d(TAG, "push received! %s", pushData);
        Intent serviceIntent = new Intent(context, MessageProcessor.class);
        serviceIntent.putExtra(MessageProcessor.EXTRA_DATA, pushData);
        context.startService(serviceIntent);
    }
}
